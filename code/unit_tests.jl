# unit tests
using Distributions
using Pipe

include("viewer_utility.jl")
include("viewer_inference.jl")
include("reporting.jl")
# include("news_objective.jl")

N = 10;
D = 5;
C = 2;
K = 3;
T = 2;
n_TZ = 1;
n_DP = 1;


μ = zeros(Float64, N, D); # viewer beliefs
μ[:,1] .= rand(Uniform(0,1), N)
μ[1,1] = 0.5 # fix first viewer's belief
μ_η = rand(Uniform(0,1), K*N)
μ_η = reshape(μ_η, K, N)

view_prob = rand(Uniform(0,1), C, N);

view_choice = zeros(Int64, C, N);
for i in eachindex(view_choice)
     view_choice[i] = rand(Binomial(1, view_prob[i]), 1)[1]
end

cumulative_view =  zeros(Float64, N, C);

# Δ_1 = zeros(Float64, K, C, N);
# Δ_0 = zeros(Float64, K, C, N);
Δ_1 = rand(Uniform(0,1), K*C*N);
Δ_1 = reshape(Δ_1, K, C, N);
Δ_0 = rand(Uniform(0,1), K*C*N);
Δ_0 = reshape(Δ_0, K, C, N);

# set half of w to 0
# w1 = ones(Float64, K, C, Int.(N/2));
# w2 = zeros(Float64, K, C, Int.(N/2));  
# w = cat(w1, w2, dims=3)

# w all ones
w = ones(Float64, K, C, N)

ρ_diff = ones(Float64, K, C)
ρ_0 = ones(Float64, K, C)
β_0 = ones(Float64, N)
β_C = ones(Float64, C)
β_T = ones(Float64, K)
β_B = ones(Float64, n_TZ * n_DP)
β_I = 1.0
β_S = 1.0

is_network = [true, false]
topic_ξ = repeat([1], K)
topic_r = [10,1,1/20]
topic_r_log = log.(topic_r)

show = zeros(Int64, C, T, n_TZ) 
show[1,:,:] .= 1 # only one show per channel
show[2,:,:] .= 2
coverage = zeros(Bool, K, C, T, n_TZ) # no coverage of any topic (COME BACK TO)
tz_daypart = ones(Int64, T, n_TZ)
choice_draws = rand(Uniform(0,1), N, T)


i=1;
d=1;
t=1;
tz=1;

viewership_decision!(i, d, t, tz,
    view_prob, view_choice,           # matrices storing viewing probability (C x N) and viewership decision (C x N)  
    cumulative_view,    # matrix storing cumlative viewing for each individual and channel (N x C) 
    Δ_1, Δ_0,       # viewer anticipated belief changes (K x C x N)
    w,              # viewer expected topic weights (K x C x N)
    μ, μ_η,         # viewer beliefs over state (N x D) and over prob of news today (K x N)
    # from here on not modified
    # parameters 
    ρ_diff, ρ_0,                    # reporting parameters (K x C)
    β_0, β_C, β_B, β_T, β_I, β_S,   # viewer taste parameters: β_0 is N x 1, β_C is C x 1 β_T is K x 1, β_B is (n_TZ * n_DP) x 1, others are scalars
    topic_ξ, topic_r, topic_r_log,  # topic parameters (K x 1)
    # data
    is_network,     # network indicator (C x 1)
    show,           # show indicator (C x T x n_TZ)
    coverage,       # indicators for segment reporting on topic (K x C x T x n_TZ)
    tz_daypart,     # index of timezone-hour dummy corresponding to this block and viewer (T x n_TZ)
    # random draws:
	choice_draws    # random draw determining choice of each i at time t (N x T)
    )



info_util = zeros(Float64, N)
for i in 1:N
    info_util[i] = β_I * sum(w[:,c,i] .* (Δ_1[:,c,i] .^ 2) .+ (1 .- w[:,c,i]) .* (Δ_0[:,c,i] .^ 2))    
end

slant_util = zeros(Float64, N)
for i in 1:N
    slant_util[i] = β_S * sum(w[:,c,i] .* topic_r_log) * (μ[i] - 0.5)
end