## Set your local directory
local_dir = "/home/gjmartin/Dropbox/STBNews"

# set to something nonempty to limit dates used in estimation
dates_to_keep = []  


# to sample only beta
# tree_base = "beta"

# # to sample all
tree_base = ""

### END OPTIONS ###

## Directory locations for code and data
using Printf
code_dir = @sprintf("%s/stb-model-event/code", local_dir)
data_dir = @sprintf("%s/stb-model-event/data", local_dir)
output_dir = @sprintf("%s/stb-model-event/output", local_dir)
sampling_dir = @sprintf("%s/stb-model-event/sampling", local_dir)

## LOAD OBJECTIVE, DATA, PARAMETERS ##
cd(code_dir)
include("load_model_data.jl")

# add pars to mprob object
SMM.addSampledParam!(mprob,pb);

# options for MCMC chain
opts = Dict(
    "N" => nprocs,
    "maxiter"=>5,
    "maxtemp" => nprocs/2,
    "sigma" => 0.005,
    "sigma_update_steps" => 100,
    "sigma_adjust_by" => 0.05,
    "smpl_iters" => 1000,
    "parallel" => true,
    "min_improve" => [0.0 for i = 1:nprocs],
    "acc_tuners" => [1.0 for i = 1:nprocs],
    "animate" => false,
    "sampling_scheme" => sample_tree
);


cd(output_dir)

## set-up BGP algorithm and worker pool to run objective evaluations
MA = MAlgoSTB(mprob, opts)
wp = CachingPool(workers())

### MAIN MCMC LOOP ###
SMM.run!(MA);

summary(MA)
chain1 = history(MA.chains[1]);
CSV.write("MCMC_chain1_20days.csv", chain1)

# to produce output for standard plots
ev1 = MA.chains[1].evals[maximum(MA.chains[1].best_id)]
ev1 = stb_obj(ev1; dt=stbdat, save_output=true, store_moments = true)

# to output moments
simmoments = SMM.check_moments(ev1)
select!(simmoments, [:moment, :data, :data_sd, :simulation, :distance])
simmoments.sq_diff = simmoments.distance.^2 .* simmoments.data_sd
sort!(simmoments, :sq_diff)
CSV.write("moments.csv", simmoments)





## grid plots

initial_beta_vote = SMM.paramd(SMM.Eval(mprob))[Symbol("beta:vote")];
initial_horse_race = SMM.paramd(SMM.Eval(mprob))[Symbol("topic_leisure:horse_race")];


bv_values = initial_beta_vote * collect(range(0.8,1.2,length=101))
hr_values = initial_horse_race * collect(range(0.8,1.2,length=101))

bv_fn_grid = zeros(Float64, 101)
bv_mom_grid = zeros(Float64, 101, length(SMM.dataMomentW(SMM.Eval(mprob))))

hr_fn_grid = zeros(Float64, 101)
hr_mom_grid = zeros(Float64, 101, length(SMM.dataMomentW(SMM.Eval(mprob))))

# fill beta_vote
println("beta:vote")
for i in 1:length(bv_values)
    if (mod(i,10) == 0)
        println(i)
    end

    # wrap param vector in OrderedDict
    pb_val = DataStructures.OrderedDict(k => k == "beta:vote" ? bv_values[i] : pb[k].value for k in keys(pb))

    # wrap in Eval object
    ev = SMM.Eval(mprob, pb_val)

    # evaluate
    ev = stb_obj(ev; dt=stbdat, store_moments = true)

    # store moments
    simmoments = SMM.check_moments(ev)
    simmoments.sq_diff = simmoments.distance.^2 .* simmoments.data_sd
    bv_mom_grid[i,:] = simmoments.sq_diff

    # store fval
    bv_fn_grid[i] = ev.value
end

# fill horse_race
println("topic_leisure:horse_race")
for i in 1:length(hr_values)
    if (mod(i,10) == 0)
        println(i)
    end
    # wrap param vector in OrderedDict
    pb_val = DataStructures.OrderedDict(k => k == "topic_leisure:horse_race" ? hr_values[i] : pb[k].value for k in keys(pb))

    # wrap in Eval object
    ev = SMM.Eval(mprob, pb_val)

    # evaluate
    ev = stb_obj(ev; dt=stbdat, store_moments = true)

    # store moments
    simmoments = SMM.check_moments(ev)
    simmoments.sq_diff = simmoments.distance.^2 .* simmoments.data_sd
    hr_mom_grid[i,:] = simmoments.sq_diff

    # store fval
    hr_fn_grid[i] = ev.value
end

using Plots
Plots.plot(bv_fn_grid)

overall_mean = dropdims(mapslices(Statistics.mean, bv_mom_grid;dims=1); dims=1)

sortperm(overall_mean)

CSV.write("bv_moments_grid.csv", Tables.table(bv_mom_grid))
CSV.write("hr_moments_grid.csv", Tables.table(hr_mom_grid))

overall_var = dropdims(mapslices(Statistics.var, bv_mom_grid;dims=1); dims=1)

sortperm(overall_var)
