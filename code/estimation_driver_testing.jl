include("setup.jl")

using TimerOutputs
using BlackBoxOptim
using Plots

# following guidance here: 
# https://discourse.julialang.org/t/how-to-correctly-define-and-use-global-variables-in-the-module-in-julia/65720
const n_func_evals = Ref{Float64}(0.0)

# define subset of pars to optimize over
# to_optimize from setup.jl
inds_to_ignore =  Ref(findall(occursin.(r"alpha|beta:info|topic", to_optimize)))
pb, pb0, to_optimize, ub, lb = load_and_update_parameters(output_dir, data_dir, to_optimize, par_bounds, inds_to_ignore)


## set initial parameter vector
x0 = [pb[k] for k in to_optimize]

## set SD of noise using diff from best to init
# change from old initial to newest changes
# x_diff = [abs(pb[k] - pb0[k].value) for k ∈ to_optimize] ./ 25
x_diff = [abs(1.0) for k ∈ to_optimize] ./ 25
x_diff = [max(x, 0.005) for x in x_diff]  # set floor
x_diff = [min(x, 0.1) for x in x_diff]  # set ceiling
# for parameters not to optimize, set diff to 0

# FIXME: add back
# x_diff = [x_diff; zeros(length(to_add))]

## generate initial candidates
rng = Random.MersenneTwister(14592150)
init_pop = Random.randn(rng, Float64, (length(to_optimize), 49)) .* x_diff .+ x0 

init_pop = min.(init_pop, ub)
init_pop = max.(init_pop, lb)

init_pop = [x0 init_pop]

## function to optimize - calls news_obj and saves params / fval to csv
function f0(x)
    # wrap param vector in OrderedDict
    # pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? x[findfirst(k .== to_optimize)] : pb[k].value for k in keys(pb))
    pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? x[findfirst(k .== to_optimize)] : pb[k] for k in keys(pb))
    # wrap in Eval object
    ev = SMM.Eval(mprob, pb_val)
    
    # call objective
    obj = news_obj(ev; dt=stbdat)

    # save parameters tested
    outrow = Dict(Symbol(to_optimize[i]) => x[i] for i = 1:length(to_optimize))
    # following guidance here: 
    # https://discourse.julialang.org/t/how-to-correctly-define-and-use-global-variables-in-the-module-in-julia/65720
    n_func_evals[] = n_func_evals[] + 1.0
    outrow[Symbol("n_func_evals")] = n_func_evals[]
    outrow[Symbol("fval")] = obj.value
    output_df = DataFrames.DataFrame(;outrow...)
    CSV.write("$output_dir/parameter_tracker.csv", output_df; append=true)
    obj.value
end

@time f0(x0)

#### SET UP CSV TO SAVE PARAMETERS TESTED ####
println("last obj value")
last_obj_val = f0(x0)
println(last_obj_val)
println("saving initital parameter_tracker and bboptim_progress")
outrow = Dict(Symbol(to_optimize[i]) => x0[i] for i = 1:length(to_optimize))
outrow[Symbol("n_func_evals")] = 0
outrow[Symbol("fval")] = last_obj_val
output_df = DataFrames.DataFrame(;outrow...)
CSV.write("$output_dir/parameter_tracker.csv", output_df)

println("start of new obj values")

#### BEGIN OPTIMIZATION ####
opt_params = Dict(:SearchSpace => BlackBoxOptim.ContinuousRectSearchSpace(lb, ub),
                  :Population => init_pop,
                  :MaxFuncEvals => 200,
                  :TraceInterval => 3600.0,
                  :TraceMode => :verbose,
                #   :Workers => workers(),
                  :PM_η => 250.0
                  )

opt_problem, opt_params = BlackBoxOptim.setup_problem(f0, chain(BlackBoxOptim.DefaultParameters, opt_params))


optimizer = BlackBoxOptim.adaptive_de_rand_1_bin_radiuslimited(opt_problem, opt_params)

opt_ctrl = BlackBoxOptim.OptController(optimizer, opt_problem, opt_params)
BlackBoxOptim.run!(opt_ctrl)

best_x = BlackBoxOptim.best_candidate(BlackBoxOptim.lastrun(opt_ctrl))
result = DataFrames.DataFrame(parameter = to_optimize, value = best_x)

# get current date in right format
using Dates
current_date = lpad(month(today()),2,"0") * lpad(day(today()),2,"0") * last(string(year(today())),2)
result |> CSV.write("$output_dir/bboptim_best_$current_date.csv")


# Create an empty DataFrame for storing all parameter values
all_results = DataFrames.DataFrame(parameter = String[], value = Float64[])

# Loop through all the parameters in the original `pb` dictionary
for k in keys(pb)
    # Check if the parameter is in the `to_optimize` array
    if k in to_optimize
        # Get the optimized value from the `best_x` array and add it to the new DataFrame
        idx = findfirst(==(k), to_optimize)
        push!(all_results, (k, best_x[idx]))
    else
        # Get the value from the `pb` dictionary and add it to the new DataFrame
        push!(all_results, (k, pb[k]))
    end
end

# Save the new DataFrame to a CSV file
all_results |> CSV.write("$output_dir/bboptim_best_all_$current_date.csv")
println("Best fval was $(BlackBoxOptim.best_fitness(BlackBoxOptim.lastrun(opt_ctrl)))")
