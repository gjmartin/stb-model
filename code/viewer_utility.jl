function viewership_decision!(i, t, d, tz,
    view_prob, 
    view_choice,        # matrices storing viewing probability (C x N) and viewership decision (C x N)  
    cumulative_view,    # matrix storing cumlative viewing for each individual and channel (N x C)
    μ,              # viewer beliefs over state (N x 1)
    # from here on not modified
    # parameters
    β_0, β_C, β_CS, β_B, β_T, β_I, β_S,   # viewer taste parameters: β_0 is N x 1, β_C and β_CS are C x 1 β_T is K x 1, β_B is (n_TZ * n_DP) x 1, others are scalars
    topic_ξ, topic_r, topic_r_log,  # topic parameters (K x 1)
    # data
    is_network,     # network indicator (C x 1)
    show,           # show indicator (C x T x n_TZ)
    coverage,       # indicators for segment reporting on topic (K x C x T x n_TZ)
    tz_daypart,     # index of timezone-hour dummy corresponding to this block and viewer (T x n_TZ)
    # random draws:
	choice_draws,    # random draw determining choice of each i at time t (N x T)
    zero_draws
    )
    # for a single individual, single block 
    # compute expected utility of watching each channel
    # simulate viewing decision 
    # if anything watched, update beliefs 

    # compute exp(utility) for each channel
    sumexp :: Float64 = 1.0
    for c = 1:C
        view_prob[c,i] = 0.0
        if !(is_network[c] & (show[c,t,tz] == 0))
            # skip non-news shows on networks
            # otherwise, compute components of utility (weighted sums over topics expected)
            # loop over K is equivalent to commented broadcast statement below but does half the allocations
            # @views view_prob[c,i] = sum(β_I .* (w[:,c,i] .* (Δ_1[:,c,i] .^ 2) .+ (1 .- w[:,c,i]) .* (Δ_0[:,c,i] .^ 2)) .+
            #             β_S .* (w[:,c,i] .* topic_r_log) .* (μ[i,d] .- 0.5) .+
            #             β_T .* w[:,c,i])

            for top in 1:K 
                @inbounds view_prob[c,i] +=  coverage[top,c,t,tz] * (β_S * topic_r_log[top] * (μ[i] - 0.5) + β_T[top]) 
            end                     
            base_taste = zero_draws[i] ? β_0[i] + β_C[c] + β_CS[c] * (μ[i] - 0.5) + β_B[tz_daypart[t,tz]] : β_0[i] # non-topic-specific components
            view_prob[c,i] = exp(min(view_prob[c,i] + base_taste, 30.0))    # add non-topic specific components, exponentiate

            sumexp += view_prob[c,i]
        end
    end

    # simulate viewing decision
    view_prob[:,i] ./= sumexp
    cumulative_prob = 1.0 / sumexp
    if choice_draws[i,t] >= cumulative_prob
        for c = 1:C
            cumulative_prob += view_prob[c,i]
            if choice_draws[i,t] < cumulative_prob
                view_choice[c,i] = true
                cumulative_view[i,c] += 1
                break
            end
        end
    end
    
end

