# using Distributed
# @everywhere begin
# get local directory
if occursin(r"gjmartin", pwd())
    local_dir = "/home/users/gjmartin"
elseif occursin(r"mlinegar", pwd())
    if occursin(r"Users", pwd())
        local_dir = "/Users/mlinegar/Code/"
    else
        local_dir = "/home/users/mlinegar/"
        # local_dir = "/home/users/mlinegar/stb"
    end
else occursin(r"mitchelllinegar", pwd())
    local_dir = "/Users/mitchelllinegar/Code/"
end

stb_dir = "stb-model"

# set to something nonempty to limit dates used in estimation
dates_to_keep = []  

# # to sample all
tree_base = ""

### END OPTIONS ###

using TimerOutputs
## Directory locations for code and data
using Dates
using BlackBoxOptim
using FileIO
using Plots
code_dir = "$local_dir/$stb_dir/code"
data_dir = "$local_dir/$stb_dir/data"
output_dir = "$local_dir/$stb_dir/output"
sampling_dir = "$local_dir/$stb_dir/sampling"

## LOAD OBJECTIVE, DATA, PARAMETERS ##
include("$code_dir/load_model_data.jl")
include("$code_dir/helper_functions.jl")

# following guidance here: 
# https://discourse.julialang.org/t/how-to-correctly-define-and-use-global-variables-in-the-module-in-julia/65720
const n_func_evals = Ref{Float64}(0.0)



# USE LAST RUN INSTEAD OF INITIAL PARAMETERS
# get most recent parameter file
best_param_files = readdir(output_dir)[occursin.(r"bboptim_best_", readdir(output_dir))]
# best_param_dates = Dates.Date.(first.(last.(best_param_files, 10),6),dateformat"mmddyy")
best_param_dates = Dates.Date.(first.(last.(best_param_files, 10),6),"mmddyy")
most_recent_best_param_file_name = best_param_files[findmax(best_param_dates)[2]]
most_recent_best_param_file = "$output_dir/$most_recent_best_param_file_name"


pb, par_init = params_from_csv(most_recent_best_param_file, par_bounds)
# add pars to mprob object
SMM.addSampledParam!(mprob,pb);
# compare to start of optimization/last time had all 
pb0, par_init0 = params_from_csv("$output_dir/bboptim_best_121622.csv", par_bounds)

# initial parameters parameters
pb00, par_init00 = params_from_csv("$data_dir/parameter_init.csv", par_bounds)#  re-order bounds
# define subset of pars to optimize over
to_optimize = String.(par_init00.parameter)  ## everything
# # to_optimize = String.(par_init.parameter[findall(occursin.(r"info|slant|topic_mu|topic_leisure", par_init.par))])
# # keep alphas fixed
inds_to_ignore =  Ref(findall(occursin.(r"alpha|beta:info|topic", to_optimize)))
to_optimize = to_optimize[eachindex(to_optimize) .∉ inds_to_ignore]

# to_optimize = [
# "beta:info",
# "beta:slant",
# "beta:zero",
# "beta:const",
# "beta:mu",
# "beta:sigma",
# "beta:base_slant:ABC",
# "beta:base_slant:CBS",
# "beta:base_slant:CNN",
# "beta:base_slant:FNC",
# "beta:base_slant:MSNBC",
# "beta:base_slant:NBC",
# "beta:channel:CBS",
# "beta:channel:CNN",
# "beta:channel:FNC",
# "beta:channel:MSNBC",
# "beta:channel:NBC",
# "rho0:0",
# "rho0:1"
# ]
par_init00 = par_init00[indexin(to_optimize, par_init00.parameter),:]
## bounds for search space
ub = par_init00.ub
lb = par_init00.lb
# ub = ub[eachindex(ub) .∉ inds_to_opt]
# lb = lb[eachindex(lb) .∉ inds_to_opt]




# if last run didn't have all the parameters, need to load in old values for missing parameters
# these are coming from pb0, but could also come from pb00
# original: throws errors
# pb = DataStructures.OrderedDict(k => k ∈ to_optimize ? pb[to_optimize[findfirst(k .== to_optimize)]].value : pb0[k].value for k in string.(keys(pb0)))
# new: 
# if in to_optimize, get from pb, otherwise get from pb0
# problem: pb and pb0 might not have all the parameters, so need to add them in from pb00
# alternative: start with pb00, then overwrite with pb0, then overwrite with pb
# NOTE: for now, assume that pb0 WILL have all of the parameters

# read in from pb00: original parameters
pb = DataStructures.OrderedDict(k => (k ∈ to_optimize) & (k ∈ string.(keys(pb))) ? pb[to_optimize[findfirst(k .== to_optimize)]].value : pb00[k].value for k in string.(keys(pb00)))

# set topics to zero
topic_pars = String.(par_init0.parameter)[occursin.(r"topic", String.(par_init0.parameter))]
pb = DataStructures.OrderedDict(k => k ∈ topic_pars ? 0.0 : pb[k] for k in keys(pb))



## set initial parameter vector
x0 = [pb[k] for k in to_optimize]

## set SD of noise using diff from best to init
# change from old initial to newest changes
x_diff = [abs(pb[k] - pb0[k].value) for k ∈ to_optimize] ./ 25
x_diff = [max(x, 0.005) for x in x_diff]  # set floor
x_diff = [min(x, 0.1) for x in x_diff]  # set ceiling
# for parameters not to optimize, set diff to 0

# FIXME: add back
# x_diff = [x_diff; zeros(length(to_add))]

## generate initial candidates
rng = Random.MersenneTwister(14592150)
# length here can't be of to_optimize, since not always all parameters
init_pop = Random.randn(rng, Float64, (length(to_optimize), 49)) .* x_diff .+ x0 

init_pop = min.(init_pop, ub)
init_pop = max.(init_pop, lb)

init_pop = [x0 init_pop]

## function to optimize - calls news_obj and saves params / fval to csv
function f0(x)
    # wrap param vector in OrderedDict
    # pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? x[findfirst(k .== to_optimize)] : pb[k].value for k in keys(pb))
    pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? x[findfirst(k .== to_optimize)] : pb[k] for k in keys(pb))
    # wrap in Eval object
    ev = SMM.Eval(mprob, pb_val)
    
    # call objective
    obj = news_obj(ev; dt=stbdat)

    # save parameters tested
    outrow = Dict(Symbol(to_optimize[i]) => x[i] for i = 1:length(to_optimize))
    # following guidance here: 
    # https://discourse.julialang.org/t/how-to-correctly-define-and-use-global-variables-in-the-module-in-julia/65720
    n_func_evals[] = n_func_evals[] + 1.0
    outrow[Symbol("n_func_evals")] = n_func_evals[]
    outrow[Symbol("fval")] = obj.value
    output_df = DataFrames.DataFrame(;outrow...)
    CSV.write("$output_dir/parameter_tracker.csv", output_df; append=true)
    obj.value
end
# end

@time f0(x0)

#### SET UP CSV TO SAVE PARAMETERS TESTED ####
println("last obj value")
last_obj_val = f0(x0)
println(last_obj_val)
println("saving initital parameter_tracker and bboptim_progress")
outrow = Dict(Symbol(to_optimize[i]) => x0[i] for i = 1:length(to_optimize))
outrow[Symbol("n_func_evals")] = 0
outrow[Symbol("fval")] = last_obj_val
output_df = DataFrames.DataFrame(;outrow...)
CSV.write("$output_dir/parameter_tracker.csv", output_df)

println("start of new obj values")

to = TimerOutput();
@timeit to "test" begin
#### BEGIN OPTIMIZATION ####
opt_params = Dict(:SearchSpace => BlackBoxOptim.ContinuousRectSearchSpace(lb, ub),
                  :Population => init_pop,
                  :MaxFuncEvals => 4000,
                  :TraceInterval => 3600.0,
                  :TraceMode => :verbose,
                #   :Workers => workers(),
                  :PM_η => 250.0
                  )

opt_problem, opt_params = BlackBoxOptim.setup_problem(f0, chain(BlackBoxOptim.DefaultParameters, opt_params))


optimizer = BlackBoxOptim.adaptive_de_rand_1_bin_radiuslimited(opt_problem, opt_params)

opt_ctrl = BlackBoxOptim.OptController(optimizer, opt_problem, opt_params)
BlackBoxOptim.run!(opt_ctrl)

best_x = BlackBoxOptim.best_candidate(BlackBoxOptim.lastrun(opt_ctrl))
result = DataFrames.DataFrame(parameter = to_optimize, value = best_x)

# get current date in right format
using Dates
current_date = lpad(month(today()),2,"0") * lpad(day(today()),2,"0") * last(string(year(today())),2)
result |> CSV.write("$output_dir/bboptim_best_$current_date.csv")
println("Best fval was $(BlackBoxOptim.best_fitness(BlackBoxOptim.lastrun(opt_ctrl)))")

end
TimerOutputs.todict(to)
using JSON3
nthreads = Threads.nthreads()
open("$output_dir/timer_n_threads_$nthreads.json", "w") do io
    JSON3.write(io, TimerOutputs.todict(to))
end

