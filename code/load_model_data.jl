### Loads all data necessary to evaluate objective ###
### expects variables code_dir, data_dir

## module loads
import CSV
import Tables
import Random
import Statistics
import Distributions
import StatsFuns
import StatsBase

# in pkg mode:
# add https://github.com/gregobad/SMM.jl#better_parallel
import SMM
import DataStructures
import LinearAlgebra.mul!
import Base.Threads.@threads
import ThreadsX

using FileIO
using DataFrames
# Testing
using Dates

# fix CSV bug:
# https://github.com/JuliaData/CSV.jl/issues/1056
@eval CSV.Parsers import Base.Ryu: writeshortest

## function to read coverage data by time zone
function read_coverage(tzs)
    [CSV.File(string("topic_coverage_", tz, ".csv")) |> DataFrame for tz in tzs]
end

## function to extract topic indicators 
# thresh argument controls how large weight must be
function extract_topics(df; thresh = 0.2)
    chans = df.channel |> unique |> length
    weights = select(sort(select(df, :channel, :block, r"^[1-9]"), [:block, :channel]), Not([:block, :channel]))
    for col in eachcol(weights)
        replace!(col, missing => 0)
    end
    reshape(permutedims(Matrix(weights), [2 1]), (ncol(weights), chans, :)) .>= thresh
end

## function to extract topic weights 
function extract_topic_weights(df)
    chans = df.channel |> unique |> length
    weights = select(sort(select(df, :channel, :block, r"^[1-9]"), [:block, :channel]), Not([:block, :channel]))
    for col in eachcol(weights)
        replace!(col, missing => 0.0)
    end
    reshape(permutedims(Matrix(weights), [2 1]), (ncol(weights), chans, :))
end

## function to extract timezone / daypart indicators
function extract_tz_daypart(df)
    convert(Array{String}, sort(unstack(df[!,[:block, :channel, :tz_daypart]], :channel, :tz_daypart), :block)[:,2])
end

## function to extract shows
function extract_show(df)
    permutedims(Matrix(select(sort(unstack(df[!,[:block, :channel, :show]], :channel, :show), :block), Not(:block))), [2 1])
end

function demean!(A::AbstractArray)
	A .-= Statistics.mean(A)
end


## data structure containing objective function arguments
# note: don't have linebreaks here, causes it to break on server for some reason
struct STBData
    ## number of channels, days, events, etc.
    B::Int64    # n blocks per day
    C::Int64    # n channels
    T::Int64    # n total blocks
    K::Int64    # n topics
    D::Int64    # n days
    N::Int64    # n simulated viewers
    network::BitVector        # vector of length C with indicator for channel c is broadcast network
    ## topic characteristics
    topic_r_hat::Vector{Float64}
    topic_freq::Vector{Float64}
    ## show array
    show_array::Array{Int,3}
    ## topic coverage arrays
    topic_array::Array{Float64,4}
    ## time-zone / daypart array
    tz_array::Array{Int,2}
    ## free signals 
    free_signals::BitMatrix
    ## consumer attributes (tz, state, DMA, R voting prob)
    consumers::DataFrame
    ## matrix to aggregate ratings to DMA-tercile level
    dma_aggregator::Array{Float64,2}
    ## matrix to aggregate votes to state level
    state_aggregator::Array{Float64,2}
    ## matrix to aggregate ratings to timezone level
    tz_aggregator::Array{Float64,2}
    ## number of blocks included in each daypart average
    blocks_per_daypart::Vector{Int}
    ## random draws for simulating
    consumer_choice_draws::Array{Float64,2}
    pre_consumer_news_draws::Array{Float64}
    pre_consumer_news_zeros::Array{Float64}
    ## symbols to access parts of the parameter vector
    keys_alpha_0::Array{Symbol,1}
    keys_alpha_r::Array{Symbol,1}
    keys_alpha_news::Array{Symbol,1}
    keys_beta_channel::Array{Symbol,1}
    keys_beta_time::Array{Symbol,1}
    keys_beta_leisure::Array{Symbol,1}
    keys_beta_base_slant::Array{Symbol,1}
end

### LOAD OBJECTIVE ###
cd(code_dir)

include("viewer_utility.jl")
include("viewer_inference.jl")
include("reporting.jl")
include("news_objective.jl")
include("algoSTB.jl")

cd(data_dir)

## events
const topics = CSV.File("topics.csv") |> DataFrame
topic_nums = topics.topic |> unique
topic_inds = [findfirst(t .== topic_nums) for t in topics.topic]
topics.topic_partisanship = (topics.word_partisanship_std .* (topics.pol_topic .> 0))
topic_partisanship = topics.topic_partisanship[[findfirst(topics.topic .== t) for t in topic_nums]]
const K = topic_nums |> length

## time zones (schedule can differ by time zone)
const time_zones = ["ETZ","CTZ", "MTZ", "PTZ"]

## read coverage matrices, reshape
channel_topic_and_show = read_coverage(time_zones)

## limit input dates here if desired
if (length(dates_to_keep) > 0)
    replace!(x -> filter(:air_date => d -> d ∈ dates_to_keep,x), channel_event_and_show)
end

## time dimensions
const T = channel_topic_and_show[1].block |> unique |> length
prog_dates = channel_topic_and_show[1].date |> unique |> sort
const D = prog_dates |> length 
const B = convert(Int64, T / D)

## channels in choice set
chans = channel_topic_and_show[1].channel |> unique |> sort
network = (chans .== "ABC") .| (chans .== "CBS") .| (chans .== "NBC")
const C = length(chans)

## free signals
free_signals = BitMatrix(undef, K, D)
free_signals .= false
for i = 1:nrow(topics)
    k = topic_inds[i]
    ds = map(d -> findfirst(d .== prog_dates), [d1 for d1 in prog_dates if (d1 >= topics.begin[i]) & (d1 < topics.end[i])])
    free_signals[k, ds] .= true
end

topic_freq = vec(sum(free_signals; dims = 2) ./ D)

## extract daypart, shows and topics, convert to arrays
# tz-daypart matrix is T x n_TZ
tz_hour = hcat(map(extract_tz_daypart, channel_topic_and_show)...)
levels_tzhour = unique(tz_hour)
tz_hour = map(x -> findfirst(x .== levels_tzhour), tz_hour)

blocks_per_daypart = [sum(tz_hour .== x) for x in unique(tz_hour)]

# show array is C x T x n_TZ
channel_show = cat(map(extract_show, channel_topic_and_show)..., dims = 3)
levels_show = unique(channel_show)
channel_show = map(x -> findfirst(x .== levels_show), channel_show)
network_nonnews = findall(occursin.(r"^(ABC|CBS|NBC)_default", levels_show))
channel_show[findall(channel_show .∈ [network_nonnews])] .= 0

# topic array is K x C x T x n_TZ
channel_topic = cat(map(ts -> extract_topic_weights(ts), channel_topic_and_show)..., dims = 4)

## de-mean each topic by channel
for k in 1:K
    for c in 1:C
        if network[c]
            @views demean!(channel_topic[k,c,findall(channel_show[c,:,:] .!= 0)])
        else
            @views demean!(channel_topic[k,c,:,:])
        end
    end
end

# ## de-mean each topic by tz-daypart
# isnews = findall(channel_show .!= 0)

# for b in 1:length(levels_tzhour)
#     this_tzhour = vcat([CartesianIndex.(c, findall(tz_hour .== b)) for c in 1:C]...)
#     for k in 1:K
#         @views demean!(channel_topic[k, intersect(isnews, this_tzhour)])
#     end
# end

    

## read household data (tz, state, DMA, R voting prob, DMA tercile)
sim_hh = CSV.File("sim_hh_sample.csv")  |> DataFrame;
filter!(:timezone => zone -> zone ∈ time_zones, sim_hh)
sort!(sim_hh, [order(:timezone), order(:state), order(:dma), order(:r_prob)])


const N = nrow(sim_hh)
sim_hh.vote = zeros(Float64, N)
sim_hh.tz = map(x -> findfirst(x .== time_zones), sim_hh.timezone)


# aggregator matrix for DMA terciles 
dma_aggregator = zeros(3,N)
for terc in unique(sim_hh.tercile)
    if (terc > 0)
        dma_aggregator[terc, findall(sim_hh.tercile .== terc)] .= 1 / length(findall(sim_hh.tercile .== terc))
    end
end

# aggregator matrix for states
states = sort(unique(sim_hh.state))
state_aggregator = zeros(N,length(states))
for st in states
    state_aggregator[findall(sim_hh.state .== st), findfirst(st .== states)] .= 1 / length(findall(sim_hh.state .== st))
end

# aggregator matrix for timezones
tz_aggregator = zeros(N, length(time_zones))
for tz in time_zones
    tz_aggregator[findall(sim_hh.timezone .== tz), findfirst(tz .== time_zones)] .= 1 / length(findall(sim_hh.timezone .== tz))
end


## read viewership data
viewership = CSV.File("nielsen_ratings.csv") |> DataFrame
viewership_se = CSV.File("nielsen_rating_ses.csv") |> DataFrame

filter!(:date => d -> d ∈ prog_dates, viewership)
filter!(:date => d -> d ∈ prog_dates, viewership_se)

sort!(viewership, [order(:date), order(:block), order(:tercile)])
sort!(viewership_se, [order(:date), order(:block), order(:tercile)])
ratings_cols = [columnindex(viewership, c) for c in Symbol.(uppercase.(chans))]

viewership_names = stack(viewership, ratings_cols; variable_name=:channel)
sort!(viewership_names, [order(:date), order(:block), order(:tercile), order(:channel)])
viewership_names.name = viewership_names.channel .* "_" .* string.(viewership_names.date) .* "_b" .* string.(viewership_names.block) .* "_t" .* string.(viewership_names.tercile)


## read timezone averages
viewership_tz_daypart = CSV.File("daypart_ratings.csv")  |> DataFrame


## read polls
polling = CSV.File("polling.csv") |> DataFrame
filter!(:date => x -> x ∈ prog_dates, polling)

const election_day = length(unique(polling.date))
sort!(polling, [order(:date), order(:state)])

## read individual moments
viewership_indiv_rawmoments = CSV.File("viewership_indiv_rawmoments.csv")  |> DataFrame

## reporting likelihood
likelihood_moment = CSV.File("reporting_likelihood_moment.csv")  |> DataFrame


## simulated draws
rng = Random.MersenneTwister(72151835); 

const consumer_choice_draws = Random.rand(rng, N, T);
const pre_consumer_news_draws = Random.randn(rng, N, 1);
const pre_consumer_news_zeros = Random.rand(rng, N, 1);

const data_moments = cat(
    viewership_indiv_rawmoments[:, :value],     # concentration moments
    vec(permutedims(viewership[:,ratings_cols] |> Tables.Matrix, [2 1])),      # tercile-block ratings (nielsen data)
    viewership_tz_daypart[:,:mean_rating];                   
    dims = 1
);

## weighting by 1 / (var of the moment in data plus var of moment in sims)

var_pop = cat(
    viewership_indiv_rawmoments[:, :se] .^ 2,
    vec(permutedims(viewership_se[:,ratings_cols] |> Tables.Matrix, [2 1])) .^ 2,
    viewership_tz_daypart[:,:se_rating] .^ 2;
    dims = 1
)

# count of sims by state for polling errors
gsim_hh = groupby(sim_hh, :state)
sims_by_state = combine(gsim_hh, :vote => length => :nsim)

polling = leftjoin(polling, sims_by_state, on = :state)
sort!(polling, [order(:date), order(:state)])

polling.simse = polling.poll .* (1 .- polling.poll) ./ polling.nsim


# count of sims by tercile for block ratings errors
gsim_hh = groupby(sim_hh, :tercile)
sims_by_terc = combine(gsim_hh, :vote => length => :nsim)
filter!(:tercile => x -> x > 0, sims_by_terc)

# count of sims by tz for timezone ratings errors
gsim_hh = groupby(sim_hh, :timezone)
sims_by_tz = combine(gsim_hh, :vote => length => :nsim)

viewership = leftjoin(viewership, sims_by_terc, on = :tercile)
sort!(viewership, [order(:date), order(:block), order(:tercile)])
view_nsim = repeat(viewership.nsim; inner = C)
tz_nsim = repeat(sims_by_tz, inner = 6)

var_sim = cat(
    viewership_indiv_rawmoments[:, :value] .* (1 .- viewership_indiv_rawmoments[:, :value]) ./ (N .* T),
    vec(permutedims(viewership[:,ratings_cols] |> Tables.Matrix, [2 1])) .* (1 .- vec(permutedims(viewership[:,ratings_cols] |> Tables.Matrix, [2 1]))) ./ view_nsim, 
    viewership_tz_daypart[:,:mean_rating] .* (1 .- viewership_tz_daypart[:,:mean_rating]) ./ (tz_nsim.nsim .* blocks_per_daypart);
    dims = 1
)

w = 1 ./ (var_pop .+ var_sim);

w1 = sum(w[1:nrow(viewership_indiv_rawmoments)])
w2 = sum(w[(nrow(viewership_indiv_rawmoments)+1):(nrow(viewership_indiv_rawmoments) + nrow(viewership) * C)])
w3 = sum(w[(nrow(viewership_indiv_rawmoments) + nrow(viewership) * C +1):(nrow(viewership_indiv_rawmoments) + nrow(viewership) * C + nrow(viewership_tz_daypart))])


mom_scale = cat(
    [1 / w1 for i in 1:nrow(viewership_indiv_rawmoments)],
    [1 / w2 for i in 1:(nrow(viewership) * C)],
    [1 / w3 for i in 1:nrow(viewership_tz_daypart)];
    dims = 1
)

w .*= mom_scale



## equal weighted version
# w = ones(Float64, length(data_moments));



## put data moments and weights into data frame
moms = DataFrames.DataFrame(
    name = vcat(
        viewership_indiv_rawmoments[:, :stat],
        viewership_names.name,
        "tz_" .* viewership_tz_daypart.tz_daypart
    ),
    value = data_moments,
    weight = w,
)

# CSV.write("$data_dir/moments_test.csv", moms)


## read parameter definition file
cd(data_dir)
par_bounds = CSV.File("parameter_bounds.csv"; delim=',') |> DataFrame
par_bounds.ub = Float64.(par_bounds.ub);
par_bounds.lb = Float64.(par_bounds.lb);

## READ PARAMETER VECTOR ##
# # to read from last MCMC run:
# @everywhere cd(output_dir)
# @everywhere par_init = CSV.File("MCMC_chain1_20days.csv") |> DataFrame;
# @everywhere cd(code_dir)
# @everywhere include("read_par_from_mcmc.jl")

# to read direct from csv:
par_init = CSV.File("parameter_init.csv") |> DataFrame

# join with the bounds definition
par_init = innerjoin(par_init[:,[:parameter,:value]], par_bounds, on=:parameter)

## correctly order parameters in par_init 
par_init[!, :parorder] .= 0

par_init[occursin.(r"topic", par_init.parameter), :parorder] = parse.(Int64, replace.(par_init[occursin.(r"topic", par_init.parameter), :parameter], r"beta:topic:" => s"")) .+ 4
par_init[occursin.(r"ETZ", par_init.parameter), :parorder] .= 1
par_init[occursin.(r"CTZ", par_init.parameter), :parorder] .= 2
par_init[occursin.(r"MTZ", par_init.parameter), :parorder] .= 3
par_init[occursin.(r"PTZ", par_init.parameter), :parorder] .= 4

sort!(par_init, [:parorder, :parameter])

# create dictionary indexed by parameter, with initial values and bounds for each
pb = DataStructures.OrderedDict(zip(
    par_init.parameter,
    DataFrames.eachrow(par_init[:, [:value, :lb, :ub]]),
))


## read sampling weights files (for MCMC)
cd(sampling_dir)
sampling_files = readdir(pwd())
sampling_files = sampling_files[occursin.(r"sample_tree", sampling_files)]
sampling_tree_nodes = [m.captures[1] for m in match.(r"sample_tree_?(\w*)\.csv",sampling_files)]

# remove nodes not indicated by tree_base
deleteat!(sampling_files, [i for i=1:length(sampling_tree_nodes) if !occursin(r"^" * tree_base, sampling_tree_nodes[i])])
deleteat!(sampling_tree_nodes, [i for i=1:length(sampling_tree_nodes) if !occursin(r"^" * tree_base, sampling_tree_nodes[i])])

sampling_file_dict = Dict(zip(sampling_tree_nodes, sampling_files))

tree_depth = length.(split.(sampling_tree_nodes, "_"))
base_node = findfirst(tree_depth.==1)
sample_tree = read_node(sampling_file_dict, sampling_files[base_node], 1.0, tree_base, par_bounds.parameter)

## construct data object to pass to objective function
stbdat = STBData(
    B,
    C,
    T,
    K,
    D,
    N,
    network,
    topic_partisanship,
    topic_freq,
    channel_show,
    channel_topic,
    tz_hour,
    free_signals,
    sim_hh,
    dma_aggregator,
    state_aggregator,
    tz_aggregator,
    blocks_per_daypart,
    consumer_choice_draws,
    pre_consumer_news_draws,
    pre_consumer_news_zeros,
    Symbol.([k for k in par_init.parameter if occursin("alpha:0", k)]),
    Symbol.([k for k in par_init.parameter if occursin("alpha:r", k)]),
    Symbol.([k for k in par_init.parameter if occursin("alpha:eta", k)]),
    Symbol.([k for k in par_init.parameter if occursin("beta:channel", k)]),
    Symbol.([k for k in par_init.parameter if occursin("beta:time", k)]),
    Symbol.([k for k in par_init.parameter if occursin("beta:topic", k)]),
    Symbol.([k for k in par_init.parameter if occursin("beta:base_slant", k)])
);


# Initialize an empty MProb() object:
#------------------------------------
mprob = SMM.MProb();

# Add moments to be matched to MProb():
#--------------------------------------
SMM.addMoment!(mprob, moms);

# Attach an objective function to MProb():
#----------------------------------------
SMM.addEvalFunc!(mprob, news_obj);
SMM.addEvalFuncOpts!(mprob, Dict(:dt => stbdat));
