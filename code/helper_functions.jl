function rep_row(x,n)
	x[ones(Int,n),:]
end

function rep_col(x,n)
	x[:,ones(Int,n)]
end

function norm_cols(A::Matrix)
	A ./ sum(A,dims=1);
end

function norm_rows(A::Matrix)
	A ./ sum(A,dims=2);
end

function demean_cols(A::Matrix)
	A .- mapslices(Statistics.mean, A, dims=1);
end

function demean_rows(A::Matrix)
	A .- mapslices(Statistics.mean, A, dims=2);
end

function simulate_path(x0::Matrix, innov::Matrix)
	cumsum(innov, dims=2) .+ x0;
end

function index_into!(A_out::Array{T,N1}, A_in::Array{T,N2}, indices::Vararg{Array{Int,N1},N2}) where {T,N1,N2}
	for i in 1:length(A_out)
		A_out[i] = A_in[map(x->x[i], indices)...];
	end
	nothing
end

function index_into!(A_out::Matrix{T}, A_in::Matrix{T}, indices::Vararg{Array{Int,2},2}) where {T}
	i_1, i_2 = indices;
	for i in 1:length(A_out)
		A_out[i] = A_in[i_1[i],i_2[i]];
	end
	nothing
end

function index_into!(A_out::Matrix{T}, A_in::Array{T,3}, indices::Vararg{Array{Int,2},3}) where {T}
	i_1, i_2, i_3 = indices;
	for i in 1:length(A_out)
		A_out[i] = A_in[i_1[i],i_2[i],i_3[i]];
	end
	nothing
end


function abs!(x :: AbstractVector)
	for i in length(x)
		x[i] = abs(x[i])
	end
end

function colmeans!(s::AbstractVector{T}, A::AbstractMatrix{T}) where T <: Real
	@views for j in 1:size(A,2)
		s[j] = sum(A[:,j]) / size(A,1)
	end
end


function get_sim_ratings(ev::SMM.Eval, channel, tercile)
	rx = Regex(channel * ".*_t" * string(tercile))
	all_labels = string.(collect(keys(SMM.dataMomentd(ev))))
	selected_labels = all_labels[occursin.(rx, all_labels)]
	[ev.simMoments[Symbol(i)] for i in selected_labels]
end

function get_sim_polls(ev::SMM.Eval, state)
	rx = Regex("poll_" * state)
	all_labels = string.(collect(keys(SMM.dataMomentd(ev))))
	selected_labels = all_labels[occursin.(rx, all_labels)]
	[ev.simMoments[Symbol(i)] for i in selected_labels]
end


function get_data_ratings(ev::SMM.Eval, channel, tercile)
	rx = Regex(channel * ".*_t" * string(tercile))
	all_labels = string.(collect(keys(SMM.dataMomentd(ev))))
	selected_labels = all_labels[occursin.(rx, all_labels)]
	[SMM.dataMomentd(ev)[Symbol(i)] for i in selected_labels]
end

function get_data_polls(ev::SMM.Eval, state)
	rx = Regex("poll_" * state)
	all_labels = string.(collect(keys(SMM.dataMomentd(ev))))
	selected_labels = all_labels[occursin.(rx, all_labels)]
	[SMM.dataMomentd(ev)[Symbol(i)] for i in selected_labels]
end


function params_from_csv(f, par_bounds)
	# optionally, load whatever the latest parameter file is automatically
	if f=="latest"
		best_param_files = readdir(output_dir)[occursin.(r"bboptim_best_", readdir(output_dir))]
		# best_param_dates = Dates.Date.(first.(last.(best_param_files, 10),6),dateformat"mmddyy")
		best_param_dates = Dates.Date.(first.(last.(best_param_files, 10),6),"mmddyy")
		most_recent_best_param_file_name = best_param_files[findmax(best_param_dates)[2]]
		most_recent_best_param_file = "$output_dir/$most_recent_best_param_file_name"
		f = most_recent_best_param_file
	end

	par_init = CSV.File(f) |> DataFrame

	# join with the bounds definition
	par_init = innerjoin(par_init[:,[:parameter,:value]], par_bounds, on=:parameter)

	## correctly order parameters in par_init 
	par_init[!, :parorder] .= 0

	par_init[occursin.(r"topic", par_init.parameter), :parorder] = parse.(Int64, replace.(par_init[occursin.(r"topic", par_init.parameter), :parameter], r"beta:topic:" => s"")) .+ 4
	par_init[occursin.(r"ETZ", par_init.parameter), :parorder] .= 1
	par_init[occursin.(r"CTZ", par_init.parameter), :parorder] .= 2
	par_init[occursin.(r"MTZ", par_init.parameter), :parorder] .= 3
	par_init[occursin.(r"PTZ", par_init.parameter), :parorder] .= 4

	sort!(par_init, [:parorder, :parameter])

	# create dictionary indexed by parameter, with initial values and bounds for each
	pb = DataStructures.OrderedDict(zip(
		par_init.parameter,
		DataFrames.eachrow(par_init[:, [:value, :lb, :ub]]),
	))

	return pb, par_init

end