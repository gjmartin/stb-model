include("setup.jl")

# define subset of pars to optimize over
to_optimize = String.(par_init00.parameter)  ## everything
inds_to_ignore =  Ref(findall(occursin.(r"alpha|beta:info|topic", to_optimize)))
pb, pb0, to_optimize = load_and_update_parameters(output_dir, data_dir, to_optimize, par_bounds, inds_to_ignore)

# add pars to mprob object
SMM.addSampledParam!(mprob,pb0);
ev = SMM.Eval(mprob, pb);

res = news_obj(ev; dt=stbdat, store_moments=true)
res.value
# only viewership moments
# sum([(res.simMoments[k] - SMM.dataMomentd(res)[k])^2 * SMM.dataMomentWd(res)[k] for k in Symbol.(viewership_names.name)])

# read in moments, output for reading in with R
moments = stack(DataFrame(res.simMoments))
rename!(moments, [:moment, :value])
# output moments
CSV.write(@sprintf("%s/current_moments.csv", data_dir), moments)


# last_best_pars = [2.88897, 2.74653, 2.50225, 2.71293, 0.780592, 0.248529, -0.82662, -2.13546, -2.18095, -4.51894, 0.751855, -5.67371, -1.22065, 1.49104, 1.0491, 0.292353, 0.115406, 0.0810208, 0.122364, 0.348799, 0.930316, -0.747151, 0.192967, 1.18432, -0.5996, -0.99913, 0.506888, 0.332368, -0.276262, -0.484701, -0.721824, -1.63378, -0.987026, 1.72752, -2.56604, -1.5236, 2.85911, -1.24157, 0.4325, 0.439767, -0.16517, -2.81189, -4.9828, -0.168397]

# function load_best_pars_into_pb(pb, last_best_pars, to_optimize)
#     pb_keys = keys(pb)
#     pb_val = DataStructures.OrderedDict()
    
#     last_best_pars_index = 1
#     for key in pb_keys
#         if key ∈ to_optimize
#             pb_val[key] = last_best_pars[last_best_pars_index]
#             last_best_pars_index += 1
#         else
#             pb_val[key] = pb[key]
#         end
#     end

#     return pb_val
# end


# pb_val = load_best_pars_into_pb(pb, last_best_pars, to_optimize)