## Set your local directory
local_dir = "/home/users/gjmartin"

# local_dir = "/Users/mitchelllinegar/Code"
# stb_dir = "stb-model-event"
stb_dir = "stb-model"
# set to something nonempty to limit dates used in estimation
dates_to_keep = []  


# to sample only beta
# tree_base = "beta"

# # to sample all
tree_base = ""

### END OPTIONS ###

## Directory locations for code and data
import Dates
using Surrogates
using SurrogatesRandomForest
using Serialization

code_dir = "$local_dir/$stb_dir/code"
data_dir = "$local_dir/$stb_dir/data"
output_dir = "$local_dir/$stb_dir/output"
sampling_dir = "$local_dir/$stb_dir/sampling"


## LOAD OBJECTIVE, DATA, INITIAL PARAMETERS ##
println("Initializing...")
include("$code_dir/load_model_data.jl")
include("$code_dir/helper_functions.jl")


# add pars to mprob object
SMM.addSampledParam!(mprob,pb);

ev = SMM.Eval(mprob)

# ## uncomment to read in trial evals generated from diff evo optimizer
# trials = CSV.File("$output_dir/parameter_tracker.csv")  |> DataFrame;
# to_optimize = intersect(names(trials), par_init.parameter)


to_optimize = ["beta:info",
               "beta:slant",
               "beta:zero",
               "beta:const",
               "beta:mu",
               "beta:sigma",
               "beta:base_slant:ABC",
               "beta:base_slant:CBS",
               "beta:base_slant:CNN",
               "beta:base_slant:FNC",
               "beta:base_slant:MSNBC",
               "beta:base_slant:NBC",
               "beta:channel:CBS",
               "beta:channel:CNN",
               "beta:channel:FNC",
               "beta:channel:MSNBC",
               "beta:channel:NBC",
               "rho0:0",
               "rho0:1"
               ]


ub = [par_init[findfirst(par_init.parameter .== p), :ub] for p in to_optimize]
lb = [par_init[findfirst(par_init.parameter .== p), :lb] for p in to_optimize]




# poly = SecondOrderPolynomialSurrogate(
#   xs,
#   ys,
#   lb,
#   ub)

## uncomment to use previous run as starting point
println("Reading start point from file $output_dir/surrogate_optimizer_randomforest.dat")

rf = deserialize("$output_dir/surrogate_optimizer_randomforest.dat")

## update for 2x size version

function f(x)
  pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? x[findfirst(k .== to_optimize)] : pb[k].value for k in keys(pb))
  # wrap in Eval object
  ev = SMM.Eval(mprob, pb_val)
  val = news_obj(ev; dt=stbdat, store_moments=false).value
  # [merge((; zip(Symbol.(to_optimize), x)...), (fv = val,))] |> CSV.write("$output_dir/fval_samples_randomforest_lcbs.csv"; append = true)
  return val
end

rf.y .= f.(rf.x)

unacceptable = findall(rf.y .> 300)

deleteat!(rf.x, unacceptable)
deleteat!(rf.y, unacceptable)

add_point!(rf, empty(rf.x), empty(rf.y))
serialize("$output_dir/surrogate_optimizer_randomforest.dat", rf)

# ## add last run that didn't get saved
# println("Reading additional trial samples from $output_dir/fval_samples_randomforest_lcbs.csv")
# trials = CSV.File("$output_dir/fval_samples_randomforest_lcbs.csv") |> DataFrame |> unique
# filter!(:fv => x -> x <= 0.85, trials)
# addl_xs = Tuple.(eachrow(trials[:, to_optimize]));
# addl_ys = trials.fv;

# new = [i for i in 1:length(addl_xs) if addl_xs[i] ∉ rf.x]

# add_point!(rf, addl_xs[new], addl_ys[new])


# new_xs = sample(2000,lb,ub,SobolSample())
# append!(xs, new_xs)
# new_ys = f.(new_xs)
# append!(ys, new_ys)



println("Best fval before optimization = $(minimum(rf.y)), of $(length(rf.y)) function evaluations.")


## main loop
for _ in 1:10
  surrogate_optimize(f, SRBF(), lb, ub, rf, SobolSample(), maxiters = 1000, num_new_samples = 50000)
  println("Best fval = $(minimum(rf.y)), after $(length(rf.x)) function evaluations total.")
  
  unacc = findall(rf.y .> 300)

  deleteat!(rf.x, unacc)
  deleteat!(rf.y, unacc)
  add_point!(rf, empty(rf.x), empty(rf.y))

  serialize("$output_dir/surrogate_optimizer_randomforest.dat", rf)
end