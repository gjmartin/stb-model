# why do some states have no change in ideology?

# to run this: 
# 1. run estimation_driver up to line 269
# 2. run news_objective 

using StatsPlots
using Statistics


# scratch: daily state-level viewership for each channel (add to news_objective before t+=1)
# v = zeros(C)
# for c in 1:C
#     v[c] = sum(view_choice[c,:] .* dt.state_aggregator'[47,:])
# end
# v

#### NOTE ####
# to run, make sure daily_polling is in memory from news_objective
# state-level polling over time
daily_polling
# some states with no change: state 1, 49
# some states with some change: state 4, 47

# why do some states have no change in ideology?

# hypothesis: states with changes in polling initially had some agents close to 0.5 initially, and this accounts for the drift
# states with no change had fewer agents close to 0.5 initially

# state individual indices
function sind(state)
    dt.state_aggregator'[state,:] .> 0
end

swing_voter_inds = μ[:,1] .> 0.4 .&& μ[:,1] .< 0.6

# for a given state, individual ideology over time
μ[sind(1),:]
μ[sind(4),:]

function ideology_change(ind)
    μ[ind,1] - μ[ind,204]
end
Statistics.mean(ideology_change(sind(1)))
Statistics.mean(ideology_change(sind(4)))
Statistics.mean(ideology_change(:))
Statistics.mean(ideology_change(swing_voter_inds))
# count number of viewers that change their vote from first day to last
function count_vote_changes(state)
    inds = sind(state)
    abs(sum(μ[inds,1] .> 0.5) - sum(μ[inds,204] .> 0.5))
end

count_vote_changes(1)
count_vote_changes(4)
count_vote_changes(49)
count_vote_changes(47)

# count number of "swing" voters
function count_swing_voters(state)
    inds = sind(state)
    sum(μ[inds,1] .> 0.4 .&& μ[inds,1] .< 0.6)
end
count_swing_voters(4)

for s in 1:49
    println(@sprintf("state %s: number of voters: %s; number of swing voters: %s; number of voting changes: %s \n", s, sum(sind(s)), count_swing_voters(s), count_vote_changes(s)))
end
# plot viewership density across states

# hypothesis: states with changes in polling had some agents close to 0.5 initially, and this accounts for the drift
# states with no change had fewer agents close to 0.5 initially
function compare_state_start_end_ideology(state1, state2)
    density(μ[sind(state1),1], label = @sprintf("state %s start", state1))
    density!(μ[sind(state1),204], label = @sprintf("state %s end", state1))
    density!(μ[sind(state2),1], label = @sprintf("state %s start", state2))
    density!(μ[sind(state2),204], label = @sprintf("state %s end", state2))
    vline!([0.5], label = "neutral voter")
end

compare_state_start_end_ideology(49, 47)
compare_state_start_end_ideology(1, 4)
compare_state_start_end_ideology(4, 47)

# thus far: states with changes tend to have more viewers around 0.5 than states with no changes
# this is only anecdotal, but seems like the most likely explanation

# plot individual evolution of ideology over time
function plot_individual_ideology(state)
    plot(1:204, μ[sind(state),:][1,:], 
        legend=false,
        title = @sprintf("Individual Ideology Evolution for State %s", state),
        xlabel = "Day",
        ylabel = "Ideology"
    )
    for i in 2:size(μ[sind(state),:],1)
        plot!(1:204, μ[sind(state),:][i,:], legend=false)
    end
    current()
end
plot_individual_ideology(1)
plot_individual_ideology(4)

# something else to check: are states with changes larger? 
