## Set your local directory
# local_dir = "/home/greg"
# local_dir = "/Users/mitchelllinegar/Code"
# local_dir = "/Users/mlinegar/Code"
# local_dir = "/home/users/mlinegar/stb"
## Set your local directory
if occursin("greg", pwd())
    local_dir = "/home/greg"
elseif occursin("users", pwd())
    local_dir = "/home/users/mlinegar/stb"
else
    local_dir = "/Users/mlinegar/Code"
end
# stb_dir = "stb-model-event"
stb_dir = "stb-model"
# set to something nonempty to limit dates used in estimation
dates_to_keep = []  

# to sample only beta
# tree_base = "beta"

# # to sample all
tree_base = ""

### END OPTIONS ###

## Directory locations for code and data
using CSV
import Dates
using FileIO
using Plots
using Printf
using Optimization, ForwardDiff, OptimizationOptimJL

code_dir = "$local_dir/$stb_dir/code"
data_dir = "$local_dir/$stb_dir/data"
output_dir = "$local_dir/$stb_dir/output"
sampling_dir = "$local_dir/$stb_dir/sampling"
grid_dir = "$data_dir/grid_obj"

## LOAD OBJECTIVE, DATA, PARAMETERS ##
include("$code_dir/load_model_data.jl")
include("$code_dir/helper_functions.jl")

# USE LAST RUN INSTEAD OF INITIAL PARAMETERS
# pb, par_init = params_from_csv("$output_dir/bboptim_best_112822.csv", par_bounds)
pb, par_init = params_from_csv("latest", par_bounds)

# join with the bounds definition
par_init = innerjoin(par_init[:,[:parameter,:value]], par_bounds, on=:parameter)

## correctly order parameters in par_init 
par_init[!, :parorder] .= 0

par_init[occursin.(r"topic", par_init.parameter), :parorder] = parse.(Int64, replace.(par_init[occursin.(r"topic", par_init.parameter), :parameter], r"beta:topic:" => s""))
par_init[occursin.(r"ETZ", par_init.parameter), :parorder] .= 1
par_init[occursin.(r"CTZ", par_init.parameter), :parorder] .= 2
par_init[occursin.(r"MTZ", par_init.parameter), :parorder] .= 3
par_init[occursin.(r"PTZ", par_init.parameter), :parorder] .= 4

sort!(par_init, [:parorder, :parameter])

# create dictionary indexed by parameter, with initial values and bounds for each
pb = DataStructures.OrderedDict(zip(
    par_init.parameter,
    DataFrames.eachrow(par_init[:, [:value, :lb, :ub]]),
))


# add pars to mprob object
SMM.addSampledParam!(mprob,pb);

ev = SMM.Eval(mprob)


# to_optimize = filter(x -> .!(occursin.(r"alpha", x)), String.(par_init.parameter)) 
to_optimize = ["beta:info"]
# TEMPORARY, REMOVE AFTER RUN
# to_optimize = to_optimize[58:98]
x0 = [par_init[findfirst(par_init.parameter .== p), :value] for p in to_optimize]


function f0(x, var; store_moments=false)
    # have to coerce single input to var to vector
    typeof(var)==String ? var = [var] : var = var
    # wrap param vector in OrderedDict
    pb_val = DataStructures.OrderedDict(k => k ∈ var ? x[findfirst(k .== var)] : pb[k].value for k in keys(pb))
    # wrap in Eval object
    ev = SMM.Eval(mprob, pb_val)
    news = news_obj(ev; dt=stbdat, store_moments=store_moments)
    
    if store_moments
        # return obj, moments, cumulative_view, view_by_tercile, daily_polling
        obj = news[1].value
        # moments = SMM.check_moments(news)
        moments = news[2]
        cumulative_view = news[3]
        view_by_tercile = news[4]
        daily_polling = news[5]
        viewer_slant_util = news[6]
        viewer_info_util = news[7]
        return obj, moments, cumulative_view, view_by_tercile, daily_polling, viewer_slant_util, viewer_info_util
    else
        obj = news.value
        return obj
    end
end


function obj_grid(param_str, param_multipliers; store_moments=false, save_output=false)
    
    # if want to set value by param_multipliers instead of multiplying
    param_vals = [par_init[par_init.parameter.==param_str,:lb][1]:param_multipliers[1]:par_init[par_init.parameter.==param_str,:ub][1]...]
    # initialize data storage
    param_obj_storage = zeros(Float64, length(param_vals))

    if store_moments
        moments = DataFrame(moment = String[], value = Float64[], opt_var = String[], opt_mult = Float64[])
        param_moments = copy(moments)
        param_cumulative_view_storage = zeros(6000, 6, length(param_multipliers))# 6000x6
        param_view_by_tercile_storage = zeros(6, 3, 4896, length(param_multipliers))#6x3x4896
        param_daily_polling_storage = zeros(49, 204, length(param_multipliers))#49x204
        param_viewer_slant_util = zeros(6000, 6, length(param_multipliers))
        param_viewer_info_util = zeros(6000, 6, length(param_multipliers))
        # for i in 1:length(param_multipliers)
        for i in 1:length(param_vals)
            @printf "Testing objective for param multiplier %s of %s \n" i length(param_multipliers)
            # x00=param_multipliers[i] * par_init[par_init.parameter.==param_str,:value][1]
            # x00=param_multipliers[i] * par_init[par_init.parameter.==param_str,:value][1]
            x00 = param_vals[i]

            param_obj = f0(x00, param_str, store_moments=store_moments)
            param_obj_storage[i] = param_obj[1]
            param_moment = param_obj[2]
            #
            param_cumulative_view_storage[:,:,i] = param_obj[3]
            param_view_by_tercile_storage[:,:,:,i] = param_obj[4]
            param_daily_polling_storage[:,:,i] = param_obj[5]
            #
            param_viewer_slant_util[:,:,i] = param_obj[6]
            param_viewer_info_util[:,:,i] = param_obj[7]
            #
            param_moment[!,:opt_var] .= param_str
            param_moment[!,:opt_mult] .= param_multipliers[i]
            append!(param_moments, param_moment)
        end

        if save_output
            CSV.write("$grid_dir/moments_over_$param_str.csv", param_moments)
            CSV.write("$grid_dir/obj_over_$param_str.csv", DataFrame(param_mult = param_multipliers, obj = param_obj_storage))
            save("$grid_dir/$(param_str)_cumulative_view_storage.jld2", "$(param_str)_cumulative_view_storage", param_cumulative_view_storage)
            save("$grid_dir/$(param_str)_view_by_tercile_storage.jld2", "$(param_str)_view_by_tercile_storage", param_view_by_tercile_storage)
            save("$grid_dir/$(param_str)_daily_polling_storage.jld2", "$(param_str)_daily_polling_storage", param_daily_polling_storage)

            param_tercile_cumulative_view_storage = zeros(6,3, length(param_multipliers))
            for par in 1:length(param_multipliers)
                param_tercile_cumulative_view_storage[:,:,par] = param_cumulative_view_storage[:,:,par]' * stbdat.dma_aggregator'
            end
            # reshape so can save as csv
            # dims: tercile channel * param (row), tercile (cols)

            param_tercile_cumulative_view_storage_long = reshape(permutedims(param_tercile_cumulative_view_storage, [1,3,2]), :,3)
            CSV.write("$grid_dir/$(param_str)tercile_cumulative_view_storage_long.csv", Tables.table(param_tercile_cumulative_view_storage_long))


            # save utilities 
            param_tercile_cumulative_slant_util_storage = zeros(6,3, length(param_multipliers))
            param_tercile_cumulative_info_util_storage = zeros(6,3, length(param_multipliers))
            for par in 1:length(param_multipliers)
                param_tercile_cumulative_slant_util_storage[:,:,par] = param_viewer_slant_util[:,:,par]' * stbdat.dma_aggregator'
                param_tercile_cumulative_info_util_storage[:,:,par] = param_viewer_info_util[:,:,par]' * stbdat.dma_aggregator'
            end

            param_viewer_slant_util_long = reshape(permutedims(param_tercile_cumulative_slant_util_storage, [1,3,2]), :,3)
            CSV.write("$grid_dir/$(param_str)viewer_slant_util_long.csv", Tables.table(param_viewer_slant_util_long))
            param_viewer_info_util_long = reshape(permutedims(param_tercile_cumulative_info_util_storage, [1,3,2]), :,3)
            CSV.write("$grid_dir/$(param_str)viewer_info_util_long.csv", Tables.table(param_viewer_info_util_long))
        else
            println("Not saving output!")
        end

    else
        # for i in 1:length(param_multipliers)
        for i in 1:length(param_vals)
            @printf "Testing objective for param multiplier %s of %s \n" i length(param_vals)
            # x00=param_multipliers[i] * par_init[par_init.parameter.==param_str,:value][1]
            x00 = param_vals[i]
            println("Value of $param_str used: $x00")
            param_obj = f0(x00, param_str, store_moments=store_moments)
            param_obj_storage[i] = param_obj
        end

        if save_output
            CSV.write("$grid_dir/obj_over_$(param_str).csv", DataFrame(param_mult = param_vals, obj = param_obj_storage))
        else
            println("Not saving output!")
        end
        
    end

    return DataFrame(param_mult = param_vals, obj = param_obj_storage)
end

# full_mult = [0.9; 1.1;]
# full_mult = [0.8:0.01:0.97; 0.98:0.001:0.995; 0.996:0.0001:1.004; 1.005:0.001:1.02;1.03:0.01:1.2;]
# full_mult = [0.8:0.02:0.98; 0.98:0.001:0.995; 0.996:0.0001:1.004; 1.005:0.001:1.02;1.02:0.02:1.2;]
# full_mult = [ 0.996:0.0001:1.004;]

full_mult = [500.0;]
# full_mult = [0.1;]
save_output=true

grid_pars = to_optimize
for param in grid_pars
    println("Running grid on parameter $param")
    obj_grid(param, full_mult, save_output=save_output)
end

# info_grid = obj_grid("beta:info", full_mult, save_output=save_output)
# slant_grid = obj_grid("beta:slant", full_mult, save_output=save_output)
# zero_grid = obj_grid("beta:zero", full_mult, save_output=save_output)
# rho00_grid = obj_grid("rho0:0", full_mult, save_output=save_output)
# rho01_grid = obj_grid("rho0:1", full_mult, save_output=save_output)

# base_slant_grid = obj_grid("beta:base_slant:ABC", full_mult, save_output=save_output)
# base_slant_grid = obj_grid("beta:base_slant:CBS", full_mult, save_output=save_output)
# base_slant_grid = obj_grid("beta:base_slant:CNN", full_mult, save_output=save_output)
# base_slant_grid = obj_grid("beta:base_slant:FNC", full_mult, save_output=save_output)
# base_slant_grid = obj_grid("beta:base_slant:MSNBC", full_mult, save_output=save_output)
# base_slant_grid = obj_grid("beta:base_slant:NBC", full_mult, save_output=save_output)

# # base_slant_grid = obj_grid("beta:channel:ABC", full_mult, save_output=save_output)
# base_slant_grid = obj_grid("beta:channel:CBS", full_mult, save_output=save_output)
# base_slant_grid = obj_grid("beta:channel:CNN", full_mult, save_output=save_output)
# base_slant_grid = obj_grid("beta:channel:FNC", full_mult, save_output=save_output)
# base_slant_grid = obj_grid("beta:channel:MSNBC", full_mult, save_output=save_output)
# base_slant_grid = obj_grid("beta:channel:NBC", full_mult, save_output=save_output)


# tau_grid = obj_grid("tau", full_mult, save_output=save_output)
# time_grid = obj_grid("beta:time:ETZ_M-F_7-8", full_mult, save_output=save_output)
# topic_grid1 = obj_grid("beta:topic:4", full_mult, save_output=save_output)
# topic_grid2 = obj_grid("beta:topic:49", full_mult, save_output=save_output)



# # var="beta:info"
# var="beta:slant"
# x00=param_multipliers[1] * par_init[par_init.parameter.==var,:value][1]

# typeof(var)==String ? var = [var] : var = var
# # wrap param vector in OrderedDict
# pb_val = DataStructures.OrderedDict(k => k ∈ var ? x[findfirst(k .== var)] : pb[k].value for k in keys(pb))
# pb_val[var[1]]

# pb_val["beta:info"]
# pb_val["beta:slant"]

f0()
param_str = "beta:info"
param_multipliers = [0.1;]
x00 = [1.0;]
param_obj = f0(x00, param_str, store_moments=false)

