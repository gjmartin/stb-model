# compare moments for current parameters and proposed changes in parameters
# also detail the contribution of each moment/group of moments to the objective

### SET OPTIONS ###
# set local directory
local_dir = "/Users/mitchelllinegar/Code"
# local_dir = "/home/gjmartin/Dropbox/STBNews"
stb_dir = "stb-model"
# which parameter to compare? 
param_to_change = "beta:channel:MSNBC"
# how much to add/subtract from parameter? 
add_to_param = 1.0


# LOAD BASE PACKAGES
# some packages need
using Printf
using BlackBoxOptim

# set to something nonempty to limit dates used in estimation
dates_to_keep = []  
tree_base = ""

code_dir = @sprintf("%s/%s/code", local_dir, stb_dir)
data_dir = @sprintf("%s/%s/data", local_dir, stb_dir)
output_dir = @sprintf("%s/%s/output", local_dir, stb_dir)
sampling_dir = @sprintf("%s/%s/sampling", local_dir, stb_dir)

include("$code_dir/load_model_data.jl")
include("$code_dir/helper_functions.jl")

cd(data_dir)
# if reading in current best parameters:
best_par = CSV.read("$output_dir/best_pars.csv", DataFrame);
# shorthand 
pars = best_par.parameter
rename!(best_par, [:parameter, :value])
# read in bounds
par_bounds = CSV.read("parameter_bounds.csv", DataFrame);
best_par = innerjoin(best_par, par_bounds, on = :parameter)

pb = DataStructures.OrderedDict(zip(
    pars,
    DataFrames.eachrow(best_par[:, [:value, :lb, :ub]])
));
# eliminate any learning
pb["tau"].value = 1
# add pars to mprob object
SMM.addSampledParam!(mprob,pb);
ev = SMM.Eval(mprob)

p0 = best_par[occursin.(r"beta", pars), :parameter]
p1 = best_par[occursin.(r"gamma", pars), :parameter]
p2 = best_par[occursin.(r"rho0", pars), :parameter]
p3 = best_par[occursin.(r"alpha", pars), :parameter]
to_opt = [p0; p1; p2; p3]
x0 = [pb[k].value for k in to_opt]

### DEFINE FUNCTIONS ###
# various functions to calculate influence of moments on objective  

# this function calculates the contribution of each moment to the objective
function influence(ev)
    infl = Dict(k => (ev.simMoments[k] - SMM.dataMomentd(ev)[k])^2 * SMM.dataMomentWd(ev)[k] for k in keys(ev.simMoments))
    sort(collect(infl), by = x -> x[2])
end

# this function compares the influence of each moment across two different objectives
function influence2(ev1, ev2)
    infl = Dict(k => (ev1.simMoments[k] - SMM.dataMomentd(ev1)[k])^2 * SMM.dataMomentWd(ev1)[k] - (ev2.simMoments[k] - SMM.dataMomentd(ev2)[k])^2 * SMM.dataMomentWd(ev2)[k] for k in keys(ev1.simMoments))
    sort(collect(infl), by = x -> x[2])
end

# this function calculates the total influence of a group of moments
function influence_sum(obj, moments)
    sum([(obj.simMoments[k] - SMM.dataMomentd(obj)[k])^2 * SMM.dataMomentWd(obj)[k] for k in moments])
end

# this function also calculates the total influence of a group of moments,
# but takes a DataFrame as input (a DataFrame is output by get_moment_vals() )
function dat_influence_sum(moms_dat, moments)
    in_moments(moment::Symbol) = moment .∈ Ref(moments)
    filtered_dat = filter(:moment => in_moments, moms_dat)
    sum(filtered_dat.obj)
end

### DEFINE GROUPS OF MOMENTS ####

# for now, just viewership and polling moments are defined
viewership_moments = Symbol.(viewership_names.name)
# polling_moments = Symbol.(moms.name[moms.name .∉ Ref([viewership_names.name; "reporting_likelihood"])])
polling_moments = Symbol.(moms.name[contains.(moms.name, "poll")])

nbc_moments = Symbol.(moms.name[contains.(moms.name, r"^NBC_2")])
abc_moments = Symbol.(moms.name[contains.(moms.name, "ABC_2")])
cbs_moments = Symbol.(moms.name[contains.(moms.name, "CBS_2")])
cnn_moments = Symbol.(moms.name[contains.(moms.name, "CNN_2")])
fnc_moments = Symbol.(moms.name[contains.(moms.name, "FNC_2")])
msnbc_moments = Symbol.(moms.name[contains.(moms.name, "MSNBC_2")])
# what moments aren't included in these?
# reporting_likelihood is excluded
channel_moments = Symbol.(moms.name[moms.name .∉ Ref([viewership_names.name; moms.name[contains.(moms.name, "poll")]; "reporting_likelihood"])])

### RUN OBJECTIVE FUNCTION AND COMPARE ACROSS PARAMETERS ###

# for a given set of parameters:
# run the objective
# compare the moment values

function calc_obj(pb)
    ev = SMM.Eval(mprob, pb)     
    obj = news_obj(ev; dt=stbdat, store_moments = true)
    obj_val = sum([(obj.simMoments[k] - SMM.dataMomentd(obj)[k])^2 * SMM.dataMomentWd(obj)[k] for k in Symbol.(moms.name[.!(moms.name.=="reporting_likelihood")])])
    @printf("Objective value: %s", obj_val)
    obj
end

function moments_from_obj(obj)
    dat = DataFrame(
        # run_name = run_name,
        moment = collect(keys(obj.simMoments)),
        sim = collect(values(obj.simMoments)),
        data = collect(values(SMM.dataMomentd(obj))),
        weight = collect(values(SMM.dataMomentWd(obj)))
    )    
    dat.obj .= (dat.sim - dat.data).^2 .* dat.weight
    dat 
end

pb_og = DataStructures.OrderedDict(k => k ∈ to_opt ? x0[findfirst(k .== to_opt)] : pb[k].value for k in keys(pb))
og_obj = calc_obj(pb_og)
og_moments = moments_from_obj(og_obj)

influence(og_obj)
influence_sum(og_obj, viewership_moments)
influence_sum(og_obj, polling_moments)
influence_sum(og_obj, channel_moments)

influence_sum(og_obj, cnn_moments)
influence_sum(og_obj, fnc_moments)
influence_sum(og_obj, msnbc_moments)
influence_sum(og_obj, abc_moments)
influence_sum(og_obj, cbs_moments)
influence_sum(og_obj, nbc_moments)

pb_new = deepcopy(pb_og)
# pb_new["beta:channel:MSNBC"] = pb_og["beta:channel:MSNBC"] .+ 1.0
pb_new[param_to_change] = pb_og[param_to_change] .+ add_to_param

new_obj = calc_obj(pb_new)
new_moments = moments_from_obj(new_obj)

influence(new_obj)
influence_sum(new_obj, viewership_moments)
influence_sum(new_obj, polling_moments)
influence_sum(new_obj, channel_moments)

# compare changes in moments between the two objectives
# negative values mean the objective is HIGHER under the proposed parameter change
influence2(og_obj, new_obj)

### SAVE COMPARISON TO CSV ###
comb_moments = deepcopy(og_moments)
comb_moments.sim_new .= new_moments.sim
comb_moments.obj_new .= new_moments.obj

comb_moments |> CSV.write("$output_dir/moment_comparison.csv")





##### PARAMETER EVOLUTION #####
# par_init = CSV.read("parameter_init.csv", DataFrame);
par_init = CSV.read("$output_dir/best_pars_sep2.csv", DataFrame);
par_change = deepcopy(par_init)
rename!(par_change, [:parameter, :initial_value])
par_change.current_value = best_par.value
par_change.abs_change = par_change.current_value - par_change.initial_value
par_change.pct_change = (par_change.abs_change)./par_change.initial_value
par_change.proposed_step_size = abs.(par_change.abs_change / 50)
par_change.min_step_size = max.(abs.(par_change.current_value .* 0.001), 0.0005)
par_change.step_size = max.(par_change.proposed_step_size, par_change.min_step_size)
par_change |> CSV.write("$output_dir/par_change.csv")
par_change |> CSV.write("/Users/mitchelllinegar/Dropbox/STBNews/stb-model-event/output/par_change.csv")
