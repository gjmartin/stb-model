#' ---
#' title: "STM Summary"
#' author: "Mitchell Linegar"
#' params:
#'   output_name: "oct2016"
#'   input_date_regex: "^2016-10-*"
#'   d2v_iter: 1000
#' output:
#'   pdf_document:
#'    dev: cairo_pdf
#' toc: true
#' toc_depth: 2
#' pdf_document:
#'   number_sections: true
#'   df_print: paged
#'   toc: yes
#'   toc_depth: 2
#'   toc_float: true
#' header-includes: \usepackage{float, placeins, longtable, booktabs, caption, array, xcolor, pdflscape, fullpage, adjustbox, dcolumn, rotating, threeparttable}
#' ---
#+ options, echo=FALSE

#### SET OPTIONS ####
stm_file_name <- "gdelt_stm_10062021.rds" #"gdelt_stm_10112021.rds"
n_topics <- 40
short_text_nchar <- 300

# key_topics <- c(1,3,12,17,22,24,31,33,36)
key_topics <- 1:40
#' ---
#' 
#' # Introduction  
#' 
#' This document summarizes STM embeddings. 
#+ setup, include=FALSE

library(data.table)
library(tidyverse)
library(lubridate)
library(stringr)
library(stm)
library(quanteda)



#### LOAD DATA ####
on_the_yens <- function() dir.exists("/ifs/gsb/")
if (!on_the_yens()){
  setwd("~/Dropbox/STBNews/")
  gdelt_collapse <- readRDS("data/gdelt/gdelt_collapsed.rds")
  model <- readRDS(file.path("data/gdelt/", stm_file_name))
} else {
  gdelt_dir <- "/ifs/projects/gjmartin-oann/data/entities_15s"
  gdelt_collapse <- readRDS(file.path(gdelt_dir, "gdelt_collapsed.rds"))
  model <- readRDS(file.path(gdelt_dir, stm_file_name))
}

# labelTopics(model)

gdelt_collapse[, rel_week := floor(time_length(date - mdy("11-08-2016"), unit = "week"))]

gdelt_dfm <- dfm(gdelt_collapse[, seg_text]) %>%
  dfm_trim(min_docfreq = 0.0001,
           max_docfreq = 0.4,
           docfreq_type = "prop")

nonblank_docs <- which(rowSums(gdelt_dfm) != 0)




# short text for printing
short_text <- substr(gdelt_collapse[nonblank_docs, seg_text], 1, short_text_nchar)
channels <- unique(gdelt_collapse[,channel])

gdelt_stm <- cbind(gdelt_collapse[nonblank_docs], model$theta)
topics <- paste0("topic", 1:n_topics)
setnames(gdelt_stm, paste0("V", 1:n_topics), topics)
gdelt_stm[,seg_text := NULL]
gdelt_stm[,network_type := dplyr::case_when(
  channel %in% c("CNN", "FNC", "MSNBC") ~ "Cable",
  channel %in% c("ABC", "CBS", "NBC") ~ "Broadcast"
)]

channel_avg_doc_prop <- gdelt_stm[, lapply(.SD, mean), .(channel), .SDcols = topics]
channel_avg_doc_prop_long <- melt(channel_avg_doc_prop, id.vars = "channel", measure.vars = topics, variable.name = "topic")
channel_avg_doc_prop_wide <- dcast(channel_avg_doc_prop_long, topic ~ channel, value.var = "value")

meta_effects <- estimateEffect(key_topics ~ channel + factor(rel_week), model, metadata = as.data.frame(gdelt_collapse[nonblank_docs,]))

# save for all topics for polling regressions
meta_effects_full <- estimateEffect(1:n_topics ~ channel + factor(rel_week), model, metadata = as.data.frame(gdelt_collapse[nonblank_docs,]))
saveRDS(meta_effects_full, file.path(gdelt_dir, "meta_effects_full.rds"))

gdelt_stm_daily_mins <- gdelt_stm[,lapply(.SD, function(x) sum(x * seg_len)),.(channel, date), .SDcols = topics]
gdelt_stm_daily_prop_mins <- gdelt_stm[,lapply(.SD, function(x) sum(x * seg_len)/sum(seg_len)),.(channel, date), .SDcols = topics]


gdelt_stm_daily_mins[,network_type := dplyr::case_when(
  channel %in% c("CNN", "FNC", "MSNBC") ~ "Cable",
  channel %in% c("ABC", "CBS", "NBC") ~ "Broadcast"
)]

gdelt_stm_daily_prop_mins[,network_type := dplyr::case_when(
  channel %in% c("CNN", "FNC", "MSNBC") ~ "Cable",
  channel %in% c("ABC", "CBS", "NBC") ~ "Broadcast"
)]
#### FUNCTIONS ####

# print sample document for topic
topic_sample_doc <- function(topic_num){
  topic_thoughts <- findThoughts(model, topics = topic_num, n=1, texts = short_text)$docs[[1]]
  plotQuote(topic_thoughts, main = sprintf("Representative Segment for Topic %s", topic_num))
}



# channel coverage for a given topic
topic_channel_prop_plot <- function(topic_num){
  plot(meta_effects, topics = topic_num, covariate = "channel", model = model, method = "pointestimate", labeltype = "custom", custom.labels = channels,
       main = sprintf("Doc-Topic Proportion Estimate for Topic %s", paste(topic, collapse = " & ")),
       xlab = "Doc-Topic Proportion Estimate (in 'median' segment)")
}

# COME BACK TO FOR NEW VERSION
# relative week coverage (BAD, not clear)

topic_channel_comparison_plot <- function(topic_num, channel1, channel2){
  # differences in topic coverage between two channels
  plot(meta_effects, topics = topic_num, covariate = "channel", model = model, method = "difference",
       cov.value1 = channel1, cov.value2 = channel2,
       labeltype = "custom", custom.labels = paste0("Topic ", topic_num),
       xlab = sprintf("%s vs %s", channel1, channel2)
  )
}


# plot coefficients over time
make_topic_coefs <- function(topic_num){
  topic_coefs <- summary(meta_effects, topic = topic_num)$tables[[1]] %>% as.data.table(keep.rownames = TRUE)
  setnames(topic_coefs, c("rn", "Std. Error"), c("variable", "std_error"))
  topic_coefs[, topic := topic_num]
  topic_coefs[, rel_week := ifelse(grepl("rel_week", variable), as.numeric(stringr::str_extract(variable, "-?[0-9]*$")), NA)]
  topic_coefs
}

# similar version using built in functions
# plot(meta_effects, topics = 31, covariate = "rel_week", model = model, method = "pointestimate",
#      xlab = "Topic Proportion", printlegend = FALSE)

topic_time_coef_plot <- function(topic_num){
  topic_coefs <- make_topic_coefs(topic_num)
  ggplot(topic_coefs[!is.na(rel_week)], aes(x = rel_week, y = Estimate)) + 
    geom_point() + 
    geom_errorbar(width = 0.1, aes(ymin = Estimate-1.96*std_error, ymax = Estimate+1.96*std_error)) + 
    geom_smooth() +
    theme_bw() + 
    labs(
      title = sprintf("Covariate Estimate over Time for Topic %s", topic_num),
      x = "Week Relative to Election",
      y = "Coefficient Estimate"
    )
}

topic_minutes_plot <- function(topic_num, dt, ylab){
  # dt should be one of: gdelt_stm_daily_mins, gdelt_stm_daily_prop_mins
  ggplot(dt, aes_string(x = "date", y = paste0("topic", topic_num), color = "channel")) +
    geom_line() + 
    theme_bw() + 
  scale_colour_manual(values=c(CNN="mediumpurple2", FNC="darkred", MSNBC="cornflowerblue", 
                               ABC="gray20", NBC="gray50", CBS="gray80")) + 
    labs(
      title = sprintf("%s for Topic %s", ylab, topic_num),
      subtitle = "Daily sum of segment length * segment topic proportion",
      y = ylab,
      x = "Date"
    ) +
    facet_grid(~network_type)
}

#### REPORT #####
#' # Corpus-Level Analysis  
#' 
#' Here we discuss corpus-level topic distributions. 
##' We focus on two types of statistics: various breakdowns of $\theta$ (the document-topic matrix), and 
##' , such as average topic coverage by channel, average topic coverage overall, average topic coverage over time. 
#+ print_channel_avg_doc_prop, results='asis', echo=FALSE
knitr::kable(channel_avg_doc_prop_wide, format = "latex", digits = 2)

#+ plot_topic_cor, echo=FALSE
# plot topic correlations
plot(topicCorr(model))

#+ plot_topic_prop, echo=FALSE, fig.height=6, fig.width=5
# overall topic proportion
plot(model, type = "summary")

#' 
#' \clearpage
#' 
#+ plot_topic_doc_dist, echo=FALSE
# topic proportions for selected topics
plot(model, topics = key_topics, type = "hist")


#' # Topic-by-Topic Analysis  
#' 
#' Here we perform some basic analysis for each topic. We focus on a few key topics: 
#+ echo=FALSE
labelTopics(model, topics = key_topics)
#+ echo=FALSE
# key_topics <- c(3, 12, 31, 36)
for (topic in key_topics){
  labelTopics(model, topic = topic) %>% print()
  topic_sample_doc(topic) %>% print() # prints extra NULL
  
  knitr::kable(make_topic_coefs(topic), 
               # format = "latex", 
               caption = sprintf("Coefficients for Topic %s", topic)) %>% print()
  
  
  topic_channel_prop_plot(topic) %>% print()
  
  topic_time_coef_plot(topic) %>% print()
  
  topic_minutes_plot(topic, gdelt_stm_daily_mins, "Daily Topic-Minutes") %>% print()
  topic_minutes_plot(topic, gdelt_stm_daily_prop_mins, "Daily Proportion of Topic-Minutes") %>% print()
}

#' # Topic Coefficients Compared Across Channels  
#' 
#' For all key topics, we show the coefficient difference between pairs of channels. We focus on cable channels. 
#+ print_key_topic_labels, echo=FALSE
labelTopics(model, topics = key_topics)

#+ plot_topic_channel_diffs, echo=FALSE
topic_channel_comparison_plot(key_topics, "MSNBC", "FNC")
topic_channel_comparison_plot(key_topics, "CNN", "FNC")
topic_channel_comparison_plot(key_topics, "MSNBC", "CNN")

#+ scratch, echo=FALSE
# 
# plot(meta_effects, covariate = "rel_week", topics = 31, model = model)
# 
# R> plot(prep, "day", method = "continuous", topics = 13,
#         + model = z, printlegend = FALSE, xaxt = "n", xlab = "Time (2008)") 
# R> monthseq <- seq(from = as.Date("2008-01-01"),
#                                                                                                + to = as.Date("2008-12-01"), by = "month")
# 
# R> plot(prep, covariate = "day", model = poliblogInteraction,
#         +    method = "continuous", xlab = "Days", moderator = "rating",
#         + moderator.value = "Liberal", linecol = "blue", ylim = c(0, 0.12),
#         +    printlegend = FALSE)
# 
# # to add:
# 
# plot.STM()
# plot.estimateEffect(meta_effects, "channel")
# plot.topicCorr()
# 
# 
# 
# thoughts3 <- findThoughts(poliblogPrevFit, texts = shortdoc, n = 2, + topics = 6)$docs[[1]]
# R> thoughts20 <- findThoughts(poliblogPrevFit, texts = shortdoc, n = 2, + topics = 18)$docs[[1]]
# R> par(mfrow = c(1, 2), mar = c(0.5, 0.5, 1, 0.5))
# R> plotQuote(thoughts3, width = 30, main = "Topic 6") 
# R> plotQuote(thoughts20, width = 30, main = "Topic 18")
# 
# 
# plot(prep, covariate = "rating", topics = c(6, 13, 18),
#      + model = poliblogPrevFit, method = "difference", cov.value1 = "Liberal",
#      + cov.value2 = "Conservative",
#      + xlab = "More Conservative ... More Liberal",
#      + main = "Effect of Liberal vs. Conservative", xlim = c(-0.1, 0.1),
#      + labeltype = "custom", custom.labels = c("Obama/McCain", "Sarah Palin",
#                                                +      "Bush Presidency"))
# 
# plot(prep, "day", method = "continuous", topics = 13,
#      + model = z, printlegend = FALSE, xaxt = "n", xlab = "Time (2008)") R> monthseq <- seq(from = as.Date("2008-01-01"),
#                                                                                             + to = as.Date("2008-12-01"), by = "month")
# 
# monthnames <- months(monthseq)
# R> axis(1,at = as.numeric(monthseq) - min(as.numeric(monthseq)), + labels = monthnames)
# 
# poliblogInteraction <- stm(out$documents, out$vocab, K = 20,
#                            + prevalence =~ rating * day, max.em.its = 75, data = out$meta,
#                            + init.type = "Spectral")
# 
# R> prep <- estimateEffect(c(16) ~ rating * day, poliblogInteraction,
#                           +    metadata = out$meta, uncertainty = "None")
# R> plot(prep, covariate = "day", model = poliblogInteraction,
#         +    method = "continuous", xlab = "Days", moderator = "rating",
#         + moderator.value = "Liberal", linecol = "blue", ylim = c(0, 0.12),
#         +    printlegend = FALSE)
# R> plot(prep, covariate = "day", model = poliblogInteraction,
#         +    method = "continuous", xlab = "Days", moderator = "rating",
#         + moderator.value = "Conservative", linecol = "red", add = TRUE,
#         +    printlegend = FALSE)
# 
# legend(0, 0.06, c("Liberal", "Conservative"), lwd = 2, + col = c("blue", "red"))
# 
# 
# 
# 
# mod.out.corr <- topicCorr(poliblogPrevFit) 
# 
# R> plot(mod.out.corr)

# 
# packages: 
  
  # stmCorrViz

# tidystm (ggplot2)
# stminsights
# reports across runs: stmprinter

#' # Appendix  
#' 
#' List of all topics and their top words:  
labelTopics(model)


