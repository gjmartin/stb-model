function update_rhos_multinomial!(ρ, α, α_r, α_η, topic_r_log, topic_η)
    # get reporting probabilities for each topic and channel 
    # for a single block (multinomial version)
    # given event indicators for today (topic_η) and log r_j's for each topic (topic_r_log)
    # updates the K by C matrix ρ
    (K,C)  = size(ρ)
    
    for c = 1:C
        sum_exp_util = 1
        for k = 1:K 
            exp_util = exp(α + α_r[c] * topic_r_log[k] + α_η[c] * topic_η[k])
            sum_exp_util += exp_util
            ρ[k,c] = exp_util
        end
        ρ[:,c] ./= sum_exp_util
    end
end

function update_rhos!(ρ_1, ρ_0, α_0, α_r, α_η, topic_r_log)
    # get reporting probabilities for each topic and channel 
    # in days with / without news on the event 
    # given log r_j's for each topic (topic_r_log)
    # updates the K by C matrices ρ_1, ρ_0
    ρ_0 .= -(α_0' .+ α_r' .* topic_r_log)
    ρ_1 .= 1 ./ (1 .+ exp.(ρ_0 .- α_η'))
    ρ_0 .= 1 ./ (1 .+ exp.(ρ_0))
end



function update_topic_weights!(i, w, μ_η, ρ_diff, ρ_0)
    # compute perceived topic weights for single viewer, single block
    # updates the K by C matrix w
    for c = 1:size(w, 2)
        @views w[:,c, i] .= μ_η[:,i] .* ρ_diff[:,c] .+ ρ_0[:,c]
    end
end