### Export data for Julia model ###
library(tidyverse)
library(data.table)
library(lubridate)
library(magrittr)
library(ggthemes)
library(quanteda)
library(here)
library(haven)

setwd("~/stb-model/")
here::i_am("code/02_pre_setup/01_export_model_data.R")

theme_set(theme_hc())

data_dir <- function(...) here("data", ...)
plot_dir <- function(...) here("output", "standard_output_graphs", ...)
local_dir <- "~/Dropbox/STBNews/"

### set date, timeblock, topic ranges to include ###

## dates:
# begin on June 8 (day after last primaries on June 7)
# end Jan 31
# exclude Saturdays
date_range <- seq(ymd("2016-06-08"), ymd("2017-01-31"), by = "day") %>%
    discard(.p = ~ lubridate::wday(., label = T, abbr = T) == "Sat")

## channels to include
allchans <- c("ABC", "CBS", "CNN", "FNC", "MSNBC", "NBC")

## time blocks: 5PM - 11PM local time weekdays;
# 8AM - 12PM plus 6PM-8PM Sundays
# in 15 min increments

time_ranges <- list(weekday   = c("17:00", "22:59"),
                    sunday_am = c("08:00", "11:59"),
                    sunday_pm = c("18:00", "19:59"))

## time zones
time_zones <- c(ETZ = "America/New_York",
                CTZ = "America/Chicago",
                MTZ = "America/Denver",
                PTZ = "America/Los_Angeles")

## topics
topics <- fread(data_dir("topics.csv"))
topic_nums <- sort(unique(topics$topic))


# generate complete schedule of 15 min blocks
gen_blocks <- function(date, block_len = "15 min") {
    if (lubridate::wday(date, label = T) != "Sun") {
        dttms <- date %>% paste(time_ranges$weekday) %>% ymd_hm
        blks <- seq(dttms[1], dttms[2], by = block_len)
    } else {
        dttms <- date %>% paste(time_ranges$sunday_am) %>% ymd_hm
        blks1 <- seq(dttms[1], dttms[2], by = block_len)

        dttms <- date %>% paste(time_ranges$sunday_pm) %>% ymd_hm
        blks2 <- seq(dttms[1], dttms[2], by = block_len)

        blks <- c(blks1, blks2)
    }

    blks
}

# cross dates by times within date
completed_sch <- data.table(date = date_range) %>%
    .[, .(block = gen_blocks(date)), by = .(date)]

# repeat for every channel
completed_sch <- allchans %>%
    map(~ copy(completed_sch)[, channel := .]) %>%
    rbindlist %>%
    .[, channel := factor(channel, levels = allchans)] %>%
    .[order(date, channel, block)]

# repeat for every timezone
completed_sch <- map(time_zones, ~ copy(completed_sch)) %>%
    rbindlist(idcol = "timezone")

## shows and topic weights from topic model
topic_ws <- fread(data_dir("block_weights.csv"))

# occasional repeats of the same channel / block because of overflow. use whichever is longer (in words)
gdelt <- readRDS(paste0(local_dir, "data/gdelt/gdelt_15min_blocks_noads.rds"))
gdelt[, ent_count := ntoken(entities, what = "fastestword")]
gdelt_select <- gdelt[order(channel, block, ent_count)] %>%
    .[, .SD[.N], by = .(channel, block)] %>%
    .[, .(archive_id, channel, show, block)]

topic_ws <- topic_ws[gdelt_select, on = .(archive_id, channel, show, block), nomatch = 0]

tz_adjust <- function(seg, tz) {
    # adjust time zone. for cable we have 24h national feed => shift
    # for broadcast we have only 1/2h local feed => assume occurs at same *local* time everywhere
    seg[, block_cable := with_tz(block, tzone = tz)]
    seg[, block_broadcast := with_tz(block, tzone = "America/Los_Angeles")]

    seg[, block := NULL]

    seg[channel %in% c("CNN", "FNC", "MSNBC"), block := force_tz(block_cable, "UTC")]
    seg[channel %in% c("ABC", "CBS", "NBC"), block := force_tz(block_broadcast, "UTC")]

    seg[channel %in% c("CNN", "FNC", "MSNBC"), date := date(block_cable)]
    seg[channel %in% c("ABC", "CBS", "NBC"), date := date(block_broadcast)]

    seg[, block_cable := NULL]
    seg[, block_broadcast := NULL]

    seg
}

coverage <- map(time_zones, ~ tz_adjust(copy(topic_ws), .)) %>%
    rbindlist(idcol = "timezone") %>%
    .[completed_sch, on = .(timezone, channel, date, block)]

# compute weighted average block partisanship
coverage <- melt(coverage,
    id.vars = c("archive_id", "timezone", "channel", "show", "date", "block"),
    measure.vars = paste0("topic", topic_nums),
    value.name = "wgt",
    variable.name = "topic"
    )

coverage[, topic := as.integer(sub("topic", "", topic))]
# event_coverage <- coverage[topics[,.(topic, event, begin, end, partisanship_std)], on = .(topic), allow.cartesian = T] # this repeats some topics with multiple events

# ## construct event-weight which is nonzero only in event periods
# event_coverage[date >= begin & date <= end, event_wgt := wgt]
# event_coverage[is.na(event_wgt) & !is.na(archive_id), event_wgt := 0]

# ## compute weighted average partisanship at the block level
# event_coverage[, repeated := .N, by = .(archive_id, channel, show, timezone, date, block, topic)]   # remember that a topic with 3 events is in this dataframe 3 times, don't want to triple count
# block_partisanship <- event_coverage[, .(partisanship = sum(partisanship_std * wgt / repeated),
#                                    event_partisanship = sum(partisanship_std * event_wgt)),
#                                 by = .(archive_id, channel, show, timezone, date, block)]


# block_partisanship %>% fwrite(data_dir("block_partisanship.csv"))

## export coverage for julia model
coverage <- dcast(coverage, timezone + archive_id + channel + show + date + block ~ topic, value.var = "wgt")
coverage[, timezone := fct_relevel(timezone, "ETZ")]
coverage <- coverage[order(timezone, channel, date, block, show)]

coverage[is.na(show), show := paste(channel, "default", sep = "_")]

# timezone-daypart indicators
coverage[lubridate::wday(date, label = T) == "Sun",
         tz_daypart := paste(timezone, if_else(hour(block) <= 12, "Sun_AM", "Sun_PM"), sep = "_")]
coverage[lubridate::wday(date, label = T) != "Sun",
    tz_daypart := paste(timezone, case_when(hour(block) <= 17 ~  "M-F_5-6",
                                    hour(block) <= 18 ~  "M-F_6-7",
                                    hour(block) <= 19 ~  "M-F_7-8",
                                    TRUE ~ "M-F_8-11"),
                    sep = "_")]

## write to CSVs, one per time zone
setwd(data_dir())
time_zones %>% iwalk(~ fwrite(coverage[timezone == .y], paste0("topic_coverage_", .y, ".csv")))


### generate simulated HHs ###

# fips codes
setwd(paste0(local_dir, "data/geo/"))
stfips <- fread("state_fips_codes.csv") %>% 
    setnames(old="statefips", new="stfips")
ctyfips <- fread("county_fips_codes.csv") %>% 
    setnames(old=c("statefips", "countyfips", "stateabbr","countyname"), new=c("stfips", "ctyfips", "state","county"))

# county to dma crosswalk
Mode <- function(x) table(x) %>% sort %>% tail(1) %>% names
cty2dma <- fread("DMA-zip.csv") %>%
    .[, .(stfips = as.integer(substr(str_pad(FIPS, width = 5, pad = "0"), 1, 2)),
         ctyfips = as.integer(substr(str_pad(FIPS, width = 5, pad = "0"), 3, 5)),
         dma = `DMA CODE`)] %>%
    .[, .(dma = Mode(dma)), by = .(stfips, ctyfips)] %>%
    .[, dma := as.integer(dma)]

# dma code translation
dmacodes <- fread("nielsen_rating_dma_codes.csv")

# read county vote data
setwd(paste0(local_dir, "data/political/"))
clean_vote <- function(dt) {
    dt[, .(statename = toupper(State),
           year = as.numeric(substr(raceDate, 1,4)),
           office = recode(Office, Governor = "gov", Senate = "sen", President = "pres"),
           county = Area,
           rep_vote = as.numeric(gsub(",", "", RepVotes)),
           dem_vote = as.numeric(gsub(",", "", DemVotes)),
           third_vote = as.numeric(gsub(",", "", ThirdVotes)),
           other_vote = as.numeric(gsub(",", "", OtherVotes)))] %>%
    .[office %in% c("sen", "gov", "pres")] %>%
    .[, map(.SD, ~ replace_na(.,0)),
        by = .(statename, year, office, county),
        .SDcols = c("rep_vote", "dem_vote", "third_vote", "other_vote")] %>%
    .[stfips, on = "statename", nomatch = 0]
}

votes <- fread("cq_voteshares_pres.csv", fill = T, sep = ",") %>%
    clean_vote

# fix a few county names
votes[, county := str_trim(county)]
votes[, county := sub(" COUNTY", "", county)]

votes$county[votes$county == "PRINCE GEORGES"] <- "PRINCE GEORGE'S"
votes$county[votes$county == "LA MOURE"] <- "LAMOURE"
votes$county[votes$county == "LA PORTE"] <- "LAPORTE"
votes$county[votes$county == "DE BACA"] <- "DEBACA"
votes$county[votes$county == "DU PAGE"] <- "DUPAGE"
votes$county[votes$county == "DE WITT" & votes$state == "TX"] <- "DEWITT"
votes$county[votes$county == "DE SOTO" & votes$state %in% c("FL", "MS")] <- "DESOTO"
votes$county[votes$county == "QUEEN ANNES"] <- "QUEEN ANNE'S"
votes$county[votes$county == "ST. MARYS"] <- "ST. MARY'S"
votes$county[votes$county == "ST. LOUIS CITY"] <- "ST LOUIS CITY"
votes$county[votes$county == "KINGSBURG" & votes$state == "SD"] <- "KINGSBURY"
votes$county[grepl("WASHABAUGH", votes$county)] <- "JACKSON"
votes$county[votes$county == "WASHINGTON" & votes$state == "SD"] <- "JACKSON"

# deduplicate county names for independent cities
ctyfips$county[ctyfips$ctyfips == 510 & ctyfips$stfips == 29] <- "ST LOUIS CITY"
ctyfips$county[ctyfips$ctyfips == 510 & ctyfips$stfips == 24] <- "BALTIMORE CITY"
ctyfips$county[ctyfips$ctyfips == 515 & ctyfips$stfips == 51] <- "BEDFORD CITY"
ctyfips$county[ctyfips$ctyfips == 600 & ctyfips$stfips == 51] <- "FAIRFAX CITY"
ctyfips$county[ctyfips$ctyfips == 620 & ctyfips$stfips == 51] <- "FRANKLIN CITY"
ctyfips$county[ctyfips$ctyfips == 760 & ctyfips$stfips == 51] <- "RICHMOND CITY"
ctyfips$county[ctyfips$ctyfips == 770 & ctyfips$stfips == 51] <- "ROANOKE CITY"

votes <- votes[ctyfips, on =c("state", "stfips", "county"), nomatch = 0]   # note this excludes AK which does not report votes at county level
votes[, total_vote := (rep_vote + dem_vote + third_vote + other_vote)]
votes[, r_prob := rep_vote / (rep_vote + dem_vote)]


# state level 2016 voting for matching moments
votes2016state <- votes[year == 2016,
                        .(r_prob = sum(rep_vote) / sum(rep_vote + dem_vote)),
                        by = .(state)]

# county level voting in 2012 for sampling baseline
votes2012 <- votes[year == 2012]

# merge with DMA info
votes2012 <- votes2012[cty2dma, on = .(stfips, ctyfips), nomatch = 0]


# read viewership files to determine DMA's with consistent viewing data
setwd(paste0(local_dir, "data/nielsen"))

# read raw data from nielsen to get set of DMAs to use in calculating ratings terciles
nielsen_files <- list.files(pattern = "*.csv")

read_nielsen <- function(f) {
    cat(f, "\n")
    n <- fread(f, skip = 2, fill = T) %>% # first 2 lines are metadata
        .[!is.na(Intab)] %>%           # drop garbage from end
        .[, `:=` (rating = as.numeric(`RTG % (X.XXX)`) / 100,
                 share  = as.numeric(`SHR %`) / 100,
                 air_hour = as.numeric(str_extract(Time, "^\\d+(?=:)")) + if_else(str_extract(Time, "^\\d+(?=:)") == 12, 0, if_else(str_extract(Time, "[ap]m")== "am", 0, 12)),
                 air_min = as.numeric(str_extract(Time, "(?<=:)\\d+")))] %>%
        .[, .(channel = if_else(`Affil.` == "CABLE", str_trim(`Viewing Source`), `Affil.`),
             date = mdy(Dates),
             dmaname = Geography,
             rating,
             share,
             block = ymd_hm(paste0(mdy(Dates), " ", air_hour, ":", air_min)),
             intab = as.numeric(gsub(",", "", Intab)))] %>%
        .[!is.na(date)] %>%
        .[channel == "FXNC", channel := "FNC"]
}

nielsen <- nielsen_files %>%
    map(read_nielsen) %>%
    rbindlist %>%
    .[dmacodes, on = .(dmaname)] %>%
    .[!is.na(intab)]

nielsen %>% saveRDS("all_nielsen_ratings.rds")

# limit to same set of blocks
nielsen[, timezone := case_when(dma >= 800 ~ "PTZ",
                           dma >= 700 ~ "MTZ",
                           dma >= 600 ~ "CTZ",
                           dma >= 500 ~ "ETZ") %>% fct_relevel("ETZ")]
nielsen <- nielsen[completed_sch, on = .(date, block, timezone, channel)]

## set of unique DMAs
rating_dmas <- nielsen[, .(dma)] %>% unique

## DMAs by tercile of r vote share in 2012
terciles <- rating_dmas[votes2012, on = .(dma), nomatch = 0] %>%
    .[, .(total_vote = sum(total_vote), r_prob = weighted.mean(r_prob, total_vote)), by = .(dma)]


# compute quantiles, weighted by DMA pop
weighted.quantile <- function(x, w, tau=0.5) {
    i <- order(x)
    x <- x[i]
    w <- w[i]
    x[min(which(cumsum(w) >= tau * sum(w)))]
}

terc_1 <- weighted.quantile(terciles$r_prob, terciles$total_vote, tau = 0.333)
terc_2 <- weighted.quantile(terciles$r_prob, terciles$total_vote, tau = 0.666)

terciles[, tercile := case_when(r_prob <= terc_1 ~ 1,
                                r_prob <= terc_2 ~ 2,
                                r_prob <= 1 ~ 3)]

fwrite(terciles, data_dir("dma_by_tercile.csv"))

# now sample from state-dmas according to # votes cast
all_dmas <- votes2012[, .(total_vote = sum(total_vote), dma_r_prob = weighted.mean(r_prob, total_vote)), by = .(state, dma)]

# number of sim households to sample
hh_samp_size <- 12000

# draw state-DMA assignments with replacement
set.seed(2385020)
hhs <- all_dmas[sample.int(n = .N, size = hh_samp_size, replace = T, prob = total_vote)]

allstates <- sort(unique(hhs$state))

# read daily polling

date_select <- data.table(t = rep(date_range, times = length(allstates)), state = rep(allstates, each = length(date_range))) 

polls <- fread(paste0(local_dir, "data/political/polls_and_votes_2016.csv")) %>%
    .[, t := ymd(t)] %>%
    .[date_select, on = .(t, state)] %>%
    .[, adjusted_mean := mean - (mean[t == ymd("2016-11-08") & type == "economist_poll_avg"] - mean[t == ymd("2016-11-08") & type == "actual"]), by = .(state)] %>%   # shift polls time series to match election day result
    .[is.na(mean), `:=`(type = "economist_poll_avg", mean = 0, adjusted_mean = 0, high = Inf)] %>%
    .[type == "economist_poll_avg", .(date = t, state, poll = adjusted_mean, se = (high - mean) / 1.96)] %>%
    .[order(date, state)]

# now sample the r_probs to match distribution of certainty from Pew
# assume three types: Trump voters, Clinton voters, Undecideds
pew <- read_spss(paste0(local_dir, "data/political/pew_2016_preelection/June16 public.sav")) %>%
    setDT

# fix states
pewstates <- data.table(state = names(attr(pew$state, "labels")),
                        pewstate = attr(pew$state, "labels"))
setnames(pew, old = "state", new = "pewstate")
pew <- pew[pewstates, .(state, q10, q10a, q11, q12), on = .(pewstate)]

pew[, trump_voter := replace_na(if_else(q10 == 2 | q10a == 2, 1, 0), 0)]
pew[, clinton_voter := replace_na(if_else(q10 == 1 | q10a == 1, 1, 0), 0)]
pew[clinton_voter == 1, already_decided := as.numeric(q11 == 2)]
pew[trump_voter == 1, already_decided := as.numeric(q12 == 2)]

# compute fraction of trump / clinton supporters who say they have made up their minds by state
# for states with at least 10 respondents, compute by state
# for states with less than 10 respondents, pool
pew[, n_trump := sum(trump_voter), by = .(state)]
pew[n_trump >= 10, decided_trump := mean(already_decided[trump_voter == 1], na.rm = T), by = .(state)]
pew[n_trump < 10, decided_trump := mean(already_decided[trump_voter == 1], na.rm = T)]

pew[, n_clinton := sum(clinton_voter), by = .(state)]
pew[n_clinton >= 10, decided_clinton := mean(already_decided[clinton_voter == 1], na.rm = T), by = .(state)]
pew[n_clinton < 10, decided_clinton := mean(already_decided[clinton_voter == 1], na.rm = T)]

# compute fraction of undecideds by state
# again pooling states with less than 10 respondents
pew[, n_resp := .N, by = .(state)]
pew[n_resp >= 10, undecided := sum(clinton_voter == 0 & trump_voter == 0) / .N, by = .(state)]
pew[n_resp < 10, undecided := sum(clinton_voter == 0 & trump_voter == 0) / .N]


state_certainty <- pew[, .(state, decided_clinton, decided_trump, undecided)] %>% unique(by = "state")

# bound away from 0 / 1 to avoid numerical issues
state_certainty[, `:=` (decided_clinton = pmin(decided_clinton, 0.975),
                       decided_trump = pmin(decided_trump, 0.975),
                       undecided = pmax(undecided, 0.025))]

hhs <- hhs[state_certainty, on = .(state), nomatch = 0]

gen_clinton_mus <- function(n, conf, thresh = 0.1) rexp(n, rate = log(1 / (1 - conf)) / thresh)
gen_trump_mus <- function(n, conf, thresh = 0.1) 1 - rexp(n, rate = log(1 / (1 - conf)) / thresh)
gen_undecided_mus <- function(n, range = 0.3) rnorm(n, mean = 0.5, sd = range / 6)

# within state-dma cells, sim from each type in proportions to match 2012 dma vote
hhs[, r_prob := case_when(seq_len(.N) / .N < undecided ~ gen_undecided_mus(.N),
                          seq_len(.N) / .N < undecided / 2 + dma_r_prob ~ gen_trump_mus(.N, decided_trump),
                          TRUE ~ gen_clinton_mus(.N, decided_clinton))
    , by = .(state, dma)]
hhs[, n_sims := .N, by = .(state, dma)]
hhs[n_sims == 1 & dma_r_prob > 0.5, r_prob := gen_trump_mus(.N, decided_trump)] # ensure that state-DMA cells with 1 sim aren't all Clinton voters

# check match by state-dma
votetest <- hhs[, .(pred = mean(r_prob > 0.5), actual = mean(dma_r_prob), n = .N), by = .(state, dma)]
summary(lm(pred ~ actual, data = votetest, weights = votetest$n))

# then adjust r_probs to match day 1 polling by state
poll0 <- polls[date == ymd(date_range[1]), .(state, state_day1_poll = poll)]
hhs <- hhs[poll0, on = .(state)]

# do this by randomly reflecting simulated r_probs around 0.5 
state_adjust <- hhs[, .(day0_error = mean(r_prob < 0.5) - mean(state_day1_poll), n = .N), by = .(state)]
state_adjust[, flips_needed := round(n * day0_error)]
hhs <- hhs[state_adjust, on = .(state)]

# case 1: too many D voters
hhs[flips_needed > 0 & r_prob < 0.5,
    flipthis := seq_len(.N) %in% sample.int(.N, size = max(flips_needed)),
    by = .(state)]

# case 2: too many R voters
hhs[flips_needed < 0 & r_prob > 0.5,
    flipthis := seq_len(.N) %in% sample.int(.N, size = -1*min(flips_needed)),
    by = .(state)]

hhs[flips_needed == 0, flipthis := F]
hhs[is.na(flipthis), flipthis := F]

# reflect
hhs[flipthis == T, r_prob := 1 - r_prob]

# check match at state level
hhs[, .(day0_error = mean(r_prob < 0.5) - mean(state_day1_poll), n = .N), by = .(state)][order(day0_error)]

# retest at state-dma level
votetest <- hhs[, .(pred = mean(r_prob > 0.5), actual = mean(dma_r_prob), n = .N), by = .(state, dma)]
summary(lm(pred ~ actual, data = votetest, weights = votetest$n))


# add tercile
hhs <- terciles[, .(dma, tercile)][hhs, on = .(dma)]
hhs[is.na(tercile), tercile := 0]

# add time zone info
hhs <- hhs[, timezone := case_when(dma >= 800 ~ "PTZ",
                           dma >= 700 ~ "MTZ",
                           dma >= 600 ~ "CTZ",
                           dma >= 500 ~ "ETZ") %>% fct_relevel("ETZ")]


# write to csv
hhs[, .(timezone, state, dma, r_prob, tercile)] %>%
    .[order(timezone, state, dma, r_prob)] %>%
    fwrite(file = data_dir("sim_hh_sample.csv"))

fwrite(polls[, .(date, state, poll, se)], data_dir("polling.csv"))

### ratings moments
### block by day by DMA tercile

nielsen <- nielsen[terciles[, .(dma, tercile)], on = .(dma), nomatch = 0]

# zero out ratings for non-news blocks on networks
zero_out <- coverage[channel %in% c("ABC", "CBS", "NBC") & is.na(show), .(timezone, channel, block, show)] %>% unique
zero_out[, show := NULL]
zero_out[, nonnews := 1]

nielsen <- zero_out[nielsen, on = .(timezone, channel, block)]
nielsen[is.na(nonnews), nonnews := 0]
nielsen[nonnews == 1, rating := 0]

# aggregate by DMA tercile
rat_block_terc <- nielsen[,
                         .(mean_nonnews = weighted.mean(nonnews, intab, na.rm = T),
                           mean_rating = weighted.mean(rating, intab, na.rm = T),
                           intab = sum(intab, na.rm = T)),
                          by = .(channel, tercile, date, block)]

# output ratings by tercile
rat_block_terc %>%
    dcast(date + block + tercile ~ channel, value.var = "mean_rating") %>%
    .[date %in% date_range] %>%
    fwrite(data_dir("nielsen_ratings.csv"))

# output se's for weighting
rat_block_terc %>%
    .[, se := sqrt(mean(mean_rating) * (1 - mean(mean_rating)) / intab), by = .(channel, tercile, intab)] %>%
    .[mean_nonnews == 1, se := Inf] %>%
    dcast(date + block + tercile ~ channel, value.var = "se") %>%
    .[date %in% date_range] %>%
    fwrite(data_dir("nielsen_rating_ses.csv"))


avg_min_day <- rat_block_terc[, .(avg_min = sum(mean_rating * 15)),
                                by = .(date, tercile, channel)]
avg_min_day[, dma_group := fct_collapse(factor(tercile), Dem = "1", Swing = "2", Rep = "3")]


avg_mins_plot <- avg_min_day[year(date) == 2016 & lubridate::wday(date) %in% 2:6 & channel %in% c("CNN", "FNC", "MSNBC")] %>%
    ggplot(aes(x = date, y = avg_min, group = channel)) +
    geom_line(aes(colour = channel)) +
    theme_bw() +
    ylab("Daily Average Minutes (Nielsen)") +
    xlab("Date") +
    scale_colour_manual(values = c(CNN = "mediumpurple2", FNC = "darkred", MSNBC = "cornflowerblue")) +
    scale_x_date(breaks = ymd(c("20160604", "20160801", "20161001", "20161108", "20130101")),
               labels = c("Jun 4", "Aug 1", "Oct 1", "Nov 8", "Jan 1")) +
  geom_vline(xintercept = ymd("20161108"), linetype = "dashed") +
  ylim(0, 25) +
  theme(legend.position = "bottom") +
  facet_wrap(~ dma_group)

ggsave(plot = avg_mins_plot,
       filename = plot_dir("nielsen_daily_avg_mins.png"),
       height = 4,
       width = 10)

max_ratings_day <- rat_block_terc %>%
    .[, .(max_rat = max(mean_rating)), by = .(date, tercile, channel)]
max_ratings_day[, dma_group := fct_collapse(factor(tercile), Dem = "1", Swing = "2", Rep = "3")]

max_ratings_plot <- max_ratings_day[year(date) == 2016 & lubridate::wday(date) %in% 2:6 & channel %in% c("CNN", "FNC", "MSNBC")] %>%
    ggplot(aes(x = date, y = max_rat, group = channel)) +
    geom_line(aes(colour = channel)) +
    theme_bw() +
    ylab("Daily Max Block Rating") +
    xlab("Date") +
    scale_colour_manual(values = c(CNN = "mediumpurple2", FNC = "darkred", MSNBC = "cornflowerblue")) +
    scale_x_date(breaks = ymd(c("20160604", "20160801", "20161001", "20161108", "20130101")),
                 labels = c("Jun 4", "Aug 1", "Oct 1", "Nov 8", "Jan 1")) +
    geom_vline(xintercept = ymd("20161108"), linetype = "dashed") +
#   ylim(0,max_day_rating) +
    theme(legend.position = "bottom") +
    facet_wrap(~ dma_group)

ggsave(plot = max_ratings_plot,
       filename = plot_dir("nielsen_daily_rating_max.png"),
       height = 4, width = 10)

### average news rating by timezone x daypart
nielsen[lubridate::wday(date, label = T) == "Sun",
         tz_daypart := paste(timezone, if_else(hour(block) <= 12, "Sun_AM", "Sun_PM"), sep = "_")]
nielsen[lubridate::wday(date, label = T) != "Sun",
    tz_daypart := paste(timezone, case_when(hour(block) <= 17 ~  "M-F_5-6",
                                    hour(block) <= 18 ~  "M-F_6-7",
                                    hour(block) <= 19 ~  "M-F_7-8",
                                    TRUE ~ "M-F_8-11"),
                    sep = "_")]
nielsen[, tz_daypart := fct_relevel(tz_daypart, "ETZ_M-F_5-6", "ETZ_M-F_6-7", "ETZ_M-F_7-8", "ETZ_M-F_8-11", "ETZ_Sun_AM", "ETZ_Sun_PM")]

nielsen[, .(mean_rating = weighted.mean(rating, intab),
            intab = sum(intab)), by = .(tz_daypart)] %>%
    .[order(tz_daypart)] %>%
    .[, se_rating := sqrt(mean_rating * (1 - mean_rating) / intab)] %>%
    fwrite(data_dir("daypart_ratings.csv"))



### individual-level moments from GfK data

gfk <- fread(paste0(local_dir, "data/mri/NewsIndicatorsWave75Clean.csv"))

stats <- gfk[, .(ABC = mean(anyABCNightlyNews),
          CBS = mean(anyCBSNightlyNews),
          CNN = mean(anyCNN),
          FNC = mean(anyFNC),
          MSNBC = mean(anyMSNBC),
          NBC = mean(anyNBCNightlyNews),
          ABC_CBS = mean(anyABCNightlyNews * anyCBSNightlyNews),
          ABC_CNN = mean(anyABCNightlyNews * anyCNN),
          ABC_FNC = mean(anyABCNightlyNews * anyFNC),
          ABC_MSNBC = mean(anyABCNightlyNews * anyMSNBC),
          ABC_NBC = mean(anyABCNightlyNews * anyNBCNightlyNews),
          CBS_CNN = mean(anyCBSNightlyNews * anyCNN),
          CBS_FNC = mean(anyCBSNightlyNews * anyFNC),
          CBS_MSNBC = mean(anyCBSNightlyNews * anyMSNBC),
          CBS_NBC = mean(anyCBSNightlyNews * anyNBCNightlyNews),
          CNN_FNC = mean(anyCNN * anyFNC),
          CNN_MSNBC = mean(anyCNN * anyMSNBC),
          CNN_NBC = mean(anyCNN * anyNBCNightlyNews),
          FNC_MSNBC = mean(anyFNC * anyMSNBC),
          FNC_NBC = mean(anyFNC * anyNBCNightlyNews),
          MSNBC_NBC = mean(anyMSNBC * anyNBCNightlyNews))] %>%
          melt(variable.name = "stat", value.name = "value")

stats[, se := sqrt(value * (1 - value) / nrow(gfk))]

fwrite(stats, file = data_dir("viewership_indiv_rawmoments.csv"))
