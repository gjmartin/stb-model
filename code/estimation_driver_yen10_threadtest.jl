## Set your local directory
local_dir = "/home/users/gjmartin"
# local_dir = "/Users/mitchelllinegar/Code"
# stb_dir = "stb-model-event"
stb_dir = "stb-model-event"
# set to something nonempty to limit dates used in estimation
dates_to_keep = []  


# to sample only beta
# tree_base = "beta"

# # to sample all
tree_base = ""

### END OPTIONS ###

## Directory locations for code and data
import Dates
using FileIO
using Plots
code_dir = "$local_dir/$stb_dir/code"
data_dir = "$local_dir/$stb_dir/data"
output_dir = "$local_dir/$stb_dir/output"
sampling_dir = "$local_dir/$stb_dir/sampling"


## LOAD OBJECTIVE, DATA, PARAMETERS ##
include("$code_dir/load_model_data.jl")
include("$code_dir/helper_functions.jl")

# USE LAST RUN INSTEAD OF INITIAL PARAMETERS
# pb, par_init = params_from_csv("$output_dir/bboptim_best_121622.csv", par_bounds)
pb, par_init = params_from_csv("$data_dir/parameter_init.csv", par_bounds)

# add pars to mprob object
SMM.addSampledParam!(mprob,pb);

ev = SMM.Eval(mprob)

news_obj(ev; dt = stbdat)
@time news_obj(ev; dt=stbdat)

