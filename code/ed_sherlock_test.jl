#### TESTING ####
# call to Distributed not needed when julia started with -p
using Distributed
# launch worker processes
# num_cores = parse(Int, ENV["SLURM_CPUS_PER_TASK"])
# num_cores = 4
# addprocs(num_cores)

### BEGIN OPTIONS ###
@everywhere begin
    ## Set your local directory
    # local_dir = "/home/gjmartin/Dropbox/STBNews"
    # local_dir = "/Users/mitchelllinegar/Code"
    local_dir = "/home/users/mlinegar/stb"
    # stb_dir = "stb-model-event"
    stb_dir = "stb-model"
    # set to something nonempty to limit dates used in estimation
    dates_to_keep = []  

    # to sample only beta
    # tree_base = "beta"

    # # to sample all
    tree_base = ""

    ### END OPTIONS ###
    
    # LOAD BASE PACKAGES
    # some packages need
    using Printf
    using BlackBoxOptim
end
# this should write to the log file
# @printf("Number of threads used: %s", Threads.nthreads() )

# note: have to have `using` in a different block than anywhere it will be called
# since the block is compiled before it is evaluated
## LOAD OBJECTIVE, DATA, PARAMETERS ##
@everywhere begin
       
    ## Directory locations for code and data
    code_dir = @sprintf("%s/%s/code", local_dir, stb_dir)
    data_dir = @sprintf("%s/%s/data", local_dir, stb_dir)
    output_dir = @sprintf("%s/%s/output", local_dir, stb_dir)
    sampling_dir = @sprintf("%s/%s/sampling", local_dir, stb_dir)

    include("$code_dir/load_model_data.jl")
end

@everywhere begin
# add pars to mprob object
    SMM.addSampledParam!(mprob,pb);

    ev = SMM.Eval(mprob)
    # dt=stbdat
    println(@time news_obj(ev; dt=stbdat))
end


@everywhere begin
    # add pars to mprob object
        SMM.addSampledParam!(mprob,pb);
    
        ev = SMM.Eval(mprob)
        # dt=stbdat
        println(@time news_obj(ev; dt=stbdat))
end