# TESTING

## Set your local directory
if occursin("greg", pwd())
    local_dir = "/home/greg"
elseif occursin("users", pwd())
    local_dir = "/home/users/mlinegar/stb"
else
    local_dir = "/Users/mlinegar/Code"
end



# local_dir = "/home/greg"
# local_dir = "/Users/mlinegar/Code"
# local_dir = "/home/users/mlinegar/stb"
# stb_dir = "stb-model-event"
stb_dir = "stb-model"
# set to something nonempty to limit dates used in estimation
dates_to_keep = []  


# to sample only beta
# tree_base = "beta"

# # to sample all
tree_base = ""

### END OPTIONS ###

## Directory locations for code and data
import Dates
using Optimization, ForwardDiff, OptimizationOptimJL
using FileIO
using Plots
code_dir = "$local_dir/$stb_dir/code"
data_dir = "$local_dir/$stb_dir/data"
output_dir = "$local_dir/$stb_dir/output"
sampling_dir = "$local_dir/$stb_dir/sampling"


## LOAD OBJECTIVE, DATA, PARAMETERS ##
include("$code_dir/load_model_data.jl")
include("$code_dir/helper_functions.jl")

# USE LAST RUN INSTEAD OF INITIAL PARAMETERS
pb, par_init = params_from_csv("$output_dir/bboptim_best_112822.csv", par_bounds)

# join with the bounds definition
par_init = innerjoin(par_init[:,[:parameter,:value]], par_bounds, on=:parameter)

## correctly order parameters in par_init 
par_init[!, :parorder] .= 0

par_init[occursin.(r"topic", par_init.parameter), :parorder] = parse.(Int64, replace.(par_init[occursin.(r"topic", par_init.parameter), :parameter], r"beta:topic:" => s""))
par_init[occursin.(r"ETZ", par_init.parameter), :parorder] .= 1
par_init[occursin.(r"CTZ", par_init.parameter), :parorder] .= 2
par_init[occursin.(r"MTZ", par_init.parameter), :parorder] .= 3
par_init[occursin.(r"PTZ", par_init.parameter), :parorder] .= 4

sort!(par_init, [:parorder, :parameter])

# create dictionary indexed by parameter, with initial values and bounds for each
pb = DataStructures.OrderedDict(zip(
    par_init.parameter,
    DataFrames.eachrow(par_init[:, [:value, :lb, :ub]]),
))


# add pars to mprob object
SMM.addSampledParam!(mprob,pb);

ev = SMM.Eval(mprob)

to_optimize = filter(x -> .!(occursin.(r"alpha", x)), String.(par_init.parameter)) 

x0 = [par_init[findfirst(par_init.parameter .== p), :value] for p in to_optimize]


function f0(x)
    # wrap param vector in OrderedDict
    pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? x[findfirst(k .== to_optimize)] : pb[k].value for k in keys(pb))

    # wrap in Eval object
    ev = SMM.Eval(mprob, pb_val)
    
    # call objective
    obj = news_obj(ev; dt=stbdat)

    obj.value
end

f0(x0)

# local obj: 0.45395062272791853
# local sum(x0):    -36.10467082132669

# sherlock obj: 0.4484031092018742
# sherlock sum(x0): -36.104670821326685

# difference: view_by_daypart
# difference caused by: different dt.dma_aggregator