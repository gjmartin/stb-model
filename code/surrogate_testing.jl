using Distributed

@everywhere begin
  ## Set your local directory
  # local_dir = "/home/greg"
  # local_dir = "/Users/mitchelllinegar/Code"
  local_dir = "/Users/mlinegar/Code"
  # stb_dir = "stb-model-event"
  stb_dir = "stb-model"
  # set to something nonempty to limit dates used in estimation
  dates_to_keep = []  


  # to sample only beta
  # tree_base = "beta"

  # # to sample all
  tree_base = ""
  train_frac = 0.8
  validation_frac = 0.1
  # number of runs to test Surrogate models
  N = 100


  ### END OPTIONS ###

  ## Directory locations for code and data
  import Dates
  using Distributions
  using Surrogates
  using SurrogatesRandomForest
  using SurrogatesPolyChaos
  using SurrogatesAbstractGPs
  using Flux
  using SurrogatesFlux
  using AbstractGPs
  using SurrogatesAbstractGPs
  using Serialization
  using Statistics

  using Hyperopt

  code_dir = "$local_dir/$stb_dir/code"
  data_dir = "$local_dir/$stb_dir/data"
  output_dir = "$local_dir/$stb_dir/output"
  sampling_dir = "$local_dir/$stb_dir/sampling"


  ## LOAD OBJECTIVE, DATA, PARAMETERS ##
  println("Initializing...")
  include("$code_dir/load_model_data.jl")
  include("$code_dir/helper_functions.jl")

  # USE LAST RUN INSTEAD OF INITIAL PARAMETERS
  # pb, par_init = params_from_csv("$output_dir/bboptim_best_112822.csv", par_bounds)


  # add pars to mprob object
  SMM.addSampledParam!(mprob,pb);

  ev = SMM.Eval(mprob)

  # ## uncomment to read in trial evals generated from diff evo optimizer
  # trials = CSV.File("$output_dir/parameter_tracker.csv")  |> DataFrame;
  # to_optimize = intersect(names(trials), par_init.parameter)


  to_optimize = ["beta:info",
                "beta:slant",
                "beta:zero",
                "beta:const",
                "beta:mu",
                "beta:sigma",
                "beta:base_slant:ABC",
                "beta:base_slant:CBS",
                "beta:base_slant:CNN",
                "beta:base_slant:FNC",
                "beta:base_slant:MSNBC",
                "beta:base_slant:NBC",
                "beta:channel:CBS",
                "beta:channel:CNN",
                "beta:channel:FNC",
                "beta:channel:MSNBC",
                "beta:channel:NBC",
                "rho0:0",
                "rho0:1"
                ]

  n_par = length(to_optimize)

  ub = [par_init[findfirst(par_init.parameter .== p), :ub] for p in to_optimize]
  lb = [par_init[findfirst(par_init.parameter .== p), :lb] for p in to_optimize]

  function f(x)
      pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? x[findfirst(k .== to_optimize)] : pb[k].value for k in keys(pb))
      # wrap in Eval object
      ev = SMM.Eval(mprob, pb_val)
      return news_obj(ev; dt=stbdat, store_moments=false).value
  end

  past = CSV.read("$output_dir/fval_samples_randomforest_lcbs.csv", DataFrame)
  unique!(past)
  past = past[past.fv .< 0.85,:]

  rands = rand(Uniform(0,1), size(past,1))
  train = past[rands .<= train_frac,:]
  validation = past[rands .> train_frac .&& rands .<= (train_frac + validation_frac), :]
  test = past[rands .> (train_frac + validation_frac),:]

  y_train = train[!, :fv]
  x_train = select(train, Not(:fv))
  x_train = Tuple.(eachrow(x_train))

  y_validation = validation[!, :fv]
  x_validation = select(validation, Not(:fv))
  x_validation = Tuple.(eachrow(x_validation))

  y_test = test[!, :fv]
  x_test = select(test, Not(:fv))
  x_test = Tuple.(eachrow(x_test))



  function train_surr(surr_name, x_train=x_train, y_train=y_train)
    surr = getfield(Surrogates, Symbol(surr_name))(
      x_train,
      y_train,
      lb,
      ub
    )
    return surr
  end

  function test_surr(surr_values, y_test=y_test)
    sq = zeros(length(y_test))
    for i in eachindex(y_test)
      # sq[i] = (y_test[i] - surr(x_test[i]))^2
      sq[i] = (y_test[i] - surr_values[i])^2
    end
    return Statistics.mean(sq)
  end

  # needs more work: "NeuralSurrogate", "Wendland", "AbstractGPSurrogate
  # not working: "LobacheskySurrogate"
  # unknown: 
  surrogate_options = [
    "SecondOrderPolynomialSurrogate", 
    "RadialBasis", 
    "LinearSurrogate", 
    "Kriging",
    "InverseDistanceSurrogate"
  ]

  mses = zeros(length(surrogate_options))
  for i in eachindex(surrogate_options)
    println(surrogate_options[i])
    surr = train_surr(surrogate_options[i])
    mses[i] = test_surr(surr.(x_test))
  end

  surrogate_mses = DataFrame(surrogate = surrogate_options, mse = mses)
end

##### RandomForestSurrogate ######
@everywhere begin
  surr_name = "RandomForestSurrogate"
  function g(n)
    surr = RandomForestSurrogate(x_train, y_train, lb, ub, num_round = n)
    return test_surr(surr.(x_validation), y_validation)
  end
end

ho = @phyperopt for n=N,
  sampler = RandomSampler(), 
  n = range(2, 500, step=10)
# print(i, "\t", n, "   \t")
@show g(n)
end

surr = RandomForestSurrogate(x_train, y_train, lb, ub, num_round = ho.minimizer[1])
mse_df = DataFrame(surrogate = surr_name, mse = test_surr(surr.(x_test)))
surrogate_mses = vcat(surrogate_mses, mse_df)


###### AbstractGPSurrogate #####
@everywhere begin
surr_name = "AbstractGPSurrogate"
# function g()
function g(gp, Σy)
  # surr = AbstractGPSurrogate(x_train, y_train, lb, ub, gp = gp, Σy = Σy)
  try
    surr = SurrogatesAbstractGPs.AbstractGPSurrogate(x_train, y_train, gp = gp, Σy = Σy)
    val = test_surr(surr.(x_validation), y_validation)
  catch
    val = 99999
  end
  return val
end
end
ho = @phyperopt for n=N,
  sampler = RandomSampler(), 
  a = [
  # GP(GibbsKernel()), 
  GP(SqExponentialKernel()), GP(Matern32Kernel()), GP(Matern52Kernel()), GP(EyeKernel()),
  GP(CosineKernel()), GP(ExponentialKernel()), GP(GammaExponentialKernel()), GP(LaplacianKernel()),
  GP(SqExponentialKernel()), GP(RBFKernel()), GP(ExponentiatedKernel()), GP(FBMKernel()), GP(gaborkernel())
  ],  
  b = LinRange(0,1,100)
print(i, "\t", n, "   \t")
@show g(a, b)
end

surr = SurrogatesAbstractGPs.AbstractGPSurrogate(x_train, y_train, gp = ho.minimizer[1], Σy = ho.minimizer[2])
mse_df = DataFrame(surrogate = surr_name, mse = test_surr(surr.(x_test)))
surrogate_mses = vcat(surrogate_mses, mse_df)
###### PolynomialChaosSurrogate #####




###### LobachevskySurrogate #####
# FIXME: come back and make alpha not have to all be the same value
@everywhere begin
surr_name = "LobachevskySurrogate"
function g(alpha, n)
  surr = LobachevskySurrogate(x_train, y_train, lb, ub, alpha = repeat([alpha], outer=n_par), n = n)
  return test_surr(surr.(x_validation), y_validation)
end
end

ho = @phyperopt for n=N,
  sampler = RandomSampler(), 
  alpha = exp10.(LinRange(-1,log10(4),100)),
  n = range(2, 10, step=2)
# print(i, "\t", n, "   \t")
@show g(alpha, n)
end

surr = LobachevskySurrogate(x_train, y_train, lb, ub, alpha = repeat([ho.minimizer[1]], outer=n_par), n = ho.minimizer[2])
mse_df = DataFrame(surrogate = surr_name, mse = test_surr(surr.(x_test)))
surrogate_mses = vcat(surrogate_mses, mse_df)


##### Wendland ######
# takes a long time
@everywhere begin
surr_name = "Wendland"
function g(e)
  surr = Wendland(x_train, y_train, lb, ub, eps = e)
  return test_surr(surr.(x_validation), y_validation)
end
end
ho = @phyperopt for n=2,
  sampler = RandomSampler(), 
  e = exp10.(LinRange(-10,log10(1),100))
# print(i, "\t", n, "   \t")
@show g(e)
end

surr = Wendland(x_train, y_train, lb, ub, num_round = ho.minimizer[1])
mse_df = DataFrame(surrogate = surr_name, mse = test_surr(surr.(x_test)))
surrogate_mses = vcat(surrogate_mses, mse_df)


##### Neural ######
# takes a long time
@everywhere begin
    surr_name = "NeuralSurrogate"
    function g(mod)
      surr = NeuralSurrogate(x_train, y_train, lb, ub, model = mod, n_echos = 50)
      return test_surr(surr.(x_validation), y_validation)
    end
    end
    ho = @phyperopt for n=N,
      sampler = RandomSampler(), 
      n1 = range(2, n_par, step=1),
      n2 = range(2, n_par, step=1),
      n3 = range(2, n_par, step=1),
      n4 = range(2, n_par, step=1)
    # print(i, "\t", n, "   \t")
    mod = Chain(
        Dense(n_par, n1, σ),
        Dense(n1,n2,σ),
        Dense(n2,n3,σ),
        Dense(n3,n4,σ),
        Dense(n4, 1)    
    )
    @show g(mod)
    end
    
    test_mod = Chain(
        Dense(n_par, ho.minimizer[1], σ),
        Dense(ho.minimizer[1],ho.minimizer[2],σ),
        Dense(ho.minimizer[2],ho.minimizer[3],σ),
        Dense(ho.minimizer[3],ho.minimizer[4],σ),
        Dense(ho.minimizer[4], 1)    
    )
    surr = NeuralSurrogate(x_train, y_train, lb, ub, model = test_mod, n_echos = 10)
    mse_df = DataFrame(surrogate = surr_name, mse = test_surr(surr.(x_test)))
    surrogate_mses = vcat(surrogate_mses, mse_df)
    
    


surr = train_surr(surrogate_options[1])

sec = SecondOrderPolynomialSurrogate(x, y, lb, ub)

plot(x, y, seriestype=:scatter, label="Sampled points", xlims=(lb, ub))
plot!(f, label="True function",  xlims=(lb, ub))
plot!(sec, label="Surrogate function",  xlims=(lb, ub))

to_optimize

function xdim(varname)
    # varname = "beta:info"
    j = findall(to_optimize.==varname)
    nx = length(x_train)
    x = zeros(nx)
    for i in 1:nx
        x[i] = x_train[i][j][1]
    end
    return x
end


using Plots
function surr_plot(varname)
    j = findall(to_optimize.==varname)
    surr_val = surr.(x_train)
    plot(xdim(varname), y_train, seriestype=:scatter, label="Sampled points", xlims=(lb[j], ub[j]))
    plot!(surr_val, label="Surrogate function",  xlims=(lb[j], ub[j]))
    # title!("Trigonometric functions")
    xlabel!(varname)
    ylabel!("Objective")
end

for v in 1:length(to_optimize)
    p = surr_plot(to_optimize[v])
    display(p)
end


function surr_x(x, varname)
    j = findall(to_optimize.==varname)
    
    surr.(x_train[1])
end

