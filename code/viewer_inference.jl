function init_μ!(μ,d,τ)
    # init beliefs today given 1 period of possible transition since yesterday
    if d > 1
	    @views μ[:,d] .= μ[:,d-1] .* τ .+ (1 .- μ[:,d-1]) .* (1 .- τ)
    end
end

function init_μ_η!(μ_η, μ, d, topic_ξ, topic_r)
    # init beliefs over event probabilities today given belief about the state
    for i = 1:size(μ,1)
        μ_η[:,i] .= topic_ξ .* (μ[i,d] .* topic_r .+ 1 .- μ[i,d])
    end
end

function update_μ!(μ, d, ρ_diff, ρ_0, signals, topic_ξ, topic_r)
    # update beliefs over state upon observing signal η^⋆_kt = 1
    # used for free signals (all i see the same)
    @views for (k, signal) in enumerate(signals[:,d])
        if signal
            μ[:,d] .*= (topic_ξ[k] .* topic_r[k] .* ρ_diff .+ ρ_0) ./ (topic_ξ[k] .* ρ_diff .* (1 .+ μ[:,d] .* (topic_r[k] .- 1)) .+ ρ_0)
        else
            μ[:,d] .*= (1 .- topic_ξ[k] .* topic_r[k] .* ρ_diff .- ρ_0) ./ (1 .- topic_ξ[k] .* ρ_diff .* (1 .+ μ[:,d] .* (topic_r[k] .- 1)) .- ρ_0)
        end
    end
end
    
function update_μ_η!(μ_η, pr_1, pr_0, ρ_1, ρ_0, d, signals, topic_ξ, topic_r)
    # update beliefs over η_kt upon observing signals η^⋆_kt
    map!(s -> s ? ρ_1 : 1 - ρ_1, pr_1, signals[:,d])
    map!(s -> s ? ρ_0 : 1 - ρ_0, pr_0, signals[:,d])
    
    μ_η .*= pr_1 ./ (pr_1 .* μ_η .+ pr_0 .* (1 .- μ_η))
end

function update_Δ_1!(i, d, Δ, μ, ρ_diff, ρ_0, topic_ξ, topic_r)
    # expects Δ_1 is KxC (one individual, one block for all topics by all channels)
    # topic_r and topic_ξ are K x 1
    # ρ_diff and ρ_0 are K x C
    Δ[:,:,i] .= μ[i,d] .* ((topic_ξ .* topic_r .* ρ_diff .+ ρ_0) ./ (topic_ξ .* ρ_diff .* (1 .+ μ[i,d] .* (topic_r .- 1)) .+ ρ_0) .- 1)
end

function update_Δ_0!(i, d, Δ, μ, ρ_diff, ρ_0, topic_ξ, topic_r)
    # expects Δ_0 is KxC (one individual, one block for all topics by all channels)
    # topic_r and topic_ξ are K x 1
    # ρ_diff and ρ_0 are K x C
    Δ[:,:,i] .= μ[i,d] .* ((1 .- topic_ξ .* topic_r .* ρ_diff .- ρ_0) ./ (1 .- topic_ξ .* ρ_diff .* (1 .+ μ[i,d] .* (topic_r .- 1)) .- ρ_0) .- 1)
end
