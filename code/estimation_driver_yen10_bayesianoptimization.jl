## Set your local directory
local_dir = "/home/users/gjmartin"

# local_dir = "/Users/mitchelllinegar/Code"
# stb_dir = "stb-model-event"
stb_dir = "stb-model"
# set to something nonempty to limit dates used in estimation
dates_to_keep = []  


# to sample only beta
# tree_base = "beta"

# # to sample all
tree_base = ""

### END OPTIONS ###

## Directory locations for code and data
import Dates
using BayesianOptimization, GaussianProcesses, Distributions
using Surrogates
using Serialization

code_dir = "$local_dir/$stb_dir/code"
data_dir = "$local_dir/$stb_dir/data"
output_dir = "$local_dir/$stb_dir/output"
sampling_dir = "$local_dir/$stb_dir/sampling"


## LOAD OBJECTIVE, DATA, INITIAL PARAMETERS ##
println("Initializing...")
include("$code_dir/load_model_data.jl")
include("$code_dir/helper_functions.jl")


# add pars to mprob object
SMM.addSampledParam!(mprob,pb);

ev = SMM.Eval(mprob)

# ## uncomment to read in trial evals generated from diff evo optimizer
# trials = CSV.File("$output_dir/parameter_tracker.csv")  |> DataFrame;
# to_optimize = intersect(names(trials), par_init.parameter)


to_optimize = ["beta:info",
               "beta:slant",
               "beta:zero",
               "beta:const",
               "beta:mu",
               "beta:sigma",
               "beta:base_slant:ABC",
               "beta:base_slant:CBS",
               "beta:base_slant:CNN",
               "beta:base_slant:FNC",
               "beta:base_slant:MSNBC",
               "beta:base_slant:NBC",
               "beta:channel:CBS",
               "beta:channel:CNN",
               "beta:channel:FNC",
               "beta:channel:MSNBC",
               "beta:channel:NBC",
               "rho0:0",
               "rho0:1"
               ]


x0 = [par_init[findfirst(par_init.parameter .== p), :value] for p in to_optimize]
ub = [par_init[findfirst(par_init.parameter .== p), :ub] for p in to_optimize]
lb = [par_init[findfirst(par_init.parameter .== p), :lb] for p in to_optimize]

## read trials from radial basis and second order polynomial surrogate
println("Reading start point from file $output_dir/surrogate_optimizer_radialbasis.dat")
poly = deserialize("$output_dir/surrogate_optimizer_radialbasis.dat")

## append from random forest one (which never made it to saving the object step)
rf_trials = CSV.File("$output_dir/fval_samples_randomforest_unique.csv") |> DataFrame
rf_ys = rf_trials.fv
rf_xs = [Tuple(r) for r in eachrow(select(rf_trials, Not(:fv)))]

function f(x)
    pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? x[findfirst(k .== to_optimize)] : pb[k].value for k in keys(pb))
    # wrap in Eval object
    ev = SMM.Eval(mprob, pb_val)
    val = news_obj(ev; dt=stbdat, store_moments=false).value
    [merge((; zip(Symbol.(to_optimize), x)...), (fv = val,))] |> CSV.write("$output_dir/fval_samples_bayesianoptimization.csv"; append = true)
    return -1 * val  # note direction reversal here
end

## uncomment to generate new samples as starting point
xs = copy(poly.x)
ys = -1 .* copy(poly.y)  # note direction reversal here

# new_xs = sample(2000,lb,ub,SobolSample())
append!(xs, rf_xs)
# new_ys = f.(new_xs)
append!(ys, -1 .* rf_ys)

ll = log.([Statistics.std([x[i] for x in xs]) for i in 1:length(to_optimize)])


model = ElasticGPE(length(to_optimize),                            
                   mean = MeanConst(-0.85),         
                   kernel = SEArd(ll, 5.),
                   capacity = length(ys))

# Optimize the hyperparameters of the GP using maximum a posteriori (MAP) estimates every 50 steps
modeloptimizer = MAPGPOptimizer(every = 10,
                                noisebounds = [-2, -2],       # bounds of the logNoise
                                kernbounds = [vcat(repeat([-2], length(to_optimize)), 0),
                                              vcat(repeat([4], length(to_optimize)), 10)],  # bounds of the 3 parameters GaussianProcesses.get_param_names(model.kernel)
                                maxeval = 40)


## add warm starts
append!(model, hcat([collect(x) for x in xs]...), ys)

## optimize
opt = BOpt(f,
           model,
           UpperConfidenceBound(),
           modeloptimizer,                        
           lb, ub;
           maxiterations = 100,
           repetitions = 1,
           sense = Max,
           acquisitionoptions = (method = :LD_LBFGS, # run optimization of acquisition function with NLopts :LD_LBFGS method
           restarts = 5,       # run the NLopt method from 5 random initial conditions each time.
           maxtime = 1,      # run the NLopt method for at most 1 second each time
           maxeval = 1000),
           initializer_iterations = 0
          )


println("Best fval before optimization = $(maximum(ys)), of $(length(ys)) function evaluations.")

result = boptimize!(opt)

