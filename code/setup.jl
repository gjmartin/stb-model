using Printf
using FileIO
using Dates

function get_local_directory()
    if occursin(r"gjmartin", pwd())
        return "/home/users/gjmartin"
    elseif occursin(r"mlinegar", pwd())
        if occursin(r"Users", pwd())
            return "/Users/mlinegar/Code/"
        else
            return "/home/users/mlinegar/"
        end
    elseif occursin(r"mitchelllinegar", pwd())
        return "/Users/mitchelllinegar/Code/"
    end
end

local_dir = get_local_directory()
stb_dir = "stb-model"

code_dir = "$local_dir/$stb_dir/code"
data_dir = "$local_dir/$stb_dir/data"
output_dir = "$local_dir/$stb_dir/output"
sampling_dir = "$local_dir/$stb_dir/sampling"

# set to something nonempty to limit dates used in estimation
dates_to_keep = []  

# to sample only beta
# tree_base = "beta"

# # to sample all
tree_base = ""

## LOAD OBJECTIVE, DATA, PARAMETERS ##
include("$code_dir/load_model_data.jl")
include("$code_dir/helper_functions.jl")

cd(data_dir)

# initial parameters
pb00, par_init00 = params_from_csv("$data_dir/parameter_init.csv", par_bounds)#  re-order bounds
to_optimize = String.(par_init00.parameter)  ## everything
par_init00 = par_init00[indexin(to_optimize, par_init00.parameter),:]


function load_and_update_parameters(output_dir, data_dir, to_optimize, par_bounds, inds_to_ignore = nothing)
    best_param_files = readdir(output_dir)[occursin.(r"bboptim_best_", readdir(output_dir))]
    best_param_dates = Dates.Date.(first.(last.(best_param_files, 10),6),"mmddyy")
    most_recent_best_param_file_name = best_param_files[findmax(best_param_dates)[2]]
    most_recent_best_param_file = "$output_dir/$most_recent_best_param_file_name"
    
    pb, par_init = params_from_csv(most_recent_best_param_file, par_bounds)
    pb0, par_init0 = params_from_csv("$output_dir/bboptim_best_121622.csv", par_bounds)
    pb00, par_init00 = params_from_csv("$data_dir/parameter_init.csv", par_bounds)
    
    if inds_to_ignore !== nothing
        to_optimize = to_optimize[eachindex(to_optimize) .∉ inds_to_ignore]
    end
    
    # which set of parameters should be read in as the default? 
    pb_default = pb00
    pb_val = DataStructures.OrderedDict(k => (k ∈ to_optimize && k ∈ string.(keys(pb))) ? pb[to_optimize[findfirst(k .== to_optimize)]].value : pb_default[k].value for k in string.(keys(pb_default)))
    ## bounds for search space
    ub = par_init00.ub
    lb = par_init00.lb
    ub = ub[eachindex(ub) .∉ inds_to_ignore]
    lb = lb[eachindex(lb) .∉ inds_to_ignore]

    return pb_val, pb0, to_optimize, ub, lb
end
