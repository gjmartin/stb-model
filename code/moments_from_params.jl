## Set your local directory
if occursin(r"gjmartin", pwd())
    local_dir = "/home/users/gjmartin"
elseif occursin(r"mlinegar", pwd())
    if occursin(r"Users", pwd())
        local_dir = "/Users/mlinegar/Code/"
    else
        # local_dir = "/home/users/mlinegar/stb"
        local_dir = "/home/users/mlinegar/"
    end
else occursin(r"mitchelllinegar", pwd())
    local_dir = "/Users/mitchelllinegar/Code"
end

# stb_dir = "stb-model-event"
stb_dir = "stb-model"
# set to something nonempty to limit dates used in estimation
dates_to_keep = []  

# to sample only beta
# tree_base = "beta"

# # to sample all
tree_base = ""

### END OPTIONS ###
## Directory locations for code and data
using Printf
# using Serialization
# using Surrogates
# using SurrogatesRandomForest

# to_optimize = [
# "beta:info",
# "beta:slant",
# "beta:zero",
# "beta:const",
# "beta:mu",
# "beta:sigma",
# "beta:base_slant:ABC",
# "beta:base_slant:CBS",
# "beta:base_slant:CNN",
# "beta:base_slant:FNC",
# "beta:base_slant:MSNBC",
# "beta:base_slant:NBC",
# "beta:channel:CBS",
# "beta:channel:CNN",
# "beta:channel:FNC",
# "beta:channel:MSNBC",
# "beta:channel:NBC",
# "rho0:0",
# "rho0:1"
# ]

using FileIO

code_dir = @sprintf("%s/%s/code", local_dir, stb_dir)
data_dir = @sprintf("%s/%s/data", local_dir, stb_dir)
output_dir = @sprintf("%s/%s/output", local_dir, stb_dir)
sampling_dir = @sprintf("%s/%s/sampling", local_dir, stb_dir)

## LOAD OBJECTIVE, DATA, PARAMETERS ##
include("$code_dir/load_model_data.jl")
include("$code_dir/helper_functions.jl")

cd(data_dir)

# USE LAST RUN INSTEAD OF INITIAL PARAMETERS
# get most recent parameter file
best_param_files = readdir(output_dir)[occursin.(r"bboptim_best_", readdir(output_dir))]
# best_param_dates = Dates.Date.(first.(last.(best_param_files, 10),6),dateformat"mmddyy")
best_param_dates = Dates.Date.(first.(last.(best_param_files, 10),6),"mmddyy")


most_recent_best_param_file_name = best_param_files[findmax(best_param_dates)[2]]

parameter_tracker = CSV.read("$output_dir/parameter_tracker.csv", DataFrame)
best_parameter_tracker = parameter_tracker[findmin(parameter_tracker[!,:fval])[2], :]
best_parameter_tracker_ind = findmin(parameter_tracker[!,:fval])[2]

# most_recent_best_param_file_name = "bboptim_best_031323.csv"
most_recent_best_param_file = "$output_dir/$most_recent_best_param_file_name"
pb, par_init = params_from_csv(most_recent_best_param_file, par_bounds)

pb0, par_init0 = params_from_csv("$output_dir/bboptim_best_121622.csv", par_bounds)



# initial parameters parameters
pb00, par_init00 = params_from_csv("$data_dir/parameter_init.csv", par_bounds)

to_optimize = String.(par_init0.parameter)  ## everything
# to_optimize = String.(par_init.parameter[findall(occursin.(r"info|slant|topic_mu|topic_leisure", par_init.par))])
# to_optimize = [
# "beta:info",
# "beta:slant",
# "beta:zero",
# "beta:const",
# "beta:mu",
# "beta:sigma",
# "beta:base_slant:ABC",
# "beta:base_slant:CBS",
# "beta:base_slant:CNN",
# "beta:base_slant:FNC",
# "beta:base_slant:MSNBC",
# "beta:base_slant:NBC",
# "beta:channel:CBS",
# "beta:channel:CNN",
# "beta:channel:FNC",
# "beta:channel:MSNBC",
# "beta:channel:NBC",
# "rho0:0",
# "rho0:1"
# ]
# keep alphas fixed
inds_to_ignore =  Ref(findall(occursin.(r"alpha|beta:info|topic", to_optimize)))
to_optimize = to_optimize[eachindex(to_optimize) .∉ inds_to_ignore]

# pb["beta:base_slant:CNN"].value = -1.0 .* pb["beta:base_slant:CNN"].value
pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? pb[to_optimize[findfirst(k .== to_optimize)]].value : pb00[k].value for k in string.(keys(pb00)))
# From parameter_tracker
pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? parameter_tracker[best_parameter_tracker_ind,Symbol(k)] : pb00[k].value for k in string.(keys(pb00)))
# pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? x[findfirst(k .== to_optimize)] : pb[k] for k in keys(pb))

# set topics to zero
topic_pars = String.(par_init0.parameter)[occursin.(r"topic", String.(par_init0.parameter))]
pb_val = DataStructures.OrderedDict(k => k ∈ topic_pars ? 0.0 : pb_val[k] for k in keys(pb_val))


# USE RANDOM FOREST SURROGATE RUNS
# rf = deserialize("$output_dir/surrogate_optimizer_randomforest.dat")
# best_x = rf.x[findmin(rf.y)[2]]
# pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? best_x[findfirst(k .== to_optimize)] : pb[k].value for k in keys(pb))

# add pars to mprob object
SMM.addSampledParam!(mprob,pb);
ev = SMM.Eval(mprob, pb_val);

res = news_obj(ev; dt=stbdat, store_moments=true)
res.value
# only viewership moments
# sum([(res.simMoments[k] - SMM.dataMomentd(res)[k])^2 * SMM.dataMomentWd(res)[k] for k in Symbol.(viewership_names.name)])

# read in moments, output for reading in with R
moments = stack(DataFrame(res.simMoments))
rename!(moments, [:moment, :value])
# output moments
CSV.write(@sprintf("%s/current_moments.csv", data_dir), moments)

# using JSON3
# using DataFrames
# json_files = readdir(output_dir)[occursin.(r"json", readdir(output_dir))]



# thread_timings = DataFrame(
#     threads = [1, 10, 20, 50, 100], 
#     time_ns_1 = [691756952143, 139883358950, 117643928662, 186759067555, 473431520182],
#     time_ns_2 = [680157252540, 139026935957, 125980031685, 280278518811, 332825820818]
#     )
# thread_timings[!,:time_s_1] = thread_timings[!,:time_ns_1] ./ 1e9
# thread_timings[!,:time_s_2] = thread_timings[!,:time_ns_2] ./ 1e9
# thread_timings[!,:sec_per_iteration_1] = thread_timings[!,:time_s_1] ./ 10
# thread_timings[!,:sec_per_iteration_2] = thread_timings[!,:time_s_2] ./ 10

# thread_timings[:, [:threads, :sec_per_iteration_1, :sec_per_iteration_2]]

# using Plots
# plot(thread_timings.threads, thread_timings.sec_per_iteration_1, label="1", legend=:topleft)
# plot!(thread_timings.threads, thread_timings.sec_per_iteration_2, label="2", legend=:topleft)