## Set your local directory
# local_dir = "/home/greg"
local_dir = "/Users/gjmartin"


# stb_dir = "stb-model-event"
stb_dir = "stb-model"
# set to something nonempty to limit dates used in estimation
dates_to_keep = []  


# to sample only beta
# tree_base = "beta"

# # to sample all
tree_base = ""

### END OPTIONS ###

## Directory locations for code and data
import Dates
using Surrogates
using SurrogatesRandomForest
using Serialization
using SurrogatesRandomForest

code_dir = "$local_dir/$stb_dir/code"
data_dir = "$local_dir/$stb_dir/data"
output_dir = "$local_dir/$stb_dir/output"
sampling_dir = "$local_dir/$stb_dir/sampling"


## LOAD OBJECTIVE, DATA, PARAMETERS ##
println("Initializing...")
include("$code_dir/load_model_data.jl")
include("$code_dir/helper_functions.jl")


# USE LAST RUN INSTEAD OF INITIAL PARAMETERS
# pb, par_init = params_from_csv("$output_dir/bboptim_best_112822.csv", par_bounds)

rf = deserialize("$output_dir/surrogate_optimizer_randomforest.dat")

to_optimize = ["beta:info",
               "beta:slant",
               "beta:zero",
               "beta:const",
               "beta:mu",
               "beta:sigma",
               "beta:base_slant:ABC",
               "beta:base_slant:CBS",
               "beta:base_slant:CNN",
               "beta:base_slant:FNC",
               "beta:base_slant:MSNBC",
               "beta:base_slant:NBC",
               "beta:channel:CBS",
               "beta:channel:CNN",
               "beta:channel:FNC",
               "beta:channel:MSNBC",
               "beta:channel:NBC",
               "rho0:0",
               "rho0:1"
               ]


best_x = rf.x[findmin(rf.y)[2]]

pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? best_x[findfirst(k .== to_optimize)] : pb[k].value for k in keys(pb))

# add pars to mprob object
SMM.addSampledParam!(mprob,pb);

ev = SMM.Eval(mprob, pb_val);
val = news_obj(ev; dt=stbdat, store_moments=false).value

# ## uncomment to read in trial evals generated from diff evo optimizer
# trials = CSV.File("$output_dir/parameter_tracker.csv")  |> DataFrame;
# to_optimize = intersect(names(trials), par_init.parameter)





ub = [par_init[findfirst(par_init.parameter .== p), :ub] for p in to_optimize]
lb = [par_init[findfirst(par_init.parameter .== p), :lb] for p in to_optimize]


## uncomment to use trial evals as starting point
# println("Reading initial trial samples from $output_dir/parameter_tracker.csv")
# xs = Tuple.(eachrow(trials[:, to_optimize]));
# ys = trials.fval;

# poly = SecondOrderPolynomialSurrogate(
#   xs,
#   ys,
#   lb,
#   ub)

## uncomment to use previous run as starting point
# println("Reading start point from file $output_dir/surrogate_optimizer.dat")
# poly = deserialize("$output_dir/surrogate_optimizer.dat")


function f(x)
    pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? x[findfirst(k .== to_optimize)] : pb[k].value for k in keys(pb))
    # wrap in Eval object
    ev = SMM.Eval(mprob, pb_val)
    return news_obj(ev; dt=stbdat, store_moments=false).value
end

## uncomment to generate new samples as starting point
x0 = Tuple([par_init[findfirst(par_init.parameter .== p), :value] for p in to_optimize])
xs = sample(500,lb,ub,SobolSample())
push!(xs, x0)
ys = f.(xs)

poly = SecondOrderPolynomialSurrogate(
  xs,
  ys,
  lb,
  ub)




surrogate_optimize(f, SRBF(), lb, ub, poly, SobolSample(), maxiters = 500)
println("Best fval = $(minimum(poly.y)), after $(length(poly.x)) function evaluations total.")
serialize("$output_dir/surrogate_optimizer.dat", poly)
