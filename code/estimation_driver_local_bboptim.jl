### OPTIONS: change things here ###

local_dir = "/home/greg"
# local_dir = "/Users/mitchelllinegar/Code"
# stb_dir = "stb-model-event"
stb_dir = "stb-model"
# set to something nonempty to limit dates used in estimation
dates_to_keep = []  


# to sample only beta
# tree_base = "beta"

# # to sample all
tree_base = ""

### END OPTIONS ###

## Directory locations for code and data
import Dates
using BlackBoxOptim
using FileIO
using Plots
code_dir = "$local_dir/$stb_dir/code"
data_dir = "$local_dir/$stb_dir/data"
output_dir = "$local_dir/$stb_dir/output"
sampling_dir = "$local_dir/$stb_dir/sampling"

## LOAD OBJECTIVE, DATA, PARAMETERS ##
include("$code_dir/load_model_data.jl")
include("$code_dir/helper_functions.jl")

# USE LAST RUN INSTEAD OF INITIAL PARAMETERS
pb, par_init = params_from_csv("$output_dir/best_pars.csv", par_bounds)

# compare to initialization
pb0, par_init0 = params_from_csv("$data_dir/parameter_init.csv", par_bounds)


# add pars to mprob object
SMM.addSampledParam!(mprob,pb);


### DIRECT OPTIMIZATION

## define subset of pars to optimize over
to_optimize = String.(par_init.parameter)  ## everything
# to_optimize = String.(par_init.parameter[findall(occursin.(r"info|slant|topic_mu|topic_leisure", par_init.par))])

## bounds for search space
# ub = par_init[findall(occursin.(r"info|slant|topic_mu|topic_leisure", par_init.par)), :ub]
# lb = par_init[findall(occursin.(r"info|slant|topic_mu|topic_leisure", par_init.par)), :lb]
ub = par_init.ub
lb = par_init.lb

## set initial parameter vector
x0 = [pb[k].value for k in to_optimize]


## set SD of noise using diff from best to init
x_diff = [abs(pb[k].value - pb0[k].value) for k ∈ to_optimize] ./ 10
x_diff = [max(x, 0.01) for x in x_diff]  # set floor

## generate initial candidates
rng = Random.MersenneTwister(14592150)
init_pop = Random.randn(rng, Float64, (length(to_optimize), 29)) .* x_diff .+ x0 

init_pop = min.(init_pop, ub)
init_pop = max.(init_pop, lb)

init_pop = [x0 init_pop]


## setup to write output
trace_file = open("$output_dir/bboptim_trace_$(Dates.now()).csv", "w")
write(trace_file, join(["fval"; to_optimize], ",") * "\n")


## function to optimize - calls news_obj and saves params / fval to csv
function f0(x)
    # wrap param vector in OrderedDict
    pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? x[findfirst(k .== to_optimize)] : pb[k].value for k in keys(pb))

    # wrap in Eval object
    ev = SMM.Eval(mprob, pb_val)

    fval = news_obj(ev; dt=stbdat).value

    ## write to trace file
    write(trace_file, join([fval; x], ",") * "\n")
    flush(trace_file)

    return fval
end




opt_params = Dict(:SearchSpace => BlackBoxOptim.ContinuousRectSearchSpace(lb, ub),
                  :Population => init_pop,
                  :MaxFuncEvals => 500,
                  :TraceInterval => 60.0,
                  :TraceMode => :verbose
                  )
opt_problem, opt_params = BlackBoxOptim.setup_problem(f0, chain(BlackBoxOptim.DefaultParameters, opt_params))


optimizer = BlackBoxOptim.adaptive_de_rand_1_bin_radiuslimited(opt_problem, opt_params)

opt_ctrl = BlackBoxOptim.OptController(optimizer, opt_problem, opt_params)

BlackBoxOptim.run!(opt_ctrl)

close(trace_file)


println(BlackBoxOptim.best_candidate(opt_ctrl))
println(BlackBoxOptim.best_fitness(opt_ctrl))

### TESTING ZONE ###
# # to evaluate at start point
# obj = stb_obj(SMM.Eval(mprob); dt=stbdat)
