function news_obj(ev::SMM.Eval;
				  dt::STBData,
				  save_output=false,
				  store_moments=false)

	SMM.start(ev)

	## extract parameters
    # topic parameters
    γ_0 = SMM.param(ev, Symbol("gamma:0"))
    γ_r = SMM.param(ev, Symbol("gamma:r"))
    
    topic_r_log = γ_0 .+ (γ_r .* dt.topic_r_hat)
    topic_r = exp.(topic_r_log)
    topic_ξ = 2 .* dt.topic_freq ./ (topic_r .+ 1)

    # taste parameters
    β = SMM.param(ev, Symbol("beta:const"))          # constant
    β_news_zero = SMM.param(ev, Symbol("beta:zero")) # chance of zero news taste
    β_news_σ = SMM.param(ev, Symbol("beta:sigma"))   # lognormal σ
    β_news_μ = SMM.param(ev, Symbol("beta:mu"))      # lognormal μ
    β_C = vcat(0, SMM.param(ev, dt.keys_beta_channel))   # channel shifts (plus excluded channel normed to 0)
    β_CS = SMM.param(ev, dt.keys_beta_base_slant)     # baseline channel slants
    β_B = vcat(0, SMM.param(ev, dt.keys_beta_time))   # time of day x timezone shifts (plus excluded category normed to 0)
    β_T = SMM.param(ev, dt.keys_beta_leisure)       # topic leisure
    β_S = SMM.param(ev, Symbol("beta:slant"))       # slant taste

    # combine parameters with sim draws to get baseline tastes 
    zero_draws = (dt.pre_consumer_news_zeros .>= β_news_zero) 
    β_0 = β .+ zero_draws .* exp.(dt.pre_consumer_news_draws .* β_news_σ .+ β_news_μ)

    # init matrices for view probability and actual view choice for every person (C x N)
    view_prob = zeros(Float64, dt.C, dt.N)
    view_choice = BitArray(undef, (dt.C, dt.N))
    view_choice .= 0
    cumulative_view = zeros(Int, dt.N, dt.C)
    
	# init arrays to store the moments
	view_thresh = zeros(Float64, dt.C)
	view_thresh_cross = zeros(Float64, binomial(dt.C,2))
	view_by_tercile = zeros(Float64, dt.C, 3, dt.T)
    view_by_tz = zeros(Float64, dt.C, 4)
    view_by_daypart = zeros(Float64, length(β_B))

	# init moment vector and locations to store components
	sim_moments = zeros(Float64, length(SMM.dataMoment(ev)))
	cur_ind = 1
	inds_view_thresh = cur_ind:(cur_ind + length(view_thresh) - 1)
	cur_ind += length(view_thresh)
	inds_view_thresh_cross = cur_ind:(cur_ind + length(view_thresh_cross) - 1)
	cur_ind += length(view_thresh_cross)
	inds_view_by_tercile = cur_ind:(cur_ind + length(view_by_tercile) - 1)
	cur_ind += length(view_by_tercile)
    inds_view_by_daypart = cur_ind:(cur_ind + length(view_by_daypart) - 1)
	cur_ind += length(view_by_daypart)

    # main loop to compute viewing
    # each day is independent, can parallelize
	@threads for d = 1:dt.D
        t = (d-1) * dt.B + 1;

        for b = 1:dt.B
            # reset viewing
            view_choice .= false

            for i = 1:dt.N
                # the inner loop: simulate viewing and updating for one consumer in one block
                viewership_decision!(i, t, d, dt.consumers[i,:tz],
                                    view_prob, view_choice, cumulative_view,
                                    dt.consumers[!,:r_prob],
                                    β_0, β_C, β_CS, β_B, β_T, 0.0, β_S,
                                    topic_ξ, topic_r, topic_r_log,
                                    dt.network,
                                    dt.show_array,
                                    dt.topic_array,
                                    dt.tz_array,
                                    dt.consumer_choice_draws,
                                    zero_draws)
            end
            
            # compute ratings by tercile for this t 
            @views mul!(view_by_tercile[:,:,t], view_choice, dt.dma_aggregator')

            # compute ratings by daypart for this t
            mul!(view_by_tz, view_choice, dt.tz_aggregator)
            view_by_daypart[dt.tz_array[t,:]] .+= (sum(view_by_tz; dims = 1) |> vec)

            t += 1
        end
    end  # d loop
	

    ## Individual viewership moments
    compute_indiv_moments(dt, cumulative_view, view_thresh, view_thresh_cross)

    sim_moments[inds_view_thresh] = vec(view_thresh)
    sim_moments[inds_view_thresh_cross] = vec(view_thresh_cross)
    sim_moments[inds_view_by_tercile] = vec(view_by_tercile)
    sim_moments[inds_view_by_daypart] = view_by_daypart ./ dt.blocks_per_daypart 

	ssq = sum((sim_moments .- SMM.dataMoment(ev)).^2 .* SMM.dataMomentW(ev,collect(keys(ev.dataMomentsW))))

	# Set value of the objective function:
	#------------------------------------
	SMM.setValue!(ev, dt.T / 2 * ssq)
	# SMM.setValue!(ev, ssq)

	# also return the moments if requested
	#-----------------------
	if store_moments
		for (i,m) in enumerate(keys(SMM.dataMomentd(ev)))
			SMM.setMoments!(ev, m, sim_moments[i]);
		end
	end
	# flag for success:
	#-------------------
	ev.status = 1

	# finish and return
	SMM.finish(ev)

    return ev

end


function compute_indiv_moments(dt, consumer_view_totals, view_thresh, view_thresh_cross)
	#  cross-viewing
	# thresh_one = [0.18, 0.36, 3.6, 36] .* dt.D ./ 15
	# thresh_two = [0.18, 0.36, 3.6] .* dt.D ./ 15
	thresh_cable = 2 * (dt.D / 24) 				# 30 min / 1 month = 2 blocks / month (24 days)
	thresh_network = dt.D / 6 			        # 15 min / wk = 1 block / 6 days

	cross_ind = 0
	for c in 1:dt.C
		thresh_one = dt.network[c] ? thresh_network : thresh_cable
		view_thresh[c] = sum(consumer_view_totals[:,c] .>= thresh_one) / dt.N
		if (c < dt.C) 
			for c2 in (c+1):dt.C
				thresh_two = dt.network[c2] ? thresh_network : thresh_cable
				cross_ind += 1
				view_thresh_cross[cross_ind] = sum((consumer_view_totals[:,c] .>= thresh_one) .& (consumer_view_totals[:,c2] .>= thresh_two)) / dt.N
			end
		end
	end


	
end
