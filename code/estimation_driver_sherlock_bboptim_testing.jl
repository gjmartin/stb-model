# call to Distributed not needed when julia started with -p
using Distributed
# launch worker processes
# num_cores = parse(Int, ENV["SLURM_CPUS_PER_TASK"])
# num_cores = 30
# addprocs(num_cores)

### BEGIN OPTIONS ###
@everywhere begin
    ## Set your local directory
    # local_dir = "/home/gjmartin/Dropbox/STBNews"
    # local_dir = "/Users/mitchelllinegar/Code"
    # local_dir = "/home/users/mlinegar/stb"
    if(occursin(r"mitchelllinegar", pwd()))
        local_dir = "/Users/mitchelllinegar/Code"
    else
        local_dir = "/home/users/mlinegar/stb"
    end
    # stb_dir = "stb-model-event"
    stb_dir = "stb-model"
    # set to something nonempty to limit dates used in estimation
    dates_to_keep = []  

    # to sample only beta
    # tree_base = "beta"

    # # to sample all
    tree_base = ""

    ### END OPTIONS ###
    
    # LOAD BASE PACKAGES
    # some packages need
    using Printf
    using BlackBoxOptim
    using Distributions
end
# this should write to the log file
# @printf("Number of threads used: %s", Threads.nthreads() )

# note: have to have `using` in a different block than anywhere it will be called
# since the block is compiled before it is evaluated
## LOAD OBJECTIVE, DATA, PARAMETERS ##
@everywhere begin
       
    ## Directory locations for code and data
    code_dir = @sprintf("%s/%s/code", local_dir, stb_dir)
    data_dir = @sprintf("%s/%s/data", local_dir, stb_dir)
    output_dir = @sprintf("%s/%s/output", local_dir, stb_dir)
    sampling_dir = @sprintf("%s/%s/sampling", local_dir, stb_dir)

    include("$code_dir/load_model_data.jl")
    include("$code_dir/helper_functions.jl")
end

## SET UP OBJECTIVE FUNCTION ##
@everywhere begin    
    # to read direct from csv:
    cd(data_dir)
    
    ## IF READING IN FROM CSV, DO THIS ##
    # if reading in initial parameters
    # par_init = CSV.read("parameter_init.csv", DataFrame);
    # if reading in current best parameters:
    par_init = CSV.read("$output_dir/best_pars.csv", DataFrame);
    # shorthand 
    pars = par_init.parameter
    # if reading in latest parameters from optimization
    # par_init = CSV.read("bboptim_progress.csv", DataFrame)
    # par_init = stack(DataFrame(last(par_init)))
    # par_init = CSV.read("bboptim_best.csv", DataFrame)
    rename!(par_init, [:parameter, :value])

    # overwrite old parameters with outside_pars
    # outside_pars = CSV.read("$output_dir/calibrated_betas_v3.csv", DataFrame)
    # for r in eachrow(outside_pars)
    #     par_init[pars.==r.parameter,:value] .= r.value
    #     # print(r.value)
    # end

    ## IF READING IN FROM CSV, DO THIS ##
    # read in bounds
    par_bounds = CSV.read("parameter_bounds.csv", DataFrame);
    par_init = innerjoin(par_init, par_bounds, on = :parameter)
    # read in shocks
    par_change = CSV.read("$output_dir/par_change.csv", DataFrame)
    # rename!(par_change, Dict(:value => "parameter"))
    par_init = innerjoin(par_init, par_change, on = :parameter)
    # # FIXME: REMOVE
    # p0 = par_init[occursin.(r"beta", pars), :parameter]
    # p1 = par_init[occursin.(r"gamma", pars), :parameter]
    # p2 = par_init[occursin.(r"rho0", pars), :parameter]
    # to_opt = [p0; p1; p2]

    # # dictionary indexed by parameter, with initial values and bounds for each
    # ## IF READING IN FROM CSV, DO THIS ##
    # # FIXME: TEMPORARY -- revert to old best obj value
    # y = [0.258461, 0.063983, -5.0e-5, 0.000842466, -3.96736, -0.634277, 3.5021, -3.46049, -4.99999, -1.44654, 3.1095, -4.55479, -5.0, -0.514227, 3.7133, -0.487382, 0.0163896, -0.0226017, 0.0, -1.99631, 0.0, -2.22998, 0.0, 0.0, 0.0230052, -0.564203, -3.98118, 1.27816, -0.39887, 1.94081, -1.94829, -5.0, 0.0, 0.0, 0.142754, -2.80344, 1.40283, -0.4945, -0.103377, 0.0, -2.11756, -1.41162, 0.643517, -1.68844, 0.0, 0.0, 0.0361411, 0.0, 1.93812, 4.99969, 0.0, 0.340594, 2.1151, 3.23396, 0.257578, -0.073997, -1.08743, 0.0148685, -1.33353, 1.91133, -1.97706, -4.99994, 0.427133, 0.385458, 0.0235389, -0.438828, 0.254353, -2.83947, 1.06216, -0.00308688, -2.34835, -1.33965, -4.99986, -1.14531, -0.931756, -0.651126, -0.148321, 0.11975, 0.234829, -2.255, -1.21859, 1.06376, 1.44718, 0.535615, 0.00358705, 0.188646, 0.993146, -1.356, -0.912276, -0.711166, -0.47787, -3.74255, 0.586607, 0.0, 0.0, 0.0526982, 0.191562]
    # temp = DataFrame(parameter = to_opt, value = y)
    # # par_init2 = deepcopy(par_init)
    # for r in eachrow(temp)
    #     par_init[pars.==r.parameter,:value] .= r.value
    #     # print(r.value)
    # end
    # FIXME: END OF TEMPORARY CHANGES

    pb = DataStructures.OrderedDict(zip(
        pars,
        DataFrames.eachrow(par_init[:, [:value, :lb, :ub, :step_size]])
    ));

    # eliminate any learning
    # pb["gamma:r"].value = 0
    pb["tau"].value = 1
    # par_init[pars.=="gamma:r", :value] .= 0
    # par_init[pars.=="tau", :value] .= 1

    # add pars to mprob object
    SMM.addSampledParam!(mprob,pb);
    ev = SMM.Eval(mprob)

    # test loading in by running objective function. Current value should be ~ 1207 (July 5, 2022)
    # obj0 = news_obj(ev; dt=stbdat, store_moments=true)
    # obj_val0 = sum([(obj0.simMoments[k] - SMM.dataMomentd(obj0)[k])^2 * SMM.dataMomentWd(obj0)[k] for k in Symbol.(viewership_names.name)].^2)
    # 2071

    # NOTE: this is the setup from calibrate.jl, but this seems incorrect
    # pb["gamma:r"].value = 0
    # pb["tau"].value = 1
    # par_init[pars.=="gamma:r", :value] .= 0
    # par_init[pars.=="tau", :value] .= 1

    # to_optimize = String.(pars)
    # ub = par_init.ub
    # lb = par_init.lb

    # ## include initial parameter vector in the population
    # x0 = [pb[k].value for k in to_optimize]

    # optimize over a subset of the betas 
    # p0 = ["beta:const", "beta:zero", "gamma:r", "tau"]
    # p0 = ["beta:const", "beta:zero"]
    # p1 = par_init[occursin.(r"channel", pars), :parameter]
    # p2 = par_init[occursin.(r"base_slant", pars), :parameter]
    # p3 = par_init[occursin.(r"time", pars), :parameter]
    # p4 = par_init[occursin.(r"beta:topic", pars), :parameter]
    # p5 = ["beta:slant", "beta:info", "beta:mu"]
    # p4 = ["tau", "gamma:r"]
    # to_opt = [p0; p1; p2; p3; p4; p5]
    
    p0 = par_init[occursin.(r"beta", pars), :parameter]
    p1 = par_init[occursin.(r"gamma", pars), :parameter]
    p2 = par_init[occursin.(r"rho0", pars), :parameter]
    p3 = par_init[occursin.(r"alpha", pars), :parameter]
    to_opt = [p0; p1; p2; p3]

    # which_topics = ["18","27","35","39","41","45","64","65","69","73","74"]
    # topic_pars = par_init[occursin.(r"topic", pars), :parameter]
    # topic_numbers = replace.(topic_pars, "beta:topic:" => "")
    # p_topics = topic_pars[topic_numbers .∈ Ref(which_topics)]
    # to_opt = [p_topics;]

    #
    x0 = [pb[k].value for k in to_opt]
    # new_variables  = ["gamma:r", "tau"]
    # to_opt = [to_opt; new_variables]
    # reorder to_opt
    # note: have to get the order right! If these variables aren't in the correct order, things go bad
    # define function for filtering variables in DataFrames
    # pars_to_opt(parameter) = parameter in to_opt
    # to_opt = filter(:parameter => pars_to_opt, par_init).parameter

    # to_opt = par_init[!,:parameter]
    # if not reading in old version
    # x0 = [pb[k].value for k in to_opt]
    # if reading in calibrated parameters from Greg
    # x0 = CSV.read("$output_dir/calibrated_betas_v2.csv", DataFrame).value
    
    # x0 and to_opt may not be the same length. In this case, read in x0 and bind to to_opt

    # check: I have verified this produces exactly the same as just reading in x0 as above
    # when calibrated pars and pars to opt are the same
    # outside_pars = CSV.read("$output_dir/calibrated_betas_v1.csv", DataFrame)
    # # outside_pars[outside_pars.parameter.=="tau",:]
    # # FIXME: the order of to_opt and outside_pars is different!!!
    # # for i in 1:length(to_opt) print(outside_pars.parameter[i]==to_opt[i]) end
    # # define function for filtering variables in DataFrames
    # calibrated_pars(parameter) = parameter in outside_pars.parameter
    # # make indices of variables
    # # FIXME: sloppy, do better when have internet
    # par_init[!,:row] .= 1:1:size(par_init,1)

    # # get indices of parameters in calibrated parameters
    # calibrated_par_inds = filter(:parameter => calibrated_pars, par_init).row
    # # get indices of parameters in pars_to_opt
    # pars_to_opt_inds = filter(:parameter => pars_to_opt, par_init).row

    # # overwrite par_init with outside, calibrated model parameters
    # par_init[calibrated_par_inds,:value] .= outside_pars.value
    # # create new x0 -- these are the starting values for the parameters we want to optimize over
    # x0 = par_init[pars_to_opt_inds,:value]

    # need to overwrite pb with new values taken from the parameters from the server
    # for i in 1:length(outside_pars.parameter)
    #     pb[outside_pars.parameter[i]].value = outside_pars[outside_pars.parameter.==outside_pars.parameter[i],:].value[1]
    # end

    bounds = [(pb[k].lb, pb[k].ub) for k in to_opt]
    lb = [pb[k].lb for k in to_opt]
    ub = [pb[k].ub for k in to_opt]
    steps = [pb[k].step_size for k in to_opt]
    # shocks = [Random.randn(rng, length(to_opt)) for i in 1:49];
    shocks = [[rand(Normal(0.0, steps[i])) for i in 1:length(steps)] for j in 1:49]

    x0_pop = [x0 .+ s for s in shocks]
    for i in 1:length(shocks)
        for j in 1:length(to_opt)
            x0_pop[i][j] = max(min(x0_pop[i][j], bounds[j][2]), bounds[j][1])
        end
    end

    push!(x0_pop, x0)

    x0_pop_mat = zeros(length(to_opt), size(x0_pop,1))
    for i in 1:size(x0_pop,1)
        for j in 1:length(to_opt)
            x0_pop_mat[j,i] = max(min(x0_pop[i][j], bounds[j][2]), bounds[j][1])
        end
    end

    cd(output_dir)

    ## the function to optimize
    function f0(x)
        # reading in has to be a bit more clever here when working with subsets of parameters that may grow or shrink
        # it's not enough to just read in pb_val here like one of these as did previously: 
        # note here we're wrapping the param vector in an OrderedDict
        # pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? x[findfirst(k .== to_optimize)] : pb[k].value for k in keys(pb))
        # pb_val = DataStructures.OrderedDict(k => k ∈ to_optimize ? x[findfirst(k .== to_optimize)] : pb[k].value for k in keys(pb))
        # (honestly not quite sure how the objective function actually ran with that)
        # we're already only optimizing over x0 in the optimizer
        # here, need to use the full list of parameters

        # pb_temp = deepcopy(pb)
        # for i in 1:length(pars_to_opt_inds)
        #     # this is where having the indices be in order is important
        #     pb_temp[pars[pars_to_opt_inds[i]]].value = x[i]
        # end
        # need to "paste" x onto pb for only the variables that are actually being optimized

        # wrap in Eval object
        # 2 versions: 
        # 1) run with Eval(mprob, pars)
        # pb_val = DataStructures.OrderedDict(k => k ∈ to_opt ? x[findfirst(k .== to_opt)] : pb_temp[k].value for k in keys(pb_temp))
        pb_val = DataStructures.OrderedDict(k => k ∈ to_opt ? x[findfirst(k .== to_opt)] : pb[k].value for k in keys(pb))
        ev = SMM.Eval(mprob, pb_val)     
        # 2) run with Eval(mprob) (so add parameters to mprob first)
        # SMM.addSampledParam!(mprob,pb_temp);
        # ev = SMM.Eval(mprob)    
        obj = news_obj(ev; dt=stbdat, store_moments = true)
        # calculate objective on all parameters
        # obj_val = obj.value
        # only calculate objective on subset of moments
        # obj_val = sum([(obj.simMoments[k] - SMM.dataMomentd(obj)[k])^2 * SMM.dataMomentWd(obj)[k] for k in Symbol.(viewership_names.name)])
        obj_val = sum([(obj.simMoments[k] - SMM.dataMomentd(obj)[k])^2 * SMM.dataMomentWd(obj)[k] for k in Symbol.(moms.name[.!(moms.name.=="reporting_likelihood")])])
        # save output each step
        # outrow = Dict(Symbol(to_optimize[i]) => x[i] for i = 1:length(to_optimize))
        outrow = Dict(Symbol(to_opt[i]) => x[i] for i = 1:length(to_opt))
        outrow[Symbol("n_func_evals")] = 0
        outrow[Symbol("fval")] = obj_val

        output_df = DataFrames.DataFrame(;outrow...)
        CSV.write("bboptim_progress.csv", output_df; append=true)
        CSV.write("parameter_tracker.csv", output_df; append=true)
        # println(obj_val)
        obj_val
    end
    # f0(x0)
    ## initial population: best previously found value plus other randomly generated ones
    # init_pop = [x0 BlackBoxOptim.rand_individuals(BlackBoxOptim.ContinuousRectSearchSpace(lb, ub),49;method = :latin_hypercube)]
    # init_pop = BlackBoxOptim.rand_individuals(BlackBoxOptim.ContinuousRectSearchSpace(lb, ub),49;method = :latin_hypercube)
end


## setup for interim output
cd(data_dir)

function callback(oc)
    # save output each step
    x1 = BlackBoxOptim.best_candidate(oc)

    # outrow = Dict(Symbol(to_optimize[i]) => x1[i] for i = 1:length(to_optimize))
    outrow = Dict(Symbol(to_opt[i]) => x1[i] for i = 1:length(to_opt))
    outrow[Symbol("n_func_evals")] = BlackBoxOptim.num_func_evals(oc)
    outrow[Symbol("fval")] = BlackBoxOptim.best_fitness(oc)

    output_df = DataFrames.DataFrame(;outrow...)
    CSV.write("bboptim_progress.csv", output_df; append=true)
end

# opt_setup = BlackBoxOptim.bbsetup(f0;
#     SearchSpace = BlackBoxOptim.ContinuousRectSearchSpace(lb, ub),
#     Population = init_pop,
#     MaxFuncEvals = 50,
#     TraceInterval = 60.0,
#     TraceMode = :silent,
#     Workers = workers(),
#     CallbackFunction = callback,
#     CallbackInterval = 1.0)

# optimized = BlackBoxOptim.run!(opt_setup)

# println(BlackBoxOptim.best_candidate(optimized))
# println(BlackBoxOptim.best_fitness(optimized))

# best_params = par_init[!, [:parameter, :value]]
# best_params[!, :value] .= BlackBoxOptim.best_candidate(optimized)
# CSV.write("bboptim_best.csv", best_params)
# best_par = [0.0, 0.0709868, -2.64685, 0.507514, 0.418003, 0.0, 0.0, -1.52595, 0.0, -5.0e-5, 0.0, 0.0, 0.1, -0.200094, 3.08641, 0.0, 0.0, 0.0, -1.31795, 0.0, -4.17436, 0.0, 2.80886, 0.99, -0.80107, 0.0, 0.0, 0.242786, 0.130007, -0.148321, 0.0, 0.114776, 0.0, -0.746419, 0.0, 0.0, -1.50908, -0.572557, 0.0288473, -0.162935, 0.237782, 0.0, -0.0665339, -0.65157, -4.17613, -4.17161, 0.0, 0.0, -0.792365, -0.0442513, 0.01, -0.219395, 0.0, 0.0, -0.259671, 2.89902, -1.35429, 0.0, -1.0355, -4.06397, 0.107638, 2.715, 0.0, 0.0, 0.001, -0.271278, 0.721829, -2.77373, 0.0, 0.0, -0.103107, -0.705801, 0.0, -0.599709, 0.0, 0.0, 0.0, 3.04231, -4.06634, 0.0, 0.0, 0.26, 0.0, -1.21859, 0.0, -0.0349428, 0.0, 0.0, 0.0282144, 0.0, 0.0, -4.03158, 0.0, 0.0, 0.112204, 0.0, 0.0, 0.0, -4.48695, 0.25, 0.0, 0.0, 0.0407101, 0.0, 0.0, 0.234829, 0.0, 0.467379, 0.0163896, 0.0, 2.7989, 0.0, 0.0, 0.110486, 0.0, 0.0]


# outrow = Dict(Symbol(to_optimize[i]) => x0[i] for i = 1:length(to_optimize))
# NOTE: writing to data_dir because output_dir seems to be reserved (interferes with load_model_data.jl)

println("last obj value")
last_obj_val = f0(x0)
println(last_obj_val)
println("saving initital parameter_tracker and bboptim_progress")

outrow = Dict(Symbol(to_opt[i]) => x0[i] for i = 1:length(to_opt))
outrow[Symbol("n_func_evals")] = 0
# outrow[Symbol("fval")] = f0(x0)
outrow[Symbol("fval")] = last_obj_val
output_df = DataFrames.DataFrame(;outrow...)

CSV.write("bboptim_progress.csv", output_df)
CSV.write("parameter_tracker.csv", output_df)

println("start of new obj values")
# optim = bboptimize(f0, best_par;
# optim = bboptimize(f0, x0;
optim = bboptimize(f0, x0_pop;
    # Method = :adaptive_de_rand_1_bin_radiuslimited,
    Method = :generating_set_search,
    # Method = :dxnes,
    SearchSpace = BlackBoxOptim.ContinuousRectSearchSpace(lb, ub), 
    SearchRange = bounds, 
    # Population = x0_pop,
    MaxFuncEvals = 100,
    TraceInterval = 60.0,
    # TraceMode = :silent,
    TraceMode = :verbose,
    Workers = workers()
    # CallbackFunction = callback,
    # CallbackInterval = 1.0
)
println("best obj value: ")
println(BlackBoxOptim.best_fitness(optim))
# println(BlackBoxOptim.best_candidate(optim))

x1 = best_candidate(optim)

if (BlackBoxOptim.best_fitness(optim) < last_obj_val)
    println(@sprintf("Objective function value improvemed from %s to %s, saving latest parameters to calibrated_betas_v3.csv and best_pars.csv", last_obj_val, BlackBoxOptim.best_fitness(optim)))
    out = DataFrame("parameter" => to_opt, "value" => x1)

    out |> CSV.write("$output_dir/calibrated_betas_v3.csv")
    
    # best_pars = CSV.read("$output_dir/calibrated_betas_v3.csv", DataFrame)
    best_pars = deepcopy(par_init)
    # for r in eachrow(out)
    for r in eachrow(out)
        best_pars[pars.==r.parameter,:value] .= r.value
        # print(r.value)
    end
    best_pars = best_pars[:,[:parameter, :value]]
    best_pars |> CSV.write("$output_dir/best_pars.csv")
    println("Done saving latest parameters")    
else 
    println("Objective function value did not improve, not saving new parameter values")
end

