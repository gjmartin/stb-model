## Directory locations for code and data
using Dates
using Surrogates
using SurrogatesPolyChaos
using FileIO

import CSV
import Random
import DataFrames

local_dir = "/home/greg"
stb_dir = "stb-model"

code_dir = "$local_dir/$stb_dir/code"
data_dir = "$local_dir/$stb_dir/data"
output_dir = "$local_dir/$stb_dir/output"
sampling_dir = "$local_dir/$stb_dir/sampling"

# size of problem
x0_size = 100

## truth
rng = Random.MersenneTwister(14592150)

init_pop = Random.randn(rng, Float64, (x0_size, 30))

x0 = Random.randn(rng, Float64, x0_size) 
W = Random.randn(rng, Float64, (x0_size, x0_size))
W0 = W' * W


# outrow = Dict(Symbol("x" * string(i)) => x0[i] for i = 1:x0_size)
# outrow[Symbol("fval")] = 0
# output_df = DataFrames.DataFrame(;outrow...)
# CSV.write("$output_dir/quadratic_test_parameter_tracker.csv", output_df)

function f0(x)
    x = collect(x)
    # outrow = Dict(Symbol("x" * string(i)) => x[i] for i = 1:x0_size)
    fval = (x - x0)' * W0 * (x - x0)
    # outrow[Symbol("fval")] = fval
    # output_df = DataFrames.DataFrame(;outrow...)
    # CSV.write("$output_dir/quadratic_test_parameter_tracker.csv", output_df; append=true)
    fval
end




### SURROGATE OPTIMIZATION
num_samples = 500
lb = repeat([-10.0], x0_size)
ub = repeat([10.0], x0_size)

#Sampling
x = sample(num_samples,lb,ub,SobolSample())
y = f0.(x)


# Creating surrogate

poly = SecondOrderPolynomialSurrogate(
  x,
  y,
  lb,
  ub)


surrogate_optimize(f0, SRBF(), lb, ub, poly, SobolSample(), maxiters = 100)


#### BEGIN OPTIMIZATION ####
# opt_params = Dict(:SearchSpace => BlackBoxOptim.ContinuousRectSearchSpace(lb, ub),
#                   :Population => init_pop,
#                   :MaxFuncEvals => 5000,
#                   :TraceInterval => 3600.0,
#                   :TraceMode => :verbose,
#                 #   :Workers => workers(),
#                   :PM_η => 250.0
#                   )

# opt_problem, opt_params = BlackBoxOptim.setup_problem(f0, chain(BlackBoxOptim.DefaultParameters, opt_params))


# optimizer = BlackBoxOptim.adaptive_de_rand_1_bin_radiuslimited(opt_problem, opt_params)

# opt_ctrl = BlackBoxOptim.OptController(optimizer, opt_problem, opt_params)
# BlackBoxOptim.run!(opt_ctrl)

# best_x = BlackBoxOptim.best_candidate(BlackBoxOptim.lastrun(opt_ctrl))
# result = DataFrames.DataFrame(parameter = to_optimize, value = best_x)
# result = DataFrames.DataFrame(parameter = all_pars, value = best_x)


# get current date in right format
using Dates
current_date = lpad(month(today()),2,"0") * lpad(day(today()),2,"0") * last(string(year(today())),2)
result |> CSV.write("$output_dir/bboptim_best_$current_date.csv")
println("Best fval was $(BlackBoxOptim.best_fitness(BlackBoxOptim.lastrun(opt_ctrl)))")
