## regress polls on aggregated topic-partisanship

library(data.table)
library(tidyverse)
library(lubridate)
library(fixest)
library(broom)
library(ggthemes)
library(here)

here::i_am("code/reduced_form_analogues/01_polling_regressions.R")

theme_set(theme_hc())



## read block-level partisanship
block_p <- fread(here("data/block_partisanship.csv")) %>%
    .[timezone == "ETZ"]  # use eastern feed

## now aggregate partisanship
partisanship_daily <- block_p %>%
    .[, .(partisanship = mean(partisanship, na.rm = T),
          event_partisanship = mean(event_partisanship, na.rm = T)),
                              by = .(date)] %>%
    .[order(date)]
                        
partisanship_daily[, partisan_roll_avg := frollmean(partisanship, n = 7)]
partisanship_daily[, event_partisan_roll_avg := frollmean(event_partisanship, n = 7)]

polls <- fread(here("data/polling.csv")) %>%
    .[, poll_roll_avg := frollmean(poll, n = 7), by = .(state)]

## regress polling on partisanship
polls_topics <- polls[partisanship_daily, on = .(date)]
polls_topics <- polls_topics[order(state, date)]

polls_topics[, lag7_p := shift(partisan_roll_avg, 7)]
polls_topics[, lag7_ep := shift(event_partisan_roll_avg, 7)]

pollmodel1 <- feols(poll_roll_avg ~ lag7_p | state, data = polls_topics[date <= mdy("11-08-2016")])
pollmodel2 <- feols(poll_roll_avg ~ lag7_ep | state, data = polls_topics[date <= mdy("11-08-2016")])

setFixest_etable(digits = 3,
                fitstat = ~ n + r2)

setFixest_dict(c(lag7_p = "Weighted Partisanship (1-week lag)",
                 lag7_ep = "Weighted Partisanship (1-week lag), Events Only",
                 poll_roll_avg = "1-Week Rolling Average Clinton Poll",
                 state = "State"))

tablestyle <- style.tex(main = "aer",
                 fixef.suffix = " FEs",
                 fixef.where = "var",
                 fixef.title = "\\midrule",
                 stats.title = "\\midrule",
                 tablefoot = F,
                 yesNo = c("Yes", "No"))

etable(list(pollmodel1, pollmodel2),
    cluster = ~ date,
    style.tex = tablestyle,
    file = here("output/polling_regression.tex"),
    replace = T)