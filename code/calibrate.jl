## Set your local directory
local_dir = "/home/gjmartin/"
# local_dir = "/Users/mitchelllinegar/Code"
# stb_dir = "stb-model-event"
stb_dir = "stbmodel"
# set to something nonempty to limit dates used in estimation
dates_to_keep = []  


# to sample only beta
# tree_base = "beta"

# # to sample all
tree_base = ""

### END OPTIONS ###

## Directory locations for code and data
using Printf
using JLD2
using FileIO
using Plots
using BlackBoxOptim
code_dir = @sprintf("%s/%s/code", local_dir, stb_dir)
data_dir = @sprintf("%s/%s/data", local_dir, stb_dir)
output_dir = @sprintf("%s/%s/output", local_dir, stb_dir)
sampling_dir = @sprintf("%s/%s/sampling", local_dir, stb_dir)

## LOAD OBJECTIVE, DATA, PARAMETERS ##
include("$code_dir/load_model_data.jl")
include("$code_dir/helper_functions.jl")

# add pars to mprob object
SMM.addSampledParam!(mprob,pb);

ev = SMM.Eval(mprob)

# eliminate any learning
pb["gamma:r"].value = 0
pb["tau"].value = 1

# optimize over a subset of the betas 
p0 = ["beta:const", "beta:zero"]
p1 = par_init[occursin.(r"channel", par_init.parameter), :parameter]
p2 = par_init[occursin.(r"base_slant", par_init.parameter), :parameter]
p3 = par_init[occursin.(r"time", par_init.parameter), :parameter]

to_opt = [p0; p1; p2; p3]
x0 = [pb[k].value for k in to_opt]
bounds = [(pb[k].lb, pb[k].ub) for k in to_opt]


shocks = [Random.randn(rng, length(to_opt)) for i in 1:49];

x0_pop = [x0 .+ s for s in shocks]

for i in 1:length(bounds)
    for j in 1:length(to_opt)
        x0_pop[i][j] = max(min(x0_pop[i][j], bounds[j][2]), bounds[j][1])
    end
end

push!(x0_pop, x0)


# optimize over viewing moments only
function f_opt(x)
    pb_val = DataStructures.OrderedDict(k => k ∈ to_opt ? x[findfirst(k .== to_opt)] : pb[k].value for k in keys(pb))
    # wrap in Eval object
    ev = SMM.Eval(mprob, pb_val)
    ev = news_obj(ev; dt=stbdat, store_moments = true)
    sum([(ev.simMoments[k] - SMM.dataMomentd(ev)[k])^2 * SMM.dataMomentWd(ev)[k] for k in Symbol.(viewership_names.name)].^2)
end

# res = bboptimize(f_opt, x0_pop; SearchRange = bounds, MaxFuncEvals = 500, TraceInterval = 600)

res = bboptimize(f_opt, x0; Method = :generating_set_search, SearchRange = bounds, MaxFuncEvals = 500, TraceInterval = 600)


x1 = best_candidate(res)

out = DataFrame("parameter" => to_opt, "value" => x1)

out |> CSV.write("$output_dir/calibrated_betas_v1.csv")


@time news_obj(ev; dt=stbdat)
ev = news_obj(ev; dt=stbdat, save_output=false, store_moments=true);
ev.simMoments[:reporting_likelihood]
# x0 = [pb[k].value for k in to_optimize]

plot(get_sim_ratings(ev3, "MSNBC", 1))
plot(get_sim_ratings(ev, "FNC", 3))

plot(get_sim_polls(ev4, "TX") - get_sim_polls(ev, "TX"))




function influence(ev)
    infl = Dict(k => (ev.simMoments[k] - SMM.dataMomentd(ev)[k])^2 * SMM.dataMomentWd(ev)[k] for k in keys(ev.simMoments))
    sort(collect(infl), by = x -> x[2])
end

function influence2(ev1, ev2)
    infl = Dict(k => (ev1.simMoments[k] - SMM.dataMomentd(ev1)[k])^2 * SMM.dataMomentWd(ev1)[k] - (ev2.simMoments[k] - SMM.dataMomentd(ev2)[k])^2 * SMM.dataMomentWd(ev2)[k] for k in keys(ev1.simMoments))
    sort(collect(infl), by = x -> x[2])
end


mitchell = CSV.File(string(data_dir, "/bboptim_best.csv")) |> DataFrame

## RUN OBJECTIVE FUNCTION AGAINST MANY VALUES OF A GIVEN PARAMETER

function f0(x, var; store_moments=false)
    # have to coerce single input to var to vector
    typeof(var)==String ? var = [var] : var = var
    # wrap param vector in OrderedDict
    pb_val = DataStructures.OrderedDict(k => k ∈ var ? x[findfirst(k .== var)] : pb[k].value for k in keys(pb))
    # wrap in Eval object
    ev = SMM.Eval(mprob, pb_val)
    news = news_obj(ev; dt=stbdat, store_moments=store_moments)
    obj = news[1].value
    # moments = SMM.check_moments(news)
    moments = news[2]
    cumulative_view = news[3]
    view_by_tercile = news[4]
    daily_polling = news[5]
    viewer_slant_util = news[6]
    viewer_info_util = news[7]
    if store_moments
        # return obj, moments, cumulative_view, view_by_tercile, daily_polling
        return obj, moments, cumulative_view, view_by_tercile, daily_polling, viewer_slant_util, viewer_info_util
    else
        return obj
    end
end

# param_multipliers = [0.0:0.1:1.5; 1.6:0.1:10;]
# param_multipliers = [0.2; 2.0]
# param_multipliers = [4.8; 5.2]
param_multipliers = [4.9; 4.95; 5.0; 5.05; 5.1;]

# initialize data storage
info_obj_storage = zeros(Float64, length(param_multipliers))
slant_obj_storage = zeros(Float64, length(param_multipliers))
# moments = DataFrame(moment=Symbol[], data=Float64[], data_sd=Float64[], simulation=Float64[], distance=Float64[], abs_percent=Float64[], abs_percent_2=Float64[], abs_percent_1000=Float64[], abs_percent_2_1000=Float64[], MSE_SD=Float64[], abs_percent_SD_weighted=Float64[], MSE_SD_1000=Float64[], abs_percent_SD_weighted_1000=Float64[], opt_var=String[], opt_mult=Float64[])
moments = DataFrame(moment = String[], value = Float64[], opt_var = String[], opt_mult = Float64[])
info_moments = copy(moments)
slant_moments = copy(moments)
info_cumulative_view_storage = zeros(6000, 6, length(param_multipliers))# 6000x6
info_view_by_tercile_storage = zeros(6, 3, 4896, length(param_multipliers))#6x3x4896
info_daily_polling_storage = zeros(49, 204, length(param_multipliers))#49x204
slant_cumulative_view_storage = zeros(6000, 6, length(param_multipliers))# 6000x6
slant_view_by_tercile_storage = zeros(6, 3, 4896, length(param_multipliers))#6x3x4896
slant_daily_polling_storage = zeros(49, 204, length(param_multipliers))#49x204
info_viewer_slant_util = zeros(6000, 6, length(param_multipliers))
info_viewer_info_util = zeros(6000, 6, length(param_multipliers))
slant_viewer_slant_util = zeros(6000, 6, length(param_multipliers))
slant_viewer_info_util = zeros(6000, 6, length(param_multipliers))
for i in 1:length(param_multipliers)
    @printf "Testing objective for param multiplier %s of %s \n" i length(param_multipliers)
    info_obj = f0(param_multipliers[i], "beta:info", store_moments=true)
    slant_obj = f0(param_multipliers[i], "beta:slant", store_moments=true)
    info_obj_storage[i] = info_obj[1]
    slant_obj_storage[i] = slant_obj[1]
    info_moment = info_obj[2]
    slant_moment = slant_obj[2]
    #
    info_cumulative_view_storage[:,:,i] = info_obj[3]
    info_view_by_tercile_storage[:,:,:,i] = info_obj[4]
    info_daily_polling_storage[:,:,i] = info_obj[5]
    #
    slant_cumulative_view_storage[:,:,i] = slant_obj[3]
    slant_view_by_tercile_storage[:,:,:,i] = slant_obj[4]
    slant_daily_polling_storage[:,:,i] = slant_obj[5]
    #
    info_viewer_slant_util[:,:,i] = info_obj[6]
    info_viewer_info_util[:,:,i] = info_obj[7]
    #
    slant_viewer_slant_util[:,:,i] = slant_obj[6]
    slant_viewer_info_util[:,:,i] = slant_obj[7]
    
    info_moment[!,:opt_var] .= "beta:info"
    info_moment[!,:opt_mult] .= param_multipliers[i]
    slant_moment[!,:opt_var] .= "beta:slant"
    slant_moment[!,:opt_mult] .= param_multipliers[i]
    append!(info_moments, info_moment)
    append!(slant_moments, slant_moment)
end
# TEMP: fix opt_mult
# info_moments.opt_mult = repeat(param_multipliers, inner=98146)
# slant_moments.opt_mult = repeat(param_multipliers, inner=98146)
# slant_moments.opt_var = "beta:slant"

# export to CSV
using CSV
CSV.write("$data_dir/moments_over_info.csv", info_moments)
CSV.write("$data_dir/moments_over_slant.csv", slant_moments)
CSV.write("$data_dir/obj_over_info.csv", DataFrame(param_mult = param_multipliers, obj = info_obj_storage))
CSV.write("$data_dir/obj_over_slant.csv", DataFrame(param_mult = param_multipliers, obj = slant_obj_storage))
using FileIO
save("$data_dir/slant_cumulative_view_storage.jld2", "slant_cumulative_view_storage", slant_cumulative_view_storage)
save("$data_dir/slant_view_by_tercile_storage.jld2", "slant_view_by_tercile_storage", slant_view_by_tercile_storage)
save("$data_dir/slant_daily_polling_storage.jld2", "slant_daily_polling_storage", slant_daily_polling_storage)

save("$data_dir/info_cumulative_view_storage.jld2", "info_cumulative_view_storage", info_cumulative_view_storage)
save("$data_dir/info_view_by_tercile_storage.jld2", "info_view_by_tercile_storage", info_view_by_tercile_storage)
save("$data_dir/info_daily_polling_storage.jld2", "info_daily_polling_storage", info_daily_polling_storage)

slant_tercile_cumulative_view_storage = zeros(6,3, length(param_multipliers))
info_tercile_cumulative_view_storage = zeros(6,3, length(param_multipliers))
for par in 1:length(param_multipliers)
    # mul!(slant_tercile_cumulative_view_storage[:,:,1], slant_cumulative_view_storage[:,:,1]' , stbdat.dma_aggregator');
    slant_tercile_cumulative_view_storage[:,:,par] = slant_cumulative_view_storage[:,:,par]' * stbdat.dma_aggregator'
    info_tercile_cumulative_view_storage[:,:,par] = info_cumulative_view_storage[:,:,par]' * stbdat.dma_aggregator'
end
# reshape so can save as csv
# dims: tercile channel * param (row), tercile (cols)

slant_tercile_cumulative_view_storage_long = reshape(permutedims(slant_tercile_cumulative_view_storage, [1,3,2]), :,3)
info_tercile_cumulative_view_storage_long = reshape(permutedims(info_tercile_cumulative_view_storage, [1,3,2]), :,3)
CSV.write("$data_dir/slant_tercile_cumulative_view_storage_long.csv", Tables.table(slant_tercile_cumulative_view_storage_long))
CSV.write("$data_dir/info_tercile_cumulative_view_storage_long.csv", Tables.table(info_tercile_cumulative_view_storage_long))


# save utilities 
slant_tercile_cumulative_slant_util_storage = zeros(6,3, length(param_multipliers))
slant_tercile_cumulative_info_util_storage = zeros(6,3, length(param_multipliers))
info_tercile_cumulative_slant_util_storage = zeros(6,3, length(param_multipliers))
info_tercile_cumulative_info_util_storage = zeros(6,3, length(param_multipliers))
for par in 1:length(param_multipliers)
    # mul!(slant_tercile_cumulative_view_storage[:,:,1], slant_cumulative_view_storage[:,:,1]' , stbdat.dma_aggregator');
    slant_tercile_cumulative_slant_util_storage[:,:,par] = slant_viewer_slant_util[:,:,par]' * stbdat.dma_aggregator'
    slant_tercile_cumulative_info_util_storage[:,:,par] = slant_viewer_info_util[:,:,par]' * stbdat.dma_aggregator'

    info_tercile_cumulative_slant_util_storage[:,:,par] = info_viewer_slant_util[:,:,par]' * stbdat.dma_aggregator'
    info_tercile_cumulative_info_util_storage[:,:,par] = info_viewer_info_util[:,:,par]' * stbdat.dma_aggregator'
end

slant_viewer_slant_util_long = reshape(permutedims(slant_tercile_cumulative_slant_util_storage, [1,3,2]), :,3)
CSV.write("$data_dir/slant_viewer_slant_util_long.csv", Tables.table(slant_viewer_slant_util_long))
slant_viewer_info_util_long = reshape(permutedims(slant_tercile_cumulative_info_util_storage, [1,3,2]), :,3)
CSV.write("$data_dir/slant_viewer_info_util_long.csv", Tables.table(slant_viewer_info_util_long))

info_viewer_slant_util_long = reshape(permutedims(info_tercile_cumulative_slant_util_storage, [1,3,2]), :,3)
CSV.write("$data_dir/info_viewer_slant_util_long.csv", Tables.table(info_viewer_slant_util_long))
info_viewer_info_util_long = reshape(permutedims(info_tercile_cumulative_info_util_storage, [1,3,2]), :,3)
CSV.write("$data_dir/info_viewer_info_util_long.csv", Tables.table(info_viewer_info_util_long))
